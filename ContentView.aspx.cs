﻿
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ContentView : WebSite
{
    private string[] aylar = new string[12] { "OCAK", "ŞUBAT", "MART", "NİSAN", "MAYIS", "HAZİRAN", "TEMMUZ", "AĞUSTOS", "EYLÜL", "EKİM", "KASIM", "ARALIK" };
    protected void Page_Load(object sender, EventArgs e)
    {
        //string Type = Request["Type"];
        //if (Type=="Egitim")
        //{
        xContentId =Convert.ToInt32(Page.RouteData.Values["ID"]);
        string ip = GetIPAddress();
        DateTime now = DateTime.Now;
        var visit = db.ContentVisitors.FirstOrDefault(c => c.FkContentId == xContentId && DbFunctions.TruncateTime(c.CreateDate) == now.Date && c.IpAddress == ip);
        if (visit == null)
        {
            db.ContentVisitors.Add(new ContentVisitors
            {
                IpAddress = ip,
                FkContentId = xContentId,
                CreateDate = now,
                Referer = GetReferer()
            });
            db.SaveChanges();
        }

        var cnt = db.Contents.FirstOrDefault(x => x.ID == xContentId && x.DbStatus == false);
        if (cnt != null)
        {
            dvInfoTitle.Visible = false;
            SetHeader(cnt.ContentTitle, "Eğitim", cnt.ContentTitle, cnt.ContentTitle);
            ltrContent.InnerHtml = cnt.ContentDescription;
            ltrContentDesc.Text = cnt.ContentDescriptionLite;
            ltrContentTitle.Text = cnt.ContentTitle;
            contentPrice.InnerText = cnt.ContentPrice.ToString() + " TL";
            contentPrice2.InnerText = contentPrice.InnerText;
            ayOzel.InnerHtml = aylar[DateTime.Now.Month - 1] + " AYINA ÖZEL<br />";
            ayOzel2.InnerHtml = aylar[DateTime.Now.Month - 1] + " AYINA ÖZEL<br />";
            if (cnt.DiscountRate == 0)
            {
                indirimBox.Visible = false;
                ayOzel.Visible = false;
                ayOzel2.Visible = false;
                indirimBox2.Visible = false;
                altIndirim.Style.Add("padding-top", "10px");
            }
            else
            {
                contentPriceNormal.InnerText = ((Convert.ToInt32(cnt.ContentPrice) * 100 / (100 - cnt.DiscountRate))).ToString() + " TL";
                contentPriceNormal2.InnerText = contentPriceNormal.InnerText;
                indirimYuzde.InnerText = "%" + cnt.DiscountRate.ToString();
                indirimYuzde2.InnerText = "%" + cnt.DiscountRate.ToString();
            }
            //ltrKategori.Text = "Eğitimler";
            lnkAppNow.NavigateUrl = "https://ulusem.uludag.edu.tr/Basvuru/" + xContentId + "/" + EXERT.StringOperations.UrlMaker(cnt.ContentTitle);
            lnkAppNow3.NavigateUrl = "https://ulusem.uludag.edu.tr/Basvuru/" + xContentId + "/" + EXERT.StringOperations.UrlMaker(cnt.ContentTitle);

            if (cnt.ContentType == 0)
            {
                contentDesc.Visible = false;
                contentTarih.Visible = true;
                ContentDates cd = db.ContentDates.ToList().LastOrDefault();
                if (cnt.AppEndDate == null)
                    sonBasvuru.InnerText = cd.AppEndDate.ToString("dd MMMM yyyy") + " " + cd.AppEndDate.ToShortTimeString();
                else
                    sonBasvuru.InnerText = cnt.AppEndDate.Value.ToString("dd MMMM yyyy") + " " + cnt.AppEndTime;

                if (cnt.ContentStartDate == null)
                    egitimBaslangic.InnerText = cd.ContentStartDate.ToString("dd MMMM yyyy") + " " + cd.ContentStartDate.ToShortTimeString();
                else
                    egitimBaslangic.InnerText = cnt.ContentStartDate.Value.ToString("dd MMMM yyyy") + " " + cnt.ContentStartTime;

                if (cnt.ExamStartDate == null)
                    sinavBaslangic.InnerText = cd.ExamStartDate.ToString("dd MMMM yyyy") + " " + cd.ExamStartDate.ToShortTimeString();
                else
                    sinavBaslangic.InnerText = cnt.ExamStartDate.Value.ToString("dd MMMM yyyy") + " " + cnt.ExamStartTime;

                if (cnt.ExamFinishDate == null)
                    sinavBitis.InnerText = cd.ExamFinishDate.ToString("dd MMMM yyyy") + " " + cd.ExamFinishDate.ToShortTimeString();
                else
                    sinavBitis.InnerText = cnt.ExamFinishDate.Value.ToString("dd MMMM yyyy") + " " + cnt.ExamFinishTime;
            }
            else
            {
                contentDesc.Visible = true;
                contentTarih.Visible = false;
                contentDescHeader.InnerHtml = cnt.ContentDescriptionHeader;

            }
        }
        else
            Response.Redirect("/AnaSayfa");
    }

    private string GetIPAddress()
    {
        System.Web.HttpContext context = System.Web.HttpContext.Current;
        string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

        if (!string.IsNullOrEmpty(ipAddress))
        {
            string[] addresses = ipAddress.Split(',');
            if (addresses.Length != 0)
            {
                return addresses[0];
            }
        }

        return context.Request.ServerVariables["REMOTE_ADDR"];
    }

    private string GetReferer()
    {
        return Context.Request.Headers["Referer"] == null ? "" : Context.Request.Headers["Referer"].ToString();
    }
}