﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ApplicationForm.aspx.cs" Inherits="ApplicationForm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .page_loading {
            position: fixed;
            z-index: 1000;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            background: rgba( 255, 255, 255, .8 ) url('/images/Preloader_3.gif') 50% 50% no-repeat;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpContent" runat="Server">
    <section id="home" class="divider bg-lighter">
        <div class="display-table">
            <div id="dvForm" runat="server" class="display-table-cell">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-md-push-3">
                            <span style="color: red;">
                                <asp:Literal ID="ltrBasvuruBilgi" runat="server"></asp:Literal>
                            </span>
                            <div class="bg-lightest border-1px p-25">
                                <h4 class="text-theme-colored text-uppercase m-0">BAŞVURU FORMU</h4>
                                <div class="line-bottom mb-30"></div>
                                <div id="job_apply_form">
                                    <div class="form-group basvuruError">
                                        <asp:ValidationSummary ValidationGroup="grpKayit" ID="vs1" runat="server" ShowMessageBox="false" ShowSummary="true" />
                                    </div>

                                    <%--<div class="form-group name" style="display:none">
                                        <label for="TCNumaraniz">TC Numaranız<span class="required">(Fatura için gereklidir.)</span></label>
                                        <asp:TextBox ID="txtTC" class="form-control" value="" data-mask="99999999999" ValidationGroup="grpKayit" placeholder="TC Numaranızı Giriniz.." runat="server"></asp:TextBox>
                                    </div>--%>
                                    <div class="form-group name">
                                        <label for="TCNumaraniz">TC Numaranız</label>
                                        <asp:TextBox ID="txtTC" class="form-control" value="" data-mask="99999999999" ValidationGroup="grpKayit" placeholder="TC Numaranızı Giriniz.." runat="server"></asp:TextBox>
                                    </div>
                                    <div class="form-group name">
                                        <label for="adiSoyadi">Adınız<span class="required">*</span></label>
                                        <asp:TextBox ID="txtAd" class="form-control" value="" ValidationGroup="grpKayit" placeholder="Adınızı Giriniz..." runat="server"></asp:TextBox>
                                    </div>
                                    <div class="form-group name">
                                        <label for="adiSoyadi">Soyadınız<span class="required">*</span></label>
                                        <asp:TextBox ID="txtSoyad" class="form-control" value="" ValidationGroup="grpKayit" placeholder="Soyadınızı Giriniz..." runat="server"></asp:TextBox>
                                    </div>
                                    <div class="form-group name">
                                        <label for="adiSoyadi">Cinsiyet<span class="required">*</span></label>
                                        <asp:DropDownList ID="drpCinsiyet" class="form-control" value="" ValidationGroup="grpKayit" placeholder="Seçiniz"  runat="server">
                                            <asp:ListItem Value="Erkek" Text="Erkek" />
                                            <asp:ListItem Value="Kadın" Text="Kadın" />
                                        </asp:DropDownList>
                                    </div>
                                    <div class="form-group name">
                                        <label for="adiSoyadi">Vatandaşlık<span class="required">*</span></label>
                                        <asp:DropDownList ID="drpVatandaslik" class="form-control" value="" ValidationGroup="grpKayit" placeholder="Seçiniz"  runat="server">
                                            <asp:ListItem Value="0" Text="T.C. Vatandaşıyım" />
                                            <asp:ListItem Value="1" Text="Yabancı Uyrukluyum" />
                                        </asp:DropDownList>
                                    </div>
                                    <div class="form-group name">
                                        <label for="adiSoyadi">Doğum Yılınız<span class="required">*</span></label>
                                        <asp:DropDownList ID="drpDogumYil" runat="server" CssClass="form-control"></asp:DropDownList>
                                    </div>
                                    <div class="form-group email">
                                        <label for="eMail">E-Mail<span class="required">(Sık Kullandığınız E-Mail Adresinizi Dikkatlice Giriniz. Sistem Üzerindeki Tüm İşlemler E-Mail Üzerinden Yapılmaktadır.)</span></label>
                                        <asp:TextBox ID="txtMail" TextMode="Email" ValidationGroup="grpKayit" class="form-control" value="" placeholder="E-Mail Adresinizi Giriniz..." runat="server"></asp:TextBox>
                                    </div>
                                    <div class="form-group phone">
                                        <label for="telefon">Telefon<span class="required">(Doğru Girdiğinizden Mutlaka Emin Olun!)</span></label>
                                        <asp:TextBox ID="txtCepTel" TextMode="Phone" ValidationGroup="grpKayit" data-mask="(999) 999 99 99" class="form-control" value="" placeholder="Telefon Numaranızı Giriniz..." runat="server"></asp:TextBox>
                                    </div>
                                    <%--<div class="form-group message">
                                        <label for="il">Programa Katılacağınız İl</label>
                                        <asp:DropDownList ID="drpil" class="form-control" runat="server" ValidationGroup="grpKayit">
                                            <asp:ListItem Value="0">İl Se&#231;iniz</asp:ListItem>
                                            <%--<asp:ListItem Value="1">Adana</asp:ListItem>
                                            <asp:ListItem Value="2">Adıyaman</asp:ListItem>
                                            <asp:ListItem Value="3">Afyon</asp:ListItem>
                                            <asp:ListItem Value="4">Ağrı</asp:ListItem>
                                            <asp:ListItem Value="5">Amasya</asp:ListItem>
                                            <asp:ListItem Value="6">Ankara</asp:ListItem>
                                            <asp:ListItem Value="7">Antalya</asp:ListItem>
                                            <asp:ListItem Value="8">Artvin</asp:ListItem>
                                            <asp:ListItem Value="9">Aydın</asp:ListItem>
                                            <asp:ListItem Value="10">Balıkesir</asp:ListItem>
                                            <asp:ListItem Value="11">Bilecik</asp:ListItem>
                                            <%--<asp:ListItem Value="12">Bingöl</asp:ListItem>
                                            <asp:ListItem Value="13">Bitlis</asp:ListItem>
                                            <asp:ListItem Value="14">Bolu</asp:ListItem>
                                            <%--<asp:ListItem Value="15">Burdur</asp:ListItem>
                                            <asp:ListItem Value="16">Bursa</asp:ListItem>
                                            <asp:ListItem Value="17">Çanakkale</asp:ListItem>
                                            <asp:ListItem Value="18">Çankırı</asp:ListItem>
                                            <asp:ListItem Value="19">Çorum</asp:ListItem>
                                            <asp:ListItem Value="20">Denizli</asp:ListItem>
                                            <asp:ListItem Value="21">Diyarbakır</asp:ListItem>
                                            <asp:ListItem Value="22">Edirne</asp:ListItem>
                                            <asp:ListItem Value="23">Elazığ</asp:ListItem>
                                            <asp:ListItem Value="24">Erzincan</asp:ListItem>
                                            <asp:ListItem Value="25">Erzurum</asp:ListItem>
                                            <asp:ListItem Value="26">Eskişehir</asp:ListItem>
                                            <asp:ListItem Value="27">Gaziantep</asp:ListItem>
                                            <asp:ListItem Value="28">Giresun</asp:ListItem>
                                            <asp:ListItem Value="29">Gümüşhane</asp:ListItem>
                                            <asp:ListItem Value="30">Hakkari</asp:ListItem>
                                            <asp:ListItem Value="31">Hatay</asp:ListItem>
                                            <asp:ListItem Value="32">Isparta</asp:ListItem>
                                            <asp:ListItem Value="33">İçel</asp:ListItem>
                                            <asp:ListItem Value="34">İstanbul</asp:ListItem>
                                            <asp:ListItem Value="35">İzmir</asp:ListItem>
                                            <asp:ListItem Value="36">Kars</asp:ListItem>
                                            <asp:ListItem Value="37">Kastamonu</asp:ListItem>
                                            <asp:ListItem Value="38">Kayseri</asp:ListItem>
                                            <asp:ListItem Value="39">Kırklareli</asp:ListItem>
                                            <asp:ListItem Value="40">Kırşehir</asp:ListItem>
                                            <asp:ListItem Value="41">Kocaeli</asp:ListItem>
                                            <asp:ListItem Value="42">Konya</asp:ListItem>
                                            <asp:ListItem Value="43">Kütahya</asp:ListItem>
                                            <asp:ListItem Value="44">Malatya</asp:ListItem>
                                            <asp:ListItem Value="45">Manisa</asp:ListItem>
                                            <asp:ListItem Value="46">Kahramanmaraş</asp:ListItem>
                                            <asp:ListItem Value="47">Mardin</asp:ListItem>
                                            <asp:ListItem Value="48">Muğla</asp:ListItem>
                                            <asp:ListItem Value="49">Muş</asp:ListItem>
                                            <asp:ListItem Value="50">Nevşehir</asp:ListItem>
                                            <asp:ListItem Value="51">Niğde</asp:ListItem>
                                            <asp:ListItem Value="52">Ordu</asp:ListItem>
                                            <asp:ListItem Value="53">Rize</asp:ListItem>
                                            <asp:ListItem Value="54">Sakarya</asp:ListItem>
                                            <%--<asp:ListItem Value="55">Samsun</asp:ListItem>
                                            <asp:ListItem Value="56">Siirt</asp:ListItem>
                                            <asp:ListItem Value="57">Sinop</asp:ListItem>
                                            <asp:ListItem Value="58">Sivas</asp:ListItem>
                                            <asp:ListItem Value="59">Tekirdağ</asp:ListItem>
                                            <asp:ListItem Value="60">Tokat</asp:ListItem>
                                            <asp:ListItem Value="61">Trabzon</asp:ListItem>
                                            <asp:ListItem Value="62">Tunceli</asp:ListItem>
                                            <asp:ListItem Value="63">Şanlıurfa</asp:ListItem>
                                            <asp:ListItem Value="64">Uşak</asp:ListItem>
                                            <asp:ListItem Value="65">Van</asp:ListItem>
                                            <asp:ListItem Value="66">Yozgat</asp:ListItem>
                                            <asp:ListItem Value="67">Zonguldak</asp:ListItem>
                                            <asp:ListItem Value="68">Aksaray</asp:ListItem>
                                            <asp:ListItem Value="69">Bayburt</asp:ListItem>
                                            <asp:ListItem Value="70">Karaman</asp:ListItem>
                                            <asp:ListItem Value="71">Kırıkkale</asp:ListItem>
                                            <asp:ListItem Value="72">Batman</asp:ListItem>
                                            <asp:ListItem Value="73">Şırnak</asp:ListItem>
                                            <asp:ListItem Value="74">Bartın</asp:ListItem>
                                            <asp:ListItem Value="75">Ardahan</asp:ListItem>
                                            <asp:ListItem Value="76">Iğdır</asp:ListItem>
                                            <asp:ListItem Value="77">Yalova</asp:ListItem>
                                            <%--<asp:ListItem Value="78">Karabük</asp:ListItem>
                                            <asp:ListItem Value="79">Kilis</asp:ListItem>
                                            <asp:ListItem Value="80">Osmaniye</asp:ListItem>
                                            <asp:ListItem Value="81">Düzce</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>--%>
                                    <div class="form-group message">
                                        <label>Okumakta Olduğunuz/Mezun Olduğunuz Üniversite</label>
                                        <asp:DropDownList ID="ddlUni" class="form-control" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                    <div class="form-group message">
                                        <label>Okumakta Olduğunuz/Mezun Olduğunuz Üniversite Bölümü</label>
                                        <asp:TextBox ID="txtBolum" class="form-control" value="" ValidationGroup="grpKayit" placeholder="Bölümünüzü Yazın..." runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <%--<div class="container" style="padding-top: 0px; margin-top: -35px;">
                    <div class="row">
                        <div class="col-md-6 col-md-push-3">
                            <div class="bg-lightest border-1px p-25">
                                <h4 class="text-theme-colored text-uppercase m-0">BAŞVURU BİLGİLERİ</h4>
                                <div class="line-bottom mb-30"></div>
                                <div class="form-group email">
                                    <label for="egitim">Eğitim</label>
                                    <asp:TextBox ID="txtEgitimAdi" class="form-control" disabled runat="server"></asp:TextBox>
                                </div>
                                <div class="form-group email">
                                    <label for="egitimUcreti">Eğitim Ücreti</label>
                                    <asp:TextBox ID="txtEgitimUcreti" class="form-control" disabled runat="server"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>--%>

                <div class="container" style="padding-top: 0px; margin-top: -35px;">
                    <div class="row">
                        <div class="col-md-12" style="text-align: center;">
                            <asp:Button ID="btnDevamEt" OnClick="btnDevamEt_Click" runat="server" class="btn btn-primary" Text="BAŞVURUYA DEVAM ET" ValidationGroup="grpKayit"></asp:Button>

                            <asp:RequiredFieldValidator ValidationGroup="grpKayit" ID="rfv1" runat="server" ControlToValidate="txtTC" ErrorMessage="TC Kimlik Numaranızı giriniz" Display="None"></asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ValidationGroup="grpKayit" ID="rfv2" runat="server" ControlToValidate="txtAd" ErrorMessage="Adınızı giriniz" Display="None"></asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ValidationGroup="grpKayit" ID="rfv3" runat="server" ControlToValidate="txtSoyad" ErrorMessage="Soyadınızı giriniz" Display="None"></asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ValidationGroup="grpKayit" ID="rfv4" runat="server" ControlToValidate="txtMail" ErrorMessage="E-mail adresinizi giriniz" Display="None"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="rev1" ValidationGroup="grpKayit" runat="server" ErrorMessage="E-mail adresinizi hatalı girdiniz" ControlToValidate="txtMail" Text="*" Display="None" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                            <asp:RequiredFieldValidator ValidationGroup="grpKayit" ID="rfv5" runat="server" ControlToValidate="txtCepTel" ErrorMessage="Telefon numaranızı giriniz" Display="None"></asp:RequiredFieldValidator>
                            <%--<asp:CustomValidator ValidationGroup="grpKayit" ID="cv2" runat="server" ErrorMessage="İl seçiniz" Text="*" ClientValidationFunction="cv2" Display="None" OnServerValidate="cv2_ServerValidate"></asp:CustomValidator>--%>

                            
                        </div>
                    </div>
                </div>
            </div>
            <div id="dvSuc" runat="server" class="display-table-cell">
                <div class="container" style="padding-top: 0px;">
                    <div class="row">
                        <div class="col-md-6 col-md-push-3">
                            <div class="bg-lightest border-1px p-25">
                                <h4 class="text-theme-colored text-uppercase m-0">BAŞVURU DURUMU</h4>
                                <div class="line-bottom mb-30"></div>
                                <p>
                                    <asp:Literal ID="ltrAppStatu" runat="server"></asp:Literal>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" id="chkKontrol" runat="server" />
    </section>

    <script src="/js/inputmask.js"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $('.page_loading').css({ 'display': 'none' });
            $("#cpContent_btnDevamEt").click(function () {
                if (Page_ClientValidate("grpKayit")) {
                    $('.page_loading').css({ 'display': 'block' });
                }
            });
            $("div#cpContent_dveftHavale").hide(100);

        });
        function krediKartiGoster() {
            $("div#cpContent_dvkrediKarti").show(100);
            $("div#cpContent_dveftHavale").hide(100);
            document.getElementById("cpContent_chkEftHavale").checked = false;
            $("#cpContent_chkEftHavale").removeAttr("checked");
            document.getElementById("cpContent_chkKrediKarti").checked = true;
            $("#cpContent_chkKrediKarti").attr("checked", "checked");
            $("#cpContent_chkKontrol").val("0");

        }
        function eftHavaleGoster() {
            $("div#cpContent_dvkrediKarti").hide(100);
            $("div#cpContent_dveftHavale").show(100);
            document.getElementById("cpContent_chkKrediKarti").checked = false;
            $("#cpContent_chkKrediKarti").removeAttr("checked");
            document.getElementById("cpContent_chkEftHavale").checked = true;
            $("#cpContent_chkEftHavale").attr("checked", "checked");
            $("#cpContent_chkKontrol").val("1");

        }
        function formKontrolEt() {
            var TCNumaraniz = document.getElementById('TCNumaraniz').value;
            var adiSoyadi = document.getElementById('adiSoyadi').value;
            var eMail = document.getElementById('eMail').value;
            var telefon = document.getElementById('telefon').value;
            var ilce = document.getElementById('ilce').value;
            var il = document.getElementById('il').value;
            var mezuniyet = document.getElementById('mezuniyet').value;
            var egitimSekli = document.getElementById('egitimSekli').value;
            var satisSozlesmesi = document.getElementById('satisSozlesmesi').checked;

            if (TCNumaraniz == "") { alert('TC Numarasını Giriniz!'); return false; }
            if (adiSoyadi == "") { alert('Adınızı ve Soyadınızı Giriniz!'); return false; }
            if (eMail == "") { alert('E-Mail Adresinizi Giriniz!'); return false; }
            if (telefon == "") { alert('Telefon Numaranızı Giriniz!'); return false; }
            if (ilce == "") { alert('İlçenizi Giriniz!'); return false; }
            if (il == "0") { alert('Yaşadığınız İli Giriniz!'); return false; }
            if (il == "0") { alert('Mezuniyetinizi Seçiniz!'); return false; }
            if (egitimSekli == "0") { alert('Lütfen Eğitim Şeklinizi Seçiniz!'); return false; }
            if (satisSozlesmesi == false) { alert('Lütfen Satış Sözleşmesini Onaylayınız!'); return false; }
            $('#btn').attr('disabled', true);
            $("#yaz").ajaxStart(function () {
                $(this).html('<img src="http://www.ilkizakademi.com/yukleniyor.gif" />');
            });
        }

        <%--function cv2(oSrc, args) {
            var drpAs = document.all ? document.all["<%=drpil.ClientID  %>"] : document.getElementById("<%= drpil.ClientID  %>");
            args.IsValid = (drpAs.selectedIndex > 0);
        }--%>
    </script>
</asp:Content>


