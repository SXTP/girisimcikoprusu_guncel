﻿using EXERT;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class testt : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        DirectoryInfo d = new DirectoryInfo(Server.MapPath("/sertifikalar/"));//Assuming Test is your Folder
        FileInfo[] Files = d.GetFiles("*.pdf"); //Getting Text files
        for (int i = 0; i < Files.Length; i++)
        {
            Files[i].MoveTo((Server.MapPath("/sertifikalar/") + Files[i].Name.UrlMaker()).Replace("-pdf",".pdf"));
            Response.Write(Files[i].Name);
        }
    }
}