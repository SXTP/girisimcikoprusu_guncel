﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class NewsDetail : WebSite
{
    protected void Page_Load(object sender, EventArgs e)
    {
        var news = db.News.FirstOrDefault(n => n.ID == xContentId);

        dvInfoTitle.Visible = true;
        SetHeader(news.NTitle, EXERT.SiteSettings.SiteName, news.NTitle, news.NTitle);

        haberResmi.Src = news.NBanner;
        ltrBaslik.InnerText = news.NTitle;
        ltrContent.Text = news.NContent;
        ltrTarih.Text = news.CreateDate.ToLongDateString() + " | " + news.CreateDate.ToShortTimeString();
    }
}