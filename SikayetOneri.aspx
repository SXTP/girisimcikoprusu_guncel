﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="SikayetOneri.aspx.cs" Inherits="SikayetOneri" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpContent" Runat="Server">

 
    <section id="home" class="divider bg-lighter">
        <div class="display-table">
            <div id="dvForm" runat="server" class="display-table-cell">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-md-push-3">
                            <div class="bg-lightest border-1px p-25">
                                <h4 class="text-theme-colored text-uppercase m-0">ŞİKAYET VEYA ÖNERİLERİNİZ</h4>
                                <div class="line-bottom mb-30"></div>
                                <div id="job_apply_form" action="SikayetOneri.aspx" name="job_apply_form" role="form" method="post" enctype="multipart/form-data" novalidate="novalidate">
                                    
                                    <div class="form-group name">
                                        <label for="adiSoyadi">Adınız Soyadınız<span class="required">*</span></label>
                                        <asp:TextBox id="txtAdSoyad" class="form-control" value="" ValidationGroup="grpKayit" placeholder="Adınızı Giriniz..." runat="server"></asp:TextBox>
                                    </div>
                                                                
                                    <div class="form-group email">
                                        <label for="eMail">E-Mail<span class="required">(Sık Kullandığınız E-Mail Adresinizi Dikkatlice Giriniz. Sistem Üzerindeki Tüm İşlemler E-Mail Üzerinden Yapılmaktadır.)</span></label>
                                        <asp:TextBox id="txtMail" runat="server" AutoCompleteType="Email" ToolTip="Email" ValidateRequestMode="Enabled" onfocus="this.type='email'" TextMode="Email" ValidationGroup="grpKayit" class="form-control" value="" placeholder="E-Mail Adresinizi Giriniz..."></asp:TextBox>
                                    </div>
                                    <div class="form-group phone">
                                        <label for="telefon">Telefon<span class="required">(Doğru Girdiğinizden Mutlaka Emin Olun!)</span></label>
                                        <asp:TextBox id="txtCepTel" runat="server" TextMode="Phone" ValidationGroup="grpKayit" data-mask="(999) 999 99 99" class="form-control" value="" placeholder="Telefon Numaranızı Giriniz..."></asp:TextBox>
                                    </div>
                                         <div class="form-group name">
                                        <label for="baslik">Şikayet veya Öneri Başlığı<span class="required">*</span></label>
                                        <asp:TextBox id="txtbaslik" runat="server" class="form-control" value="" ValidationGroup="grpKayit" placeholder="Başlığı Giriniz..."></asp:TextBox>
                                    </div>
                                         <div class="form-group phone">
                                        <label for="telefon">Şikayet veya Öneriniz<span class="required"></span></label>
                                        <asp:TextBox id="TextBox" runat="server" TextMode="multiline"  ValidationGroup="grpKayit" class="form-control" value=""></asp:TextBox>
                                    </div>
                                     <div class="col-md-12 text-center" style="margin:auto;">
                                           <asp:Button ID="btnSikayetGonder" OnClick="btnSikayetGonder_Onclick" runat="server" class="btn btn-primary" Text="GÖNDER" ValidationGroup="grpKayit"></asp:Button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          
        </div>
        <input type="hidden" id="chkKontrol" runat="server" />
    </section>


</asp:Content>

