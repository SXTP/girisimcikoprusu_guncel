﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Administrator/Content/MasterPage.master" AutoEventWireup="true" CodeFile="SikayetveOneriler.aspx.cs" Inherits="Administrator_SikayetveOneriler" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCss" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpContent" Runat="Server">
      <div class="row">
        <div class="col-md-12">
            <div class="card">

                <div class="card-body">
                    <%--<div class="col-md-12">
                        <asp:Button id="excelBtn" runat="server" style="float: right; margin-bottom: 10px;" CssClass="btn btn-brand" Text="Excel İndir" OnClick="excelBtn_Click" />
                    </div>--%>
                    <!-- Modal -->
                    <%--<div class="modal fade" id="addRowModal" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header no-bd">
                                            <h5 class="modal-title">
                                                <span class="fw-mediumbold">New</span>
                                                <span class="fw-light">Row
                                                </span>
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <p class="small">Create a new row using this form, make sure you fill them all</p>
                                            <div>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group form-group-default">
                                                            <label>Name</label>
                                                            <input id="addName" type="text" class="form-control" placeholder="fill name">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 pr-0">
                                                        <div class="form-group form-group-default">
                                                            <label>Position</label>
                                                            <input id="addPosition" type="text" class="form-control" placeholder="fill position">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group form-group-default">
                                                            <label>Office</label>
                                                            <input id="addOffice" type="text" class="form-control" placeholder="fill office">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer no-bd">
                                            <button type="button" id="addRowButton" class="btn btn-primary">Add</button>
                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>--%>

                    <div class="table-responsive">
                        <table id="add-row" class="display table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th style="width: 30%">ID</th>
                                    <th style="width: 30%">Adı Soyadı</th>                                   
                                    <th>İçerik</th>
                                    <th style="width: 10%">İşlemler</th>
                                </tr>
                            </thead>
                     
                            <tbody>
                                <asp:Repeater ID="rptSikayetList" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                            <td><%#Eval("ID") %></td>
                                            <td><%#Eval("AdSoyad") %></td>                           
                                            <td><%#Eval("Icerik") %></td>
                                            <td>
                                                <div class="form-button-action">
                                                    <a href="SikayetveOneriView.aspx?Id=<%#Eval("ID") %>" class="btn btn-link btn-primary btn-lg" data-original-title="Düzenle">
                                                        <i class="fa fa-edit"></i>
                                                    </a>
                                                    <a data-id="<%#Eval("ID") %>" onclick="SilClick" class="btn btn-link btn-danger" data-original-title="Sil">
                                                        <i class="fa fa-times"></i>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphJs" Runat="Server">
</asp:Content>

