﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AnnouncementDetail.aspx.cs" Inherits="AnnouncementDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpContent" runat="Server">
    <section class="">
        <div class="container">
            <div class="section-content">
                <div class="row">
                    <div class="col-md-12" style="text-align: center;">
                        <img id="haberResmi" runat="server" alt="" style="max-height: 400px; max-width: 400px;"/>
                    </div>
                    <div class="col-md-12" style="text-align: center;">
                        <h2 ID="ltrBaslik" runat="server"></h2>
                    </div>
                    <div class="col-md-12">
                        <asp:Literal ID="ltrContent" runat="server"></asp:Literal>
                    </div>
                    <div class="col-md-12" style="text-align: right;">
                        <asp:Literal ID="ltrTarih" runat="server"></asp:Literal>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>