﻿<%@ Page Language="C#" MasterPageFile="~/Administrator/Content/MasterPage.master" AutoEventWireup="true" CodeFile="ContentDateAddEdit.aspx.cs" Inherits="Administrator_ContentDateAddEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCss" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpContent" runat="Server">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Haber Bilgileri</h4>
                </div>
                <div id="exampleValidation" novalidate="novalidate">
                    <div class="card-body">
                        
                             <div class="form-group form-show-validation row">
                                <label for="name" class="col-lg-3 col-md-3 col-sm-4 mt-sm-2 text-right">Son Başvuru Tarihi</label>
                                <div class="col-lg-4 col-md-9 col-sm-8">
                                    <asp:TextBox ID="txtSonBasvuru" class="form-control" placeholder="Son Başvuru Tarihi Giriniz." runat="server"></asp:TextBox>
                                </div>
                            </div>
                             <div class="form-group form-show-validation row">
                                <label for="name" class="col-lg-3 col-md-3 col-sm-4 mt-sm-2 text-right">Son Başvuru Saati</label>
                                <div class="col-lg-4 col-md-9 col-sm-8">
                                    <asp:TextBox ID="txtSonBasvuruSaat" class="form-control" placeholder="Son Başvuru Saati Giriniz." runat="server"></asp:TextBox>
                                </div>
                            </div>
                             <div class="form-group form-show-validation row">
                                <label for="name" class="col-lg-3 col-md-3 col-sm-4 mt-sm-2 text-right">Eğitim Başlangıç Tarihi</label>
                                <div class="col-lg-4 col-md-9 col-sm-8">
                                    <asp:TextBox ID="txtEgitimBaslangic" class="form-control" placeholder="Eğitim Başlangıç Tarihi Giriniz." runat="server"></asp:TextBox>
                                </div>
                            </div>
                             <div class="form-group form-show-validation row">
                                <label for="name" class="col-lg-3 col-md-3 col-sm-4 mt-sm-2 text-right">Eğitim Başlangıç Saati</label>
                                <div class="col-lg-4 col-md-9 col-sm-8">
                                    <asp:TextBox ID="txtEgitimBaslangicSaat" class="form-control" placeholder="Eğitim Başlangıç Saati Giriniz." runat="server"></asp:TextBox>
                                </div>
                            </div>
                             <div class="form-group form-show-validation row">
                                <label for="name" class="col-lg-3 col-md-3 col-sm-4 mt-sm-2 text-right">Sınav Başlangıç Tarihi</label>
                                <div class="col-lg-4 col-md-9 col-sm-8">
                                    <asp:TextBox ID="txtSinavBaslangic" class="form-control" placeholder="Sınav Başlangıç Tarihi Giriniz." runat="server"></asp:TextBox>
                                </div>
                            </div>
                             <div class="form-group form-show-validation row">
                                <label for="name" class="col-lg-3 col-md-3 col-sm-4 mt-sm-2 text-right">Sınav Başlangıç Saati</label>
                                <div class="col-lg-4 col-md-9 col-sm-8">
                                    <asp:TextBox ID="txtSinavBaslangicSaat" class="form-control" placeholder="Sınav Başlangıç Saati Giriniz." runat="server"></asp:TextBox>
                                </div>
                            </div>
                             <div class="form-group form-show-validation row">
                                <label for="name" class="col-lg-3 col-md-3 col-sm-4 mt-sm-2 text-right">Sınav Bitiş Tarihi</label>
                                <div class="col-lg-4 col-md-9 col-sm-8">
                                    <asp:TextBox ID="txtSinavBitis" class="form-control" placeholder="Sınav Bitiş Tarihi Giriniz." runat="server"></asp:TextBox>
                                </div>
                            </div>
                             <div class="form-group form-show-validation row">
                                <label for="name" class="col-lg-3 col-md-3 col-sm-4 mt-sm-2 text-right">Sınav Bitiş Saati</label>
                                <div class="col-lg-4 col-md-9 col-sm-8">
                                    <asp:TextBox ID="txtSinavBitisSaat" class="form-control" placeholder="Sınav Bitiş Saati Giriniz." runat="server"></asp:TextBox>
                                </div>
                            </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-action">
                    <div class="row">
                        <div class="col-md-12">
                            <asp:Button ID="btnSave" class="btn btn-success" OnClick="btnSave_Click" runat="server" Text="Ekle / Kaydet" />
                            <button class="btn btn-danger">İptal</button>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphJs" runat="Server">
    <script>
        
		$('#cpContent_txtSonBasvuru').datetimepicker({
			format: 'DD/MM/YYYY',
		});
		$('#cpContent_txtEgitimBaslangic').datetimepicker({
			format: 'DD/MM/YYYY',
		});
		$('#cpContent_txtSinavBaslangic').datetimepicker({
			format: 'DD/MM/YYYY',
		});
		$('#cpContent_txtSinavBitis').datetimepicker({
			format: 'DD/MM/YYYY',
        });
    </script>
</asp:Content>