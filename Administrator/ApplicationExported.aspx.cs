﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class Administrator_ApplicationExported : Admin
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SetHeader("Başvuru İşlemleri", "Eğitime Aktarılmış Başvurular", "", "");
        rptList.DataSource = db.Applications.Where(x => x.DbStatus == false && x.EgitimeAktarildi == true).ToList();
        rptList.DataBind();
    }

    protected void rptList_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        Applications app = (Applications)e.Item.DataItem;
        Literal tarih = (Literal)e.Item.FindControl("tblTarih");
        tarih.Text = app.CreateDate.ToLongDateString() + " - " + app.CreateDate.ToShortTimeString();

        Literal odemeTipi = (Literal)e.Item.FindControl("tblOdemeTipi");
        odemeTipi.Text = app.PaymentType == 1 ? "<b class='m--font-focus'>Kredi Kartı</b>" : "<b class='m--font-info'>EFT/Havale</b>";
    }
}