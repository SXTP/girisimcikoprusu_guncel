﻿<%@ Page Language="C#" MasterPageFile="~/Administrator/Content/MasterPage.master" AutoEventWireup="true" CodeFile="ApplicationExported.aspx.cs" Inherits="Administrator_ApplicationExported" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCss" runat="Server">
    <link href="/Administrator/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpContent" runat="Server">
    <div class="row">
        <div class="col-md-12">
            
            <div class="m-portlet m-portlet--responsive-mobile">
                <div class="m-portlet__body">
                    <table id="appList" class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Kimlik Numarası</th>
                                <th style="width: 20%">Adı Soyadı</th>
                                <th style="width: 30%">Başvurulan İçerik</th>
                                <th style="width: 10%">Ödeme Tipi</th>
                                <th>Başvuru Tarihi</th>
                                <th style="width: 10%">İşlemler</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>#</th>
                                <th>Kimlik Numarası</th>
                                <th>Adı Soyadı</th>
                                <th>Başvurulan İçerik</th>
                                <th>Ödeme Tipi</th>
                                <th>Başvuru Tarihi</th>
                                <th>İşlemler</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <asp:Repeater ID="rptList" runat="server" OnItemDataBound="rptList_ItemDataBound">
                                <ItemTemplate>
                                    <tr>
                                        <td><%#Eval("ID") %></td>
                                        <td><%#Eval("Users.UserName") %></td>
                                        <td><%#Eval("Users.Name") %> <%#Eval("Users.Surname") %></td>
                                        <td><a href="AppListByContent.aspx?Id=<%#Eval("FkContentId") %>"><%#Eval("Contents.ContentTitle") %></a></td>
                                        <td><asp:Literal runat="server" ID="tblOdemeTipi"></asp:Literal></td>
                                        <td><asp:Literal runat="server" ID="tblTarih"></asp:Literal></td>
                                        <td>
                                            <div class="form-button-action">
                                                <a href="ApplicationView.aspx?Id=<%#Eval("ID") %>&Pt=2" class="btn btn-link btn-primary" data-original-title="Düzenle">
                                                    <i class="fa fa-edit" style="font-size: 0.8rem;"></i>
                                                </a>
                                                <a data-id="<%#Eval("ID") %>" onclick="SilClick(this);" class="btn btn-link btn-danger" data-original-title="Sil">
                                                    <i class="fa fa-times" style="color: white;"></i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphJs" runat="Server">
    <script>
        $(document).ready(function () {
            $('#appList').DataTable({
                responsive: true,
                pageLength: 10,
                order: [[0, "desc"]],
                language: {
                    url: "/Administrator/assets/vendors/custom/datatables/Turkish.json"
                },
                columnDefs: [{
                    targets: 5,
                    orderable: false
                }, {
                    targets: 6,
                    orderable: false
                }]
            });
        });

        function SilClick(elem) {
            swal({
				title: 'Emin misiniz?',
				text: "Başvuruyu silmek istediğinize emin misiniz?",
				type: 'warning',
				buttons:{
					confirm: {
						text : 'Sil',
						className : 'btn btn-danger'
					},
					cancel: {
                        visible: true,
                        text: "İptal",
						className: 'btn btn-success'
					}
				}
			}).then((Delete) => {
                if (Delete) {
                    
                    var id = $(elem).data("id");
                    
                    $().newAjaxAdminHTML({},
                    {
                        funcName: "BasvuruSil",
                        newData: "{'id': '" + id + "'}",
                        onSuccess: function (msg) {
                            window.location.href = "ApplicationList.aspx";
                        }
                    });

				} else {
					swal.close();
				}
			});
        };
    </script>
</asp:Content>

