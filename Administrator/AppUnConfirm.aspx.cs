﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

using ExcelLibrary;
using ExcelLibrary.SpreadSheet;
public partial class Administrator_AppUnConfirm : Admin
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SetHeader("Onaylanmayan Başvurular", "Onaylanmayan Başvurular", "", "");
        rptList.DataSource = db.Applications.Where(x => x.Status == 0 && x.DbStatus == false && x.FkContentId == 1087).ToList();
        rptList.DataBind();
    }
    protected void excelBtn_Click(object sender, EventArgs e)
    {

        Workbook workBook = new Workbook();
        Worksheet workSheet = new Worksheet("Basvurular");

        var basvurular = db.Applications.Where(b => b.Status == 0 && b.FkContentId == 1087).ToList();
        for (int i = 0; i < basvurular.Count; i++)
        {
            Cell cell = new Cell(basvurular[i].ID);
            workSheet.Cells[i, 0] = cell;
            cell = new Cell(basvurular[i].Users.UserName);
            workSheet.Cells[i, 1] = cell;
            cell = new Cell(basvurular[i].Users.Name + " " + basvurular[i].Users.Surname);
            workSheet.Cells[i, 2] = cell;
            cell = new Cell(basvurular[i].Users.UserDetails.ToList().LastOrDefault().PhoneNumber);
            workSheet.Cells[i, 3] = cell;
            cell = new Cell(basvurular[i].Users.UserDetails.ToList().LastOrDefault().Email);
            workSheet.Cells[i, 4] = cell;
            cell = new Cell(basvurular[i].Users.UserDetails.ToList().LastOrDefault().Address);
            workSheet.Cells[i, 5] = cell;
            cell = new Cell(basvurular[i].Contents.ContentTitle);
            workSheet.Cells[i, 6] = cell;
            cell = new Cell(basvurular[i].CreateDate.ToString());
            workSheet.Cells[i, 7] = cell;
            cell = new Cell(basvurular[i].ContentField1);
            workSheet.Cells[i, 8] = cell;
            if (basvurular[i].Status == 0)
            {
                cell = new Cell("Onay Bekliyor");
                workSheet.Cells[i, 9] = cell;
            }
            else if (basvurular[i].Status == 1)
            {
                cell = new Cell("Reddedildi");
                workSheet.Cells[i, 9] = cell;
            }
            else
            {
                cell = new Cell("Onaylandı");
                workSheet.Cells[i, 9] = cell;
            }
            //workSheet.Cells[i, 8] = basvurular[i - 2].Status == 0 ? "Onay Bekliyor" : basvurular[i - 2].Status == 1 ? "Reddedildi" : "Onaylandı";
        }



        /*workSheet.Cells[1, 1].Value = "ID";
        workSheet.Cells[1, 2].Value = "KİMLİK NUMARASI";
        workSheet.Cells[1, 3].Value = "ADI SOYADI";
        workSheet.Cells[1, 4].Value = "TELEFON";
        workSheet.Cells[1, 5].Value = "BAŞVURULAN İÇERİK";
        workSheet.Cells[1, 6].Value = "BAŞVURU TARİHİ";
        workSheet.Cells[1, 7].Value = "BAŞVURULAN İL";
        workSheet.Cells[1, 8].Value = "BAŞVURU DURUMU";*/

        workBook.Worksheets.Add(workSheet);
        workBook.Save(Server.MapPath(@"~/w1.xls"));

        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        Response.AppendHeader("Content-Disposition", "attachment; filename=Basvurular.xls");
        Response.TransmitFile(Server.MapPath(@"~/w1.xls"));
        Response.End();
    }
}