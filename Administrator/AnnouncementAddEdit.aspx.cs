﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class AnnouncementAddEdit : Admin
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SetHeader("Duyuru İşlemleri", "Duyuru Ekle / Düzenle", "", "");
        if (!IsPostBack)
        {
            if (xProcessType == 2)
            {
                var ann = db.Announcements.FirstOrDefault(x => x.ID == xContentId);
                if (ann != null)
                {
                    txtContentTitle.Text = ann.ATitle;
                    txtContentDescription.Text = ann.AContent;
                    imgBox.Src = ann.ABanner;
                }
            }
            else if (xProcessType == 3)
            {
                var ann = db.Announcements.FirstOrDefault(x => x.ID == xContentId);
                ann.DbStatus = true;
                db.SaveChanges();
                ShowSuccess("Duyuru Başarıyla Silindi.Listeleme sayfasına yönlendirilirken lütfen bekleyin..");
                pnlPageContent.Visible = false;
                RedirectWithJavascript("AnnouncementList.aspx", 2000);
            }
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (xProcessType!=2)
        {
            Announcements ann = new Announcements();
            ann.ATitle = txtContentTitle.Text;
            ann.AContent = txtContentDescription.Text;
            ann.CreateDate = DateTime.Now;
            ann.DbStatus = false;
            ann.FkCreated = EXERT.User.GetUserId();
            string FilePath = "~/uploads/" + Guid.NewGuid() + ".png";
            if (flMedia.HasFile == true)
            {
                flMedia.SaveAs(Server.MapPath(FilePath));
            }
            ann.ABanner = FilePath.Replace("~", "");

            db.Announcements.Add(ann);
            db.SaveChanges();
            ShowSuccess("Duyuru Başarıyla Eklendi.Listeleme sayfasına yönlendirilirken lütfen bekleyin..");
            pnlPageContent.Visible = false;
            RedirectWithJavascript("AnnouncementList.aspx", 2000);
        }
        else
        {
            var ann = db.Announcements.FirstOrDefault(x => x.ID == xContentId);
            ann.ATitle = txtContentTitle.Text;
            ann.AContent = txtContentDescription.Text;
            ann.CreateDate = DateTime.Now;
            ann.DbStatus = false;
            ann.FkCreated = EXERT.User.GetUserId();
            string FilePath = "~/uploads/" + Guid.NewGuid() + ".png";
            if (flMedia.HasFile == true)
            {
                flMedia.SaveAs(Server.MapPath(FilePath));
            }
            ann.ABanner = FilePath.Replace("~", "");

            db.SaveChanges();
            ShowSuccess("Değişiklikler Başarıyla Kaydedildi.Listeleme sayfasına yönlendirilirken lütfen bekleyin..");
            pnlPageContent.Visible = false;
            RedirectWithJavascript("AnnouncementList.aspx", 2000);
        }
        
    }
}