﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Administrator_ContentSort : Admin
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SetHeader("İçerikler", "İçerik Düzenleme", "", "");

        var contents = db.Contents.Where(p => p.DbStatus == false).OrderBy(p => p.SortId).ThenByDescending(p => p.ID).ToList();
        List<Contents> c1 = new List<Contents>();
        List<Contents> c2 = new List<Contents>();
        List<Contents> c3 = new List<Contents>();

        for (int i = 0; i < contents.Count; i++)
        {
            if (i % 3 == 0)
                c1.Add(contents[i]);
            else if (i % 3 == 1)
                c2.Add(contents[i]);
            else
                c3.Add(contents[i]);
        }
        content1Rpt.DataSource = c1;
        content1Rpt.DataBind();

        content2Rpt.DataSource = c2;
        content2Rpt.DataBind();

        content3Rpt.DataSource = c3;
        content3Rpt.DataBind();
    }

    int rowId1 = 1;
    int rowId2 = 2;
    int rowId3 = 3;
    protected void content1Rpt_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        var div = (Panel)e.Item.FindControl("c1P");
        div.Attributes.Add("data-no", rowId1.ToString());
        div.Attributes.Add("data-id", ((Contents)e.Item.DataItem).ID.ToString());
        rowId1 += 3;
    }

    protected void content2Rpt_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        var div = (Panel)e.Item.FindControl("c2P");
        div.Attributes.Add("data-no", rowId2.ToString());
        div.Attributes.Add("data-id", ((Contents)e.Item.DataItem).ID.ToString());
        rowId2 += 3;
    }

    protected void content3Rpt_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        var div = (Panel)e.Item.FindControl("c3P");
        div.Attributes.Add("data-no", rowId3.ToString());
        div.Attributes.Add("data-id", ((Contents)e.Item.DataItem).ID.ToString());
        rowId3 += 3;
    }
}