﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Administrator/Content/MasterPage.master"  ValidateRequest="false" AutoEventWireup="true" CodeFile="ContentAddEdit.aspx.cs" Inherits="ContentAddEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCss" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpContent" runat="Server">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">İçerik Bilgileri</h4>
                </div>
                <div id="exampleValidation" novalidate="novalidate">
                    <div class="card-body">
                        <div class="form-group form-show-validation row">
                            <label for="name" class="col-lg-3 col-md-3 col-sm-4 mt-sm-2 text-right">Başlık <span class="required-label">*</span></label>
                            <div class="col-lg-4 col-md-9 col-sm-8">
                                <asp:TextBox ID="txtContentTitle" class="form-control" placeholder="Başlık Giriniz" required="" runat="server"></asp:TextBox>

                            </div>
                        </div>
                        <div class="form-group form-show-validation row">
                            <label for="name" class="col-lg-3 col-md-3 col-sm-4 mt-sm-2 text-right">Anahtar Kelimeler <span class="required-label">*</span></label>
                            <div class="col-lg-4 col-md-9 col-sm-8">
                                <asp:TextBox ID="txtContentKeywords" TextMode="MultiLine" class="form-control" placeholder="Arama Motorları için Anahtar Kelimeleri Giriniz." required="" runat="server"></asp:TextBox>

                            </div>
                        </div>
                        <div class="form-group form-show-validation row">
                            <label for="name" class="col-lg-3 col-md-3 col-sm-4 mt-sm-2 text-right">Açıklama <span class="required-label">*</span></label>
                            <div class="col-lg-4 col-md-9 col-sm-8">
                                <asp:TextBox ID="txtContentDescSEO" TextMode="MultiLine" class="form-control" placeholder="Arama Motorları için Açıklama Giriniz." required="" runat="server"></asp:TextBox>
                            </div>
                        </div>
                         <div class="form-group form-show-validation row">
                            <label for="name" class="col-lg-3 col-md-3 col-sm-4 mt-sm-2 text-right">Açıklama (Banner)<span class="required-label">*</span></label>
                            <div class="col-lg-4 col-md-9 col-sm-8">
                                <asp:TextBox ID="txtContentDescLite" TextMode="MultiLine" class="form-control" placeholder="Banner için Açıklama Giriniz." required="" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group form-show-validation row">
                            <label for="name" class="col-lg-3 col-md-3 col-sm-4 mt-sm-2 text-right">Fiyat <span class="required-label">*</span></label>
                            <div class="col-lg-4 col-md-9 col-sm-8">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">₺</span>
                                    </div>
                                    <asp:TextBox ID="txtContentPrice" class="form-control" placeholder="İçerik Fiyatı Giriniz" required="" runat="server"></asp:TextBox>

                                    <div class="input-group-append">
                                        <span class="input-group-text">.00</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                         <div class="form-group form-show-validation row">
                            <label for="checkbox" class="col-lg-3 col-md-3 col-sm-4 mt-sm-2 text-right">İndirim Olsun Mu?</label>
                            <div class="col-lg-4 col-md-9 col-sm-8">
                                <input type="checkbox" id="indirimChk" style="margin-top: 10px;" runat="server" />
                            </div>
                        </div>
                         <div id="indirimDiv" class="form-group form-show-validation row" runat="server">
                            <label for="name" class="col-lg-3 col-md-3 col-sm-4 mt-sm-2 text-right">İndirim Yüzdesi</label>
                            <div class="col-lg-4 col-md-9 col-sm-8">
                                <asp:TextBox ID="txtIndirimYuzde" class="form-control" placeholder="İndirim Yüzdesi Giriniz." runat="server"></asp:TextBox>
                            </div>
                        </div>
                         <div class="form-group form-show-validation row">
                            <label for="checkbox" class="col-lg-3 col-md-3 col-sm-4 mt-sm-2 text-right">Özel Tarih Aralığı Olsun Mu?</label>
                            <div class="col-lg-4 col-md-9 col-sm-8">
                                <input type="checkbox" id="tarihChk" style="margin-top: 10px;" runat="server" />
                            </div>
                        </div>
                        <div id="tarihDiv" runat="server">
                             <div class="form-group form-show-validation row">
                                <label for="name" class="col-lg-3 col-md-3 col-sm-4 mt-sm-2 text-right">Son Başvuru Tarihi</label>
                                <div class="col-lg-4 col-md-9 col-sm-8">
                                    <asp:TextBox ID="txtSonBasvuru" class="form-control" placeholder="Son Başvuru Tarihi Giriniz." runat="server"></asp:TextBox>
                                </div>
                            </div>
                             <div class="form-group form-show-validation row">
                                <label for="name" class="col-lg-3 col-md-3 col-sm-4 mt-sm-2 text-right">Son Başvuru Saati</label>
                                <div class="col-lg-4 col-md-9 col-sm-8">
                                    <asp:TextBox ID="txtSonBasvuruSaat" class="form-control" placeholder="Son Başvuru Saati Giriniz." runat="server"></asp:TextBox>
                                </div>
                            </div>
                             <div class="form-group form-show-validation row">
                                <label for="name" class="col-lg-3 col-md-3 col-sm-4 mt-sm-2 text-right">Eğitim Başlangıç Tarihi</label>
                                <div class="col-lg-4 col-md-9 col-sm-8">
                                    <asp:TextBox ID="txtEgitimBaslangic" class="form-control" placeholder="Eğitim Başlangıç Tarihi Giriniz." runat="server"></asp:TextBox>
                                </div>
                            </div>
                             <div class="form-group form-show-validation row">
                                <label for="name" class="col-lg-3 col-md-3 col-sm-4 mt-sm-2 text-right">Eğitim Başlangıç Saati</label>
                                <div class="col-lg-4 col-md-9 col-sm-8">
                                    <asp:TextBox ID="txtEgitimBaslangicSaat" class="form-control" placeholder="Eğitim Başlangıç Saati Giriniz." runat="server"></asp:TextBox>
                                </div>
                            </div>
                             <div class="form-group form-show-validation row">
                                <label for="name" class="col-lg-3 col-md-3 col-sm-4 mt-sm-2 text-right">Sınav Başlangıç Tarihi</label>
                                <div class="col-lg-4 col-md-9 col-sm-8">
                                    <asp:TextBox ID="txtSinavBaslangic" class="form-control" placeholder="Sınav Başlangıç Tarihi Giriniz." runat="server"></asp:TextBox>
                                </div>
                            </div>
                             <div class="form-group form-show-validation row">
                                <label for="name" class="col-lg-3 col-md-3 col-sm-4 mt-sm-2 text-right">Sınav Başlangıç Saati</label>
                                <div class="col-lg-4 col-md-9 col-sm-8">
                                    <asp:TextBox ID="txtSinavBaslangicSaat" class="form-control" placeholder="Sınav Başlangıç Saati Giriniz." runat="server"></asp:TextBox>
                                </div>
                            </div>
                             <div class="form-group form-show-validation row">
                                <label for="name" class="col-lg-3 col-md-3 col-sm-4 mt-sm-2 text-right">Sınav Bitiş Tarihi</label>
                                <div class="col-lg-4 col-md-9 col-sm-8">
                                    <asp:TextBox ID="txtSinavBitis" class="form-control" placeholder="Sınav Bitiş Tarihi Giriniz." runat="server"></asp:TextBox>
                                </div>
                            </div>
                             <div class="form-group form-show-validation row">
                                <label for="name" class="col-lg-3 col-md-3 col-sm-4 mt-sm-2 text-right">Sınav Bitiş Saati</label>
                                <div class="col-lg-4 col-md-9 col-sm-8">
                                    <asp:TextBox ID="txtSinavBitisSaat" class="form-control" placeholder="Sınav Bitiş Saati Giriniz." runat="server"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">Tarih Açıklama</div>
                </div>
                <div class="card-body">
                    <div class="form-group form-show-validation row">
                        <label for="checkbox" class="col-lg-3 col-md-3 col-sm-4 mt-sm-2 text-right">Örgün Eğitim Mi?</label>
                        <div class="col-lg-4 col-md-9 col-sm-8">
                            <input type="checkbox" id="chkOrgun" style="margin-top: 10px;" runat="server" />
                        </div>
                    </div>
                    <div id="orgunDiv" runat="server" style="display: none;">
                        <asp:TextBox ID="txtContentDescDate" TextMode="MultiLine" runat="server"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">İçerik Hakkında</div>
                </div>
                <div class="card-body">
                    <asp:TextBox ID="txtContentDescription" TextMode="MultiLine" runat="server"></asp:TextBox>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">İçerik Medya</h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="input-file input-file-image">
                                <img class="img-upload-preview" width="150" src="https://placehold.it/150x150" alt="preview">
                                <asp:FileUpload ID="flMedia" runat="server" />
                                <label for="cpContent_flMedia" class="label-input-file btn btn-default btn-round">
                                    <span class="btn-label">
                                        <i class="fa fa-file-image"></i>
                                    </span>
                                    Resim Yükle
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-action">
                    <div class="row">
                        <div class="col-md-12">
                            <asp:Button ID="btnSave" class="btn btn-success" OnClick="btnSave_Click" runat="server" Text="Ekle / Kaydet" />
                            <button class="btn btn-danger">İptal</button>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphJs" runat="Server">
    <!-- Summernote -->
    <script src="../../assets/js/plugin/summernote/summernote-bs4.min.js"></script>
    <script src="assets/vendors/custom/datepicker/bootstrap-datepicker.tr.min.js" type="text/javascript"></script>
    <script>
        $('#cpContent_txtContentDescription').summernote({
            placeholder: 'İçerik Hakkında Bilgi Giriniz.',
            fontNames: ['Arial', 'Arial Black', 'Comic Sans MS', 'Courier New'],
            tabsize: 2,
            height: 300
        });
        $('#cpContent_txtContentDescDate').summernote({
            placeholder: 'Tarih Açıklaması Hakkında Bilgi Giriniz.',
            fontNames: ['Arial', 'Arial Black', 'Comic Sans MS', 'Courier New'],
            tabsize: 2,
            height: 200
        });
        
		$('#cpContent_txtSonBasvuru').datepicker({
            format: 'dd/mm/yyyy',
            clearBtn: true,
            language: "tr",
            todayHighlight: true,
            orientation: "top left",
            daysOfWeekHighlighted: "[0,6]"
		});
        $('#cpContent_txtEgitimBaslangic').datepicker({
            format: 'dd/mm/yyyy',
            clearBtn: true,
            language: "tr",
            todayHighlight: true,
            orientation: "top left",
            daysOfWeekHighlighted: "[0,6]"
		});
        $('#cpContent_txtSinavBaslangic').datepicker({
            format: 'dd/mm/yyyy',
            clearBtn: true,
            language: "tr",
            todayHighlight: true,
            orientation: "top left",
            daysOfWeekHighlighted: "[0,6]"
		});
        $('#cpContent_txtSinavBitis').datepicker({
            format: 'dd/mm/yyyy',
            clearBtn: true,
            language: "tr",
            todayHighlight: true,
            orientation: "top left",
            daysOfWeekHighlighted: "[0,6]"
        });
        $("#cpContent_indirimChk").on("click", function() {
            if (document.getElementById("cpContent_indirimChk").checked) {
                $("#cpContent_indirimDiv").removeAttr("style");
            }
            else{
                $("#cpContent_indirimDiv").attr("style", "display: none;");
            }
        });
        $("#cpContent_tarihChk").on("click", function() {
            if (document.getElementById("cpContent_tarihChk").checked) {
                $("#cpContent_tarihDiv").removeAttr("style");
            }
            else{
                $("#cpContent_tarihDiv").attr("style", "display: none;");
            }
        });
        $("#cpContent_chkOrgun").on("click", function() {
            if (document.getElementById("cpContent_chkOrgun").checked) {
                $("#cpContent_orgunDiv").removeAttr("style");
            }
            else{
                $("#cpContent_orgunDiv").attr("style", "display: none;");
            }
        });
    </script>
</asp:Content>