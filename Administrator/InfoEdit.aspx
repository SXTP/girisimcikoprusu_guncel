﻿<%@ Page Language="C#" MasterPageFile="~/Administrator/Content/MasterPage.master" ValidateRequest="false" AutoEventWireup="true" CodeFile="InfoEdit.aspx.cs" Inherits="Administrator_InfoEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCss" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpContent" runat="Server">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Haber Bilgileri</h4>
                </div>
                <div id="exampleValidation" novalidate="novalidate">
                    <div class="card-body">
                        <div class="form-group form-show-validation row">
                            <label for="name" class="col-lg-3 col-md-3 col-sm-4 mt-sm-2 text-right">Adres <span class="required-label">*</span></label>
                            <div class="col-lg-4 col-md-9 col-sm-8">
                                <asp:TextBox ID="txtAdres" class="form-control" required="" runat="server"></asp:TextBox>

                            </div>
                        </div>
                        <div class="form-group form-show-validation row">
                            <label for="name" class="col-lg-3 col-md-3 col-sm-4 mt-sm-2 text-right">Telefon <span class="required-label">*</span></label>
                            <div class="col-lg-4 col-md-9 col-sm-8">
                                <asp:TextBox ID="txtTelefon" class="form-control" required="" runat="server"></asp:TextBox>

                            </div>
                        </div>
                        <div class="form-group form-show-validation row">
                            <label for="name" class="col-lg-3 col-md-3 col-sm-4 mt-sm-2 text-right">Mail <span class="required-label">*</span></label>
                            <div class="col-lg-4 col-md-9 col-sm-8">
                                <asp:TextBox ID="txtMail" class="form-control" required="" runat="server"></asp:TextBox>

                            </div>
                        </div>
                        <div class="form-group form-show-validation row">
                            <label for="name" class="col-lg-3 col-md-3 col-sm-4 mt-sm-2 text-right">Web Sitesi <span class="required-label">*</span></label>
                            <div class="col-lg-4 col-md-9 col-sm-8">
                                <asp:TextBox ID="txtSite" class="form-control" required="" runat="server"></asp:TextBox>

                            </div>
                        </div>
                        <div class="form-group form-show-validation row">
                            <label for="name" class="col-lg-3 col-md-3 col-sm-4 mt-sm-2 text-right">Facebook <span class="required-label">*</span></label>
                            <div class="col-lg-4 col-md-9 col-sm-8">
                                <asp:TextBox ID="txtFacebook" class="form-control" required="" runat="server"></asp:TextBox>

                            </div>
                        </div>
                        <div class="form-group form-show-validation row">
                            <label for="name" class="col-lg-3 col-md-3 col-sm-4 mt-sm-2 text-right">Twitter <span class="required-label">*</span></label>
                            <div class="col-lg-4 col-md-9 col-sm-8">
                                <asp:TextBox ID="txtTwitter" class="form-control" required="" runat="server"></asp:TextBox>

                            </div>
                        </div>
                        <div class="form-group form-show-validation row">
                            <label for="name" class="col-lg-3 col-md-3 col-sm-4 mt-sm-2 text-right">İnstagram <span class="required-label">*</span></label>
                            <div class="col-lg-4 col-md-9 col-sm-8">
                                <asp:TextBox ID="txtInstagram" class="form-control" required="" runat="server"></asp:TextBox>

                            </div>
                        </div>
                        <div class="form-group form-show-validation row">
                            <label for="name" class="col-lg-3 col-md-3 col-sm-4 mt-sm-2 text-right">LinkedIn <span class="required-label">*</span></label>
                            <div class="col-lg-4 col-md-9 col-sm-8">
                                <asp:TextBox ID="txtLinkedIn" class="form-control" required="" runat="server"></asp:TextBox>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-action">
                    <div class="row">
                        <div class="col-md-12">
                            <asp:Button ID="btnSave" class="btn btn-success" OnClick="btnSave_Click" runat="server" Text="Kaydet" />
                            <button class="btn btn-danger">İptal</button>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
</asp:Content>