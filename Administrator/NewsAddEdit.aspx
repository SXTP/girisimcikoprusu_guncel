﻿<%@ Page Language="C#" MasterPageFile="~/Administrator/Content/MasterPage.master" ValidateRequest="false" AutoEventWireup="true" CodeFile="NewsAddEdit.aspx.cs" Inherits="Administrator_NewsAddEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCss" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpContent" runat="Server">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Haber Bilgileri</h4>
                </div>
                <div id="exampleValidation" novalidate="novalidate">
                    <div class="card-body">
                        <div class="form-group form-show-validation row">
                            <label for="name" class="col-lg-3 col-md-3 col-sm-4 mt-sm-2 text-right">Başlık <span class="required-label">*</span></label>
                            <div class="col-lg-4 col-md-9 col-sm-8">
                                <asp:TextBox ID="txtContentTitle" class="form-control" placeholder="Başlık Giriniz" required="" runat="server"></asp:TextBox>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">İçerik Hakkında</div>
                </div>
                <div class="card-body">
                    <asp:TextBox ID="txtContentDescription" TextMode="MultiLine" runat="server"></asp:TextBox>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Haber Banner</h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="input-file input-file-image">
                                <img id="imgBox" runat="server" class="img-upload-preview" width="150" src="https://placehold.it/150x150" alt="preview" />
                                <input type="file" class="form-control form-control-file" id="uploadImg2" name="uploadImg2" accept="image/*">
                                <asp:FileUpload ID="flMedia" runat="server" />
                                <label for="cpContent_flMedia" class="label-input-file btn btn-default btn-round">
                                    <span class="btn-label">
                                        <i class="fa fa-file-image"></i>
                                    </span>
                                    Resim Yükle
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-action">
                    <div class="row">
                        <div class="col-md-12">
                            <asp:Button ID="btnSave" class="btn btn-success" OnClick="btnSave_Click" runat="server" Text="Ekle / Kaydet" />
                            <button class="btn btn-danger">İptal</button>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphJs" runat="Server">
    <!-- Summernote -->
    <script src="../../assets/js/plugin/summernote/summernote-bs4.min.js"></script>
    <script>
        $('#cpContent_txtContentDescription').summernote({
            placeholder: 'İçerik Hakkında Bilgi Giriniz.',
            fontNames: ['Arial', 'Arial Black', 'Comic Sans MS', 'Courier New'],
            tabsize: 2,
            height: 300
        });
    </script>
</asp:Content>
