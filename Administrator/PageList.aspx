﻿<%@ Page Language="C#" MasterPageFile="~/Administrator/Content/MasterPage.master" AutoEventWireup="true" CodeFile="PageList.aspx.cs" Inherits="Administrator_PageList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCss" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpContent" runat="Server">

    <div class="row">
        <div class="col-md-12">
            <div class="m-portlet m-portlet--responsive-mobile">
                <div class="m-portlet__body">
                    <table id="add-row" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th style="width: 50%">Başlık</th>
                                <th>Oluşturan</th>
                                <th style="width: 10%">İşlemler</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Başlık</th>
                                <th>Oluşturan</th>
                                <th>İşlemler</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <asp:Repeater ID="rptList" runat="server">
                                <ItemTemplate>
                                    <tr>
                                        <td><%#Eval("PageTitle") %></td>
                                        <td><%#Eval("Users.UserName") %></td>
                                        <td>
                                            <div class="form-button-action">
                                                <a href="PageAddEdit.aspx?Id=<%#Eval("ID") %>&Pt=2" class="btn btn-link btn-primary" data-original-title="Düzenle">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                                <a href="PageAddEdit.aspx?Id=<%#Eval("ID") %>&Pt=3" class="btn btn-link btn-danger" data-original-title="Sil">
                                                    <i class="fa fa-times" style="color: white;"></i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphJs" runat="Server">
    <script>
        $(document).ready(function () {
            $('#basic-datatables').DataTable({
            });

            $('#multi-filter-select').DataTable({
                "pageLength": 5,
                initComplete: function () {
                    this.api().columns().every(function () {
                        var column = this;
                        var select = $('<select class="form-control"><option value=""></option></select>')
                            .appendTo($(column.footer()).empty())
                            .on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );

                                column
                                    .search(val ? '^' + val + '$' : '', true, false)
                                    .draw();
                            });

                        column.data().unique().sort().each(function (d, j) {
                            select.append('<option value="' + d + '">' + d + '</option>')
                        });
                    });
                }
            });

            // Add Row
            $('#add-row').DataTable({
                "pageLength": 10,
            });
        });
    </script>
</asp:Content>