﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Administrator_Ajax : System.Web.UI.Page
{
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Xml)]
    public static string BasvuruSil(string id)
    {
        using (DataEntities db = new DataEntities())
        {
            int _id = Convert.ToInt32(id);
            db.Applications.FirstOrDefault(i => i.ID == _id).DbStatus = true;
            db.SaveChanges();
        }

        return "";
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Xml)]
    public static string BasvuruGeriAl(string id)
    {
        using (DataEntities db = new DataEntities())
        {
            int _id = Convert.ToInt32(id);
            db.Applications.FirstOrDefault(i => i.ID == _id).DbStatus = false;
            db.SaveChanges();
        }

        return "";
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Xml)]
    public static string BasvuruTamamenSil(string id)
    {
        using (DataEntities db = new DataEntities())
        {
            int _id = Convert.ToInt32(id);
            Applications app = db.Applications.FirstOrDefault(i => i.ID == _id);
            db.Applications.Remove(app);
            db.SaveChanges();
        }

        return "";
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Xml)]
    public static string SayfaGeriAl(string id)
    {
        using (DataEntities db = new DataEntities())
        {
            int _id = Convert.ToInt32(id);
            db.WebPageContents.FirstOrDefault(i => i.ID == _id).DbStatus = false;
            db.SaveChanges();
        }

        return "";
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Xml)]
    public static string IcerikGeriAl(string id)
    {
        using (DataEntities db = new DataEntities())
        {
            int _id = Convert.ToInt32(id);
            db.Contents.FirstOrDefault(p => p.ID == _id).DbStatus = false;
            db.SaveChanges();
        }
        return "";
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Xml)]
    public static string BannerGeriAl(string id)
    {
        using (DataEntities db = new DataEntities())
        {
            int _id = Convert.ToInt32(id);
            db.Banners.FirstOrDefault(i => i.ID == _id).DbStatus = false;
            db.SaveChanges();
        }

        return "";
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Xml)]
    public static string HaberGeriAl(string id)
    {
        using (DataEntities db = new DataEntities())
        {
            int _id = Convert.ToInt32(id);
            db.News.FirstOrDefault(i => i.ID == _id).DbStatus = false;
            db.SaveChanges();
        }

        return "";
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Xml)]
    public static string DuyuruGeriAl(string id)
    {
        using (DataEntities db = new DataEntities())
        {
            int _id = Convert.ToInt32(id);
            db.Announcements.FirstOrDefault(i => i.ID == _id).DbStatus = false;
            db.SaveChanges();
        }

        return "";
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GetChartData()
    {
        List<ChartData> data = new List<ChartData>();
        using (DataEntities ent = new DataEntities())
        {
            DateTime firstDate = DateTime.Now.AddYears(-1).AddMonths(1);
            foreach (var item in ent.Applications.Where(p => p.CreateDate > firstDate && p.DbStatus == false))
            {
                string ayAdi = item.CreateDate.ToString("MMMM", new CultureInfo("tr-TR"));
                var _data = data.FirstOrDefault(p => p.ay == ayAdi);
                if (_data == null)
                    data.Add(new ChartData { ay = ayAdi, sayi = 1 });
                else
                    _data.sayi++;
            }
        }
        return JsonConvert.SerializeObject(data);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GetMapData()
    {
        Dictionary<string, int> data = new Dictionary<string, int>();
        using (DataEntities ent = new DataEntities())
        {
            for (int i = 1; i < 82; i++)
            {
                data.Add(i.ToString(), 0);
            }
            foreach (var item in ent.UserDetails)
            {
                var _data = data.FirstOrDefault(p => p.Key == item.AddressCityNo.ToString());
                data[_data.Key] = _data.Value + 1;
            }
        }
        return JsonConvert.SerializeObject(data);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GetAppsByCountry(string id)
    {
        int code = Convert.ToInt32(id);
        List<MapData2> data = new List<MapData2>();
        using (DataEntities db = new DataEntities())
        {
            data = db.UserDetails.Where(usd => usd.AddressCityNo == code).Select(p => new MapData2 { adSoyad = p.Users.Name + " " + p.Users.Surname }).ToList();
        }
        return JsonConvert.SerializeObject(data);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string UpdateContentSort(string _data)
    {
        var data = JsonConvert.DeserializeObject<string[,]>(_data);
        using (DataEntities db = new DataEntities())
        {
            for (int i = 0; i < data.GetLength(0); i++)
            {
                try
                {
                    var id = Convert.ToInt32(data[i, 0]);
                    var no = Convert.ToInt32(data[i, 1]);
                    db.Contents.FirstOrDefault(p => p.ID == id).SortId = no;
                }
                catch (Exception)
                {
                    return "Bir hata oluştu!";
                }
            }
            db.SaveChanges();
        }
        return "OK";
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GetMorrisChartData(string filter)
    {
        List<MorrisChartData> data = new List<MorrisChartData>();
        ToplamGoruntuleme toplam = new ToplamGoruntuleme();
        using (DataEntities db = new DataEntities())
        {
            DateTime now = DateTime.Now;
            if (filter == "all")
            {
                DateTime yesterday = DateTime.Now.AddDays(-1);
                toplam.totalVisit = db.ContentVisitors.Count();
                data = db.Contents.Where(p => p.DbStatus == false).OrderByDescending(p => p.ContentVisitors.Count()).Take(5).Select(p => new MorrisChartData
                {
                    y = p.ContentTitle,
                    a = p.ContentVisitors.Count()
                }).ToList();
            }
            else if (filter == "month")
            {
                DateTime lastMonth = DateTime.Now.AddMonths(-1);
                toplam.totalVisit = db.ContentVisitors.Where(c => DbFunctions.TruncateTime(c.CreateDate.Value).Value.Month == now.Month).Count();
                int lastTotal = db.ContentVisitors.Where(c => DbFunctions.TruncateTime(c.CreateDate.Value).Value.Month == lastMonth.Month).Count();
                toplam.oranNum = (int)((((float)(toplam.totalVisit - lastTotal)) / (lastTotal == 0 ? 1 : lastTotal)) * 100);

                data = db.Contents.Where(p => p.DbStatus == false).OrderByDescending(p => p.ContentVisitors.Where(c => DbFunctions.TruncateTime(c.CreateDate.Value).Value.Month == now.Month && DbFunctions.TruncateTime(c.CreateDate.Value).Value.Year == now.Year).Count()).Take(5).Select(p => new MorrisChartData
                {
                    y = p.ContentTitle,
                    a = p.ContentVisitors.Where(c => DbFunctions.TruncateTime(c.CreateDate.Value).Value.Month == lastMonth.Month).Count(),
                    b = p.ContentVisitors.Where(c => DbFunctions.TruncateTime(c.CreateDate.Value).Value.Month == now.Month).Count()
                }).ToList();
            }
            else if (filter == "week")
            {
                List<ContentVisitors> cv = db.ContentVisitors.ToList();
                CultureInfo culture = new CultureInfo("tr-TR");
                var weekNo = culture.Calendar.GetWeekOfYear(DateTime.Now, culture.DateTimeFormat.CalendarWeekRule, culture.DateTimeFormat.FirstDayOfWeek);
                toplam.totalVisit = cv.Where(c => culture.Calendar.GetWeekOfYear(c.CreateDate.Value, culture.DateTimeFormat.CalendarWeekRule, culture.DateTimeFormat.FirstDayOfWeek) == weekNo && c.CreateDate.Value.Year == DateTime.Now.Year).Count();
                int lastTotal = cv.Where(c => culture.Calendar.GetWeekOfYear(c.CreateDate.Value, culture.DateTimeFormat.CalendarWeekRule, culture.DateTimeFormat.FirstDayOfWeek) == weekNo - 1 && c.CreateDate.Value.Year == DateTime.Now.Year).Count();
                toplam.oranNum = (int)((((float)(toplam.totalVisit - lastTotal)) / (lastTotal == 0 ? 1 : lastTotal)) * 100);

                data = db.Contents.Where(p => p.DbStatus == false).ToList().OrderByDescending(p => p.ContentVisitors.Where(c => culture.Calendar.GetWeekOfYear(c.CreateDate.Value, culture.DateTimeFormat.CalendarWeekRule, culture.DateTimeFormat.FirstDayOfWeek) == weekNo && c.CreateDate.Value.Year == DateTime.Now.Year).Count()).Take(5).Select(p => new MorrisChartData
                {
                    y = p.ContentTitle,
                    a = p.ContentVisitors.Where(c => culture.Calendar.GetWeekOfYear(c.CreateDate.Value, culture.DateTimeFormat.CalendarWeekRule, culture.DateTimeFormat.FirstDayOfWeek) == weekNo - 1 && c.CreateDate.Value.Year == DateTime.Now.Year).Count(),
                    b = p.ContentVisitors.Where(c => culture.Calendar.GetWeekOfYear(c.CreateDate.Value, culture.DateTimeFormat.CalendarWeekRule, culture.DateTimeFormat.FirstDayOfWeek) == weekNo && c.CreateDate.Value.Year == DateTime.Now.Year).Count()
                }).ToList();
            }
            else
            {
                DateTime yesterday = DateTime.Now.AddDays(-1);
                toplam.totalVisit = db.ContentVisitors.Where(c => DbFunctions.TruncateTime(c.CreateDate.Value) == now.Date).Count();
                int lastTotal = db.ContentVisitors.Where(c => DbFunctions.TruncateTime(c.CreateDate.Value) == yesterday.Date).Count();
                toplam.oranNum = (int)((((float)(toplam.totalVisit - lastTotal)) / (lastTotal == 0 ? 1 : lastTotal)) * 100);

                data = db.Contents.Where(p => p.DbStatus == false).OrderByDescending(p => p.ContentVisitors.Where(c => DbFunctions.TruncateTime(c.CreateDate.Value) == now.Date).Count()).Take(5).Select(p => new MorrisChartData
                {
                    y = p.ContentTitle,
                    a = p.ContentVisitors.Where(c => DbFunctions.TruncateTime(c.CreateDate.Value) == yesterday.Date).Count(),
                    b = p.ContentVisitors.Where(c => DbFunctions.TruncateTime(c.CreateDate.Value) == now.Date).Count()
                }).ToList();
            }
        }
        return JsonConvert.SerializeObject(new JsonData { morris = data, toplam = toplam });
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GetPieData()
    {
        int total = 0;
        List<ChartistData> cd = new List<ChartistData>();
        List<Chartist> chartist = new List<Chartist>();
        List<string> labels = new List<string>();
        using (DataEntities db = new DataEntities())
        {
            DateTime start = DateTime.Now.AddDays(-30);
            var apps = db.Applications.Where(p => p.DbStatus == false && p.CreateDate > start);
            total = apps.Count();
            foreach (var item in apps)
            {
                if (cd.Any(p => p.Id == item.FkContentId))
                    cd.FirstOrDefault(p => p.Id == item.FkContentId).sayi++;
                else
                    cd.Add(new ChartistData { Id = item.FkContentId, sayi = 1 });
            }
            cd = cd.OrderByDescending(p => p.sayi).ToList();
            for (int i = 0; i < 5; i++)
            {
                if (i == 4)
                {
                    int _total = 0;
                    for (int j = 4; j < cd.Count; j++)
                    {
                        _total += cd[j].sayi;
                    }
                    chartist.Add(new Chartist { value = (int)(((float)_total / total) * 100), className = "custom", meta = new meta { color = "#6167e6" } });
                    labels.Add("Diğer");
                }
                else
                {
                    chartist.Add(new Chartist { value = (int)(((float)cd[i].sayi / total) * 100), className = "custom", meta = new meta { color = i == 0 ? "#34bfa3" : i == 1 ? "#ffb822" : i == 2 ? "#f4516c" : "#00c5dc" } });
                    int id = cd[i].Id;
                    labels.Add(db.Contents.FirstOrDefault(p => p.ID == id).ContentTitle);
                }
            }
        }

        return JsonConvert.SerializeObject(new PieData { chartist = chartist, total = total, labels = labels });
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Xml)]
    public static string ProjeBasvuruSil(string id)
    {
        using (DataEntities db = new DataEntities())
        {
            int _id = Convert.ToInt32(id);
            db.ProjectApplications.FirstOrDefault(i => i.ID == _id).IsDeleted = true;
            db.SaveChanges();
        }

        return "";
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Xml)]
    public static string BildirimOku(string id)
    {
        //using (DataEntities db = new DataEntities())
        //{
        //    var ids = id.Split('-');
        //    List<int> _ids = new List<int>();
        //    foreach (var item in ids)
        //    {
        //        if (item != "")
        //        {
        //            _ids.Add(Convert.ToInt32(item));
        //        }
        //    }

        //    foreach (var item in _ids)
        //    {
        //        db.Notifications.FirstOrDefault(i => i.ID == item).Readed = true;
        //    }
        //    db.SaveChanges();
        //}

        return "";
    }

    private class PieData
    {
        public List<Chartist> chartist { get; set; }
        public List<string> labels { get; set; }
        public int total { get; set; }
    }

    private class Chartist
    {
        public int value { get; set; }
        public string className { get; set; }
        public meta meta { get; set; }
    }
    public class meta
    {
        public string color { get; set; }
    }

    private class ChartistData
    {
        public int Id { get; set; }
        public int sayi { get; set; }
    }

    private class ChartData
    {
        public string ay { get; set; }
        public int sayi { get; set; }
    }

    private class MapData
    {
        public string plaka { get; set; }
        public int sayi { get; set; }
    }

    private class MorrisChartData
    {
        public string y { get; set; }
        public int a { get; set; }
        public int b { get; set; }
    }

    private class ToplamGoruntuleme
    {
        public int oranNum { get; set; }
        public int totalVisit { get; set; }
    }

    private class JsonData
    {
        public List<MorrisChartData> morris { get; set; }
        public ToplamGoruntuleme toplam { get; set; }
    }

    private class MapData2
    {
        public string adSoyad { get; set; }
    }
}