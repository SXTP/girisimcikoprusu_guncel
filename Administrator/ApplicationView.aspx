﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Administrator/Content/MasterPage.master" ValidateRequest="false" AutoEventWireup="true" CodeFile="ApplicationView.aspx.cs" Inherits="ApplicationView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCss" runat="Server">
    <style>
        .baslik {
            font-size: .85rem;
            font-weight: 500;
            color: #a4a6ae;
        }

        .kutucuk {
            padding-bottom: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpContent" runat="Server">
    <div class="row">
        <div class="col-md-8">
            <div class="m-portlet">
                <div class="m-portlet__body">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a id="bilgiLink" class="nav-link active" data-toggle="tab" href="#m_tabs_1_1">
                                <i class="la la-exclamation-circle m--font-success"></i>Başvuru Bilgileri
                            </a>
                        </li>
                        <li class="nav-item">
                            <a id="duzenleLink" class="nav-link" data-toggle="tab" href="#m_tabs_1_2">
                                <i class="la la-pencil"></i>Düzenle
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="m_tabs_1_1" role="tabpanel" style="text-align: center;">
                            <div class="col-md-12 kutucuk">
                                <div class="form-group form-group-default">
                                    <div class="col-xs-12 baslik">
                                        Başvurulan İçerik
                                    </div>
                                    <div class="col-xs-12">
                                        <b><a runat="server" id="cntId" style="color: palevioletred;">
                                            <asp:Literal ID="ltrContentTitle" runat="server"></asp:Literal></a></b>
                                    </div>
                                </div>
                            </div>
                            <div class="row kutucuk">
                                <div class="col-md-6">
                                    <div class="form-group form-group-default">
                                        <div class="col-xs-12 baslik">
                                            Başvuru Tarihi
                                        </div>
                                        <div class="col-xs-12">
                                            <b>
                                                <asp:Literal ID="ltrCreateDate" runat="server"></asp:Literal></b>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-group-default">
                                        <div class="col-xs-12 baslik">
                                            Başvuru Durumu
                                        </div>
                                        <div class="col-xs-12">
                                            <b><span runat="server" id="ltrStatus"></span></b>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row kutucuk">
                                <div class="col-md-6">
                                    <div class="form-group form-group-default">
                                        <div class="col-xs-12 baslik">
                                            Eğitime Başvurulan İl
                                        </div>
                                        <div class="col-xs-12">
                                            <b>
                                                <asp:Literal ID="ltrBasvuruIl" runat="server"></asp:Literal></b>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-group-default">
                                        <div class="col-xs-12 baslik">
                                            Üniversite
                                        </div>
                                        <div class="col-xs-12">
                                            <b>
                                                <asp:Literal ID="ltrUni" runat="server"></asp:Literal></b>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row kutucuk">
                                <div class="col-md-6">
                                    <div class="form-group form-group-default">
                                        <div class="col-xs-12 baslik">
                                            E-mail
                                        </div>
                                        <div class="col-xs-12">
                                            <b><a id="ltrEmail" runat="server" style="color: #575962;"></a></b>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-group-default">
                                        <div class="col-xs-12 baslik">
                                            Şehir
                                        </div>
                                        <div class="col-xs-12">
                                            <b>
                                                <asp:Literal ID="ltrSehir" runat="server"></asp:Literal></b>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row kutucuk">
                                <div class="col-md-6">
                                    <div class="form-group form-group-default">
                                        <div class="col-xs-12 baslik">
                                            Girişimcilik eğitimi aldı mı?
                                        </div>
                                        <div class="col-xs-12">
                                            <b>
                                                <asp:Literal ID="ltrEgitimAldimi" runat="server"></asp:Literal></b>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-group-default">
                                        <div class="col-xs-12 baslik">
                                            Daha önce girişimcilik faaliyetinde bulundu mu?
                                        </div>
                                        <div class="col-xs-12">
                                            <b>
                                                <asp:Literal ID="ltrFaaliyet" runat="server"></asp:Literal></b>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row kutucuk">
                                <div class="col-md-6">
                                    <div class="form-group form-group-default">
                                        <div class="col-xs-12 baslik">
                                            Proje Fikri
                                        </div>
                                        <div class="col-xs-12">
                                            <b>
                                                <asp:Literal ID="ltrFikir" runat="server"></asp:Literal></b>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-group-default">
                                        <div class="col-xs-12 baslik">
                                            Projeyi hayata geçirme planı
                                        </div>
                                        <div class="col-xs-12">
                                            <b>
                                                <asp:Literal ID="ltrSurec" runat="server"></asp:Literal></b>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="m_tabs_1_2">
                            <div class="form-group m-form__group row" id="divEgitimeAktarildi" runat="server">
                                <label for="egitimeAktarildi" class="col-3 col-form-label">Eğitime Aktarıldı</label>
                                <div class="col-10">
                                    <span class="m-switch">
                                        <label>
                                            <input type="checkbox" checked="checked" name="" id="egitimeAktarildiDuzenle" runat="server" />
                                            <span></span>
                                        </label>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="adDuzenle" class="col-3 col-form-label">Ad</label>
                                <div class="col-10">
                                    <input class="form-control m-input" type="text" value="" id="adDuzenle" runat="server" />
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="soyadDuzenle" class="col-3 col-form-label">Soyad</label>
                                <div class="col-10">
                                    <input class="form-control m-input" type="text" value="" id="soyadDuzenle" runat="server" />
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="eMailDuzenle" class="col-3 col-form-label">E-mail</label>
                                <div class="col-10">
                                    <input class="form-control m-input" type="text" value="" id="eMailDuzenle" runat="server" />
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="telefonDuzenle" class="col-3 col-form-label">Telefon</label>
                                <div class="col-10">
                                    <input class="form-control m-input" type="text" value="" id="telefonDuzenle" data-mask="(999) 999 99 99" runat="server" />
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="adresDuzenle" class="col-3 col-form-label">Adres</label>
                                <div class="col-10">
                                    <input class="form-control m-input" type="text" value="" id="adresDuzenle" runat="server" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__foot" style="display: inline-block;">
                    <div id="onaylaDiv">
                        <asp:Button ID="geriAlBtn" runat="server" class="btn btn-metal" Text="Başvuru Durumunu Sıfırla" OnClick="geriAlBtn_Click" Visible="false"></asp:Button>
                        <asp:Button ID="onaylaBtn" runat="server" class="btn btn-success" Text="Onayla" OnClick="onaylaBtn_Click"></asp:Button>
                        <asp:Button ID="retBtn" runat="server" class="btn btn-danger" Text="Reddet" OnClick="retBtn_Click"></asp:Button>
                    </div>
                    <div id="kaydetDiv" style="display: none;">
                        <asp:Button ID="kaydetBtn" runat="server" class="btn btn-brand" Text="Kaydet" OnClick="kaydetBtn_Click"></asp:Button>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="m-portlet m-portlet--head-overlay m-portlet--full-height   m-portlet--rounded-force">
                <div class="m-portlet__body">
                    <div class="m-widget28">
                        <div class="m-widget28__pic m-portlet-fit--sides"></div>
                        <div class="m-widget28__container">

                            <!-- begin::Nav pills -->
                            <ul class="m-widget28__nav-items nav nav-pills nav-fill" role="tablist">
                                <li class="m-widget28__nav-item nav-item">
                                    <img src="assets/app/media/img/users/user4.jpg" class="m--img-rounded m--marginless" />
                                </li>
                            </ul>

                            <!-- begin::Tab Content -->
                            <div class="m-widget28__tab tab-content">
                                <div id="menu11" class="m-widget28__tab-container tab-pane active">
                                    <div class="m-widget28__tab-items" style="text-align: center;">
                                        <div class="m-widget28__tab-item">
                                            <span>Ad Soyad</span>
                                            <b><span runat="server" id="adSoyad"></span></b>
                                        </div>
                                        <div class="m-widget28__tab-item">
                                            <span>Yaş</span>
                                            <b><span runat="server" id="yas"></span></b>
                                        </div>
                                        <div class="m-widget28__tab-item">
                                            <span>Telefon No</span>
                                            <span runat="server" id="telefonNo"></span>
                                        </div>
                                        <div class="m-widget28__tab-item">
                                            <button class="btn btn-brand">Detaylı İncele</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- end::Tab Content -->
                        </div>
                    </div>
                </div>
            </div>
            <%--<div class="card card-profile">
                <div class="card-header" style="background-image: url('../assets/img/blogpost.jpg')">
                    <div class="profile-picture">
                        <div class="avatar avatar-xl">
                           
                            <img src=" <asp:Literal ID="ltrAppImage" runat="server"></asp:Literal>" alt="..." class="avatar-img rounded-circle">
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="user-profile text-center">
                        <div class="name">
                            <asp:Literal ID="ltrUserNameSurname" runat="server"></asp:Literal></div>
                        
										<div class="job"> <asp:Literal ID="ltrUserOld" runat="server"></asp:Literal></div>
										<div class="desc btn btn-danger btn-border btn-round">

                                            <asp:Literal ID="ltrUserPhone" runat="server"></asp:Literal></div>
                        <div class="view-profile">
                            <a href="#" class="btn btn-secondary btn-block">Detaylı İncele</a>
                        </div>
                    </div>
                </div>
            </div>--%>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphJs" runat="Server">
    <script>
        $("#bilgiLink").on("click", function () {
            $("#onaylaDiv").css("display", "block");
            $("#kaydetDiv").css("display", "none");
        });
        $("#duzenleLink").on("click", function () {
            $("#onaylaDiv").css("display", "none");
            $("#kaydetDiv").css("display", "block");
        });
        $("#cpContent_telefonDuzenle").inputmask("mask", { mask: "(999) 999-9999" });
    </script>
</asp:Content>

