﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Administrator/Content/MasterPage.master" AutoEventWireup="true" CodeFile="ProjectApplication.aspx.cs" Inherits="Administrator_ProjectApplication" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCss" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpContent" runat="Server">
    <div class="row">
        <div class="col-md-12">
            <div class="m-portlet m-portlet--responsive-mobile">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text"></h3>
                        </div>
                    </div>
                    <%-- <div class="m-portlet__head-tools">
                        <ul class="m-portlet__nav">
                            <li class="m-portlet__nav-item">
                                <asp:LinkButton runat="server" OnClick="excelBtn_Click" CssClass="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
									<span>
										<i class="la la-file-excel-o"></i>
										<span>Excel'e Aktar</span>
									</span>
                                </asp:LinkButton>
                            </li>
                        </ul>
                    </div>--%>
                </div>
                <div class="m-portlet__body">
                    <table id="appList" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>TC Kimlik</th>
                                <th>Ad</th>
                                <th>Soyadı</th>
                                <th>Başvuru Tarihi</th>
                                <th>Proje</th>
                                <th>İşlemler</th>
                            </tr>
                        </thead>
                        <tbody>
                            <asp:Repeater ID="rptList" runat="server">
                                <ItemTemplate>
                                    <tr>
                                        <td><%#Eval("ID") %></td>
                                        <td><%#Eval("TCNo") %></td>
                                        <td><%#Eval("Ad") %></td>
                                        <td><%#Eval("Soyad") %></td>
                                        <td><%# string.Format("{0:g}", Eval("CreatedOn")) %></td>
                                        <td><a href="/uploads/projedosya/<%#Eval("DosyaUrl") %>" target="_blank"><i class="fa fa-search"></i></a></td>
                                        <td>
                                            <div class="form-button-action">
                                                <a data-id="" href="javascript:SilClick(<%#Eval("ID") %>);" class="btn btn-link btn-danger" data-original-title="Sil">
                                                    <i class="fa fa-times" style="color: white;"></i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphJs" runat="Server">
    <script>
        $(document).ready(function () {
            $('#appList').DataTable({
                "pageLength": 10,
                order: [[0, "desc"]]
            });
        });

        function SilClick(id) {
            swal({
                title: 'Emin misiniz?',
                text: "Başvuruyu silmek istediğinize emin misiniz?",
                type: 'warning',
                buttons: {
                    confirm: {
                        text: 'Sil',
                        className: 'btn btn-danger'
                    },
                    cancel: {
                        visible: true,
                        text: "İptal",
                        className: 'btn btn-success'
                    }
                }
            }).then((Delete) => {
                if (Delete) {

                    $().newAjaxAdminHTML({},
                        {
                            funcName: "ProjeBasvuruSil",
                            newData: "{'id': '" + id + "'}",
                            onSuccess: function (msg) {
                                window.location.href = "ProjectApplication.aspx";
                            }
                        });

                } else {
                    swal.close();
                }
            });
        };
    </script>
</asp:Content>

