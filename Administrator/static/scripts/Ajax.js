﻿; (function ($) {
    $.fn.newAjaxAdminHTML = function (options, args2) {
        var settings = { funcName: '', newData: {}, onSuccess: function () { }, onError: function () { }, onBeforeSend: function () { } };
        settings = jQuery.extend(options, args2 || {});
        $.ajax({
            type: "POST", url: "Ajax.aspx/" + settings.funcName,
            data: settings.newData,
            contentType: "application/json; charset=utf-8",
            dataType: "html",
            success: settings.onSuccess,
            error: settings.onError,
            beforeSend: settings.onBeforeSend
        });

    }
})(jQuery);