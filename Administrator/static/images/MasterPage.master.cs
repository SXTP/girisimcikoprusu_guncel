﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MasterPage : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (Request.RawUrl.ToLower().IndexOf("/default") > -1)
            ltrHeader.Text = "<div class=\"header\">";
        else
            ltrHeader.Text = "<div class=\"header headeractive\">";

        if (HttpContext.Current.Request.Url.Host=="artisertifika.com" || HttpContext.Current.Request.Url.Host == "www.artisertifika.com")
        {
            ltrLogo.Text = "<img src='static/images/logo.png' />";
            ltrFlogo.Text = "<img style='width: 170px;' alt='Kariyer Kampüsü Küçük Logo' src='/static/images/flogo.png' />";
        }
        else if (HttpContext.Current.Request.Url.Host == "uludagsertifika.com" || HttpContext.Current.Request.Url.Host == "www.uludagsertifika.com")
        {
            ltrLogo.Text = "<img src='static/images/logoUS.png' />";
            ltrFlogo.Text = "<img style='width: 170px;' alt='Kariyer Kampüsü Küçük Logo' src='/static/images/logoUSf.png' />";

        }
        else 
        {
            ltrLogo.Text = HttpContext.Current.Request.Url.Host;
        }

        ltrSayfaAdi.Text = HttpContext.Current.Request.Url.Host;

        //Response.Write(Request.RawUrl.ToLower());
    }
}
