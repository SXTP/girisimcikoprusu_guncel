﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using EXERT;

public partial class Login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ltrPageTitle.Text = SiteSettings.SiteName + " " + SiteSettings.AppName;
            ltrPageName.Text = SiteSettings.SiteName;
            ltrAppName.Text = SiteSettings.AppName;
        }
        
    }

    protected void btnLogin_Click(object sender, EventArgs e)
    {
        Admin pg = new Admin();
        var usr = pg.LoginController(txtUserName.Text, txtPassword.Text);
        if (usr != null)
        {
            FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(1, string.Format("{0}|{1}|{2}|{3}|{4}",
                    usr.ID,
                    usr.Name,
                    usr.Surname,
                    usr.UserName,
                    (usr.UserDetails.FirstOrDefault() != null ? usr.UserDetails.FirstOrDefault().Email : "nll")
                    ),
                    DateTime.Now,
                    DateTime.Now.AddMinutes(720),
                    chkRemember.Checked,
                    "admin");
            string encTicket = FormsAuthentication.Encrypt(authTicket);
            HttpCookie faCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encTicket);
            Context.Response.Cookies.Add(faCookie);
            Response.Redirect(Request["ReturnUrl"]);
        }
        else
        {
            lblInfo.Text = "Bilgilerinizi Kontrol Ediniz";
        }


    }
}