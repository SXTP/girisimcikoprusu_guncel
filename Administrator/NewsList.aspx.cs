﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Administrator_NewsList : Admin
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SetHeader("Tüm Haberler", "Haberler", "", "");

        rptList.DataSource = db.News.Where(x => x.DbStatus == false).ToList();
        rptList.DataBind();
    }
}