﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Administrator_PageBack : Admin
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SetHeader("Silinmiş Sayfa İşlemleri", "Silinmiş Sayfalar", "", "");
        rptList.DataSource = db.WebPageContents.Where(x => x.DbStatus == true).ToList();
        rptList.DataBind();
    }
}