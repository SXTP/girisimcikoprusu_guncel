﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Administrator/Content/MasterPage.master" AutoEventWireup="true" CodeFile="ContentList.aspx.cs" Inherits="ContentList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCss" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpContent" runat="Server">

    <div class="row">
        <div class="col-md-12">
            <div class="m-portlet m-portlet--responsive-mobile">
                <div class="m-portlet__body">
                    <table id="add-row" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th style="width: 50%">Başlık</th>
                                <th>Başvurular</th>
                                <th>Oluşturan</th>
                                <th style="width: 10%">İşlemler</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Başlık</th>
                                <th>Başvurular</th>
                                <th>Oluşturan</th>
                                <th>İşlemler</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <asp:Repeater ID="rptList" runat="server">
                                <ItemTemplate>
                                    <tr>
                                        <td><%#Eval("ContentTitle") %></td>
                                        <td><a href="AppListByContent.aspx?Id=<%#Eval("ID") %>">Başvuruları Görüntüle</a></td>
                                        <td><%#Eval("Users.UserName") %></td>
                                        <td>
                                            <div class="form-button-action">
                                                <a href="ContentAddEdit.aspx?Id=<%#Eval("ID") %>&Pt=2" class="btn btn-link btn-primary" data-original-title="Düzenle">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                                <a href="ContentAddEdit.aspx?Id=<%#Eval("ID") %>&Pt=3" class="btn btn-link btn-danger" data-original-title="Sil">
                                                    <i class="fa fa-times" style="color: white;"></i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphJs" runat="Server">
    <script>
        $(document).ready(function () {

            // Add Row
            $('#add-row').DataTable({
                "pageLength": 10,
            });
        });
    </script>
</asp:Content>

