﻿
using EXERT;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using OfficeOpenXml.Style;
using OfficeOpenXml;

public partial class Administrator_KesinBasvuruList : Admin
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SetHeader("Başvuru İşlemleri", "Tüm Başvurular", "", "");
        rptList.DataSource = db.KesinBasvurular.Where(p=> p.ID > 385).ToList();
        rptList.DataBind();
    }
    protected void excelBtn_Click(object sender, EventArgs e)
    {

        ExcelPackage excel = new ExcelPackage();
        var workSheet = excel.Workbook.Worksheets.Add("Sheet1");
        workSheet.TabColor = System.Drawing.Color.Black;
        workSheet.DefaultRowHeight = 12;

        workSheet.Row(1).Height = 20;
        workSheet.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        workSheet.Row(1).Style.Font.Bold = true;
        workSheet.Cells[1, 1].Value = "ID";
        workSheet.Cells[1, 2].Value = "T.C KİMLİK NO";
        workSheet.Cells[1, 3].Value = "ADI SOYADI";
        workSheet.Cells[1, 4].Value = "DOĞUM YILI";
        workSheet.Cells[1, 5].Value = "TELEFON NO";
        workSheet.Cells[1, 6].Value = "E-POSTA";
        workSheet.Cells[1, 7].Value = "PROGRAMA KATILACAĞI İL ";
        workSheet.Cells[1, 8].Value = "BAŞVURU TARİHİ";


        //Workbook workBook = new Workbook();
        //Worksheet workSheet = new Worksheet("Basvurular");

        var basvurular = db.KesinBasvurular.Where(p => p.ID > 385).ToList();
        int recordIndex = 2;
        foreach (var item in basvurular)
        {
            workSheet.Cells[recordIndex, 1].Value = item.ID;
            workSheet.Cells[recordIndex, 2].Value = item.TCNo;
            workSheet.Cells[recordIndex, 3].Value = item.Users.Name +" "+ item.Users.Surname;
            var UserDetails = db.UserDetails.Where(x => x.FkUserId == item.Users.ID).FirstOrDefault();
            workSheet.Cells[recordIndex, 4].Value = UserDetails.BirthYear;
            workSheet.Cells[recordIndex, 5].Value = UserDetails.PhoneNumber;
            workSheet.Cells[recordIndex, 6].Value = UserDetails.Email;
            workSheet.Cells[recordIndex, 7].Value = UserDetails.AddressCity;
            workSheet.Cells[recordIndex, 8].Value = UserDetails.CreateDate.ToString();

            recordIndex++;
        }
        workSheet.Column(1).AutoFit();
        workSheet.Column(2).AutoFit();
        workSheet.Column(3).AutoFit();
        workSheet.Column(4).AutoFit();
        workSheet.Column(5).AutoFit();
        workSheet.Column(6).AutoFit();
        workSheet.Column(7).AutoFit();
        workSheet.Column(8).AutoFit();
        workSheet.Column(9).AutoFit();
        workSheet.Column(10).AutoFit();
        workSheet.Column(11).AutoFit();
        workSheet.Column(12).AutoFit();
        workSheet.Column(13).AutoFit();
        workSheet.Column(14).AutoFit();
        string excelName = "KesinKayıtListesi";
        using (var memoryStream = new MemoryStream())
        {
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment; filename=" + excelName + ".xlsx");
            excel.SaveAs(memoryStream);
            memoryStream.WriteTo(Response.OutputStream);
            Response.Flush();
            Response.End();
        }
    }
    protected void rptList_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        KesinBasvurular app = (KesinBasvurular)e.Item.DataItem;
        Literal dogum = (Literal)e.Item.FindControl("tblDogum");
        Literal uni = (Literal)e.Item.FindControl("tblUni");

        dogum.Text = app.Users.UserDetails.FirstOrDefault().BirthYear.ToString();
        uni.Text = app.Users.UserDetails.FirstOrDefault().Universities.name;

        Literal onayDurumu = (Literal)e.Item.FindControl("tblOnayDurumu");
    }
}