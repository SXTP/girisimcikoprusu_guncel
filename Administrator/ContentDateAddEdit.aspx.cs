﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Administrator_ContentDateAddEdit : Admin
{
    protected void Page_Load(object sender, EventArgs e)
    {

        SetHeader("Tarih İşlemleri", "Tarih Ekle / Düzenle", "", "");
        if (!IsPostBack)
        {
            if (xProcessType == 2)
            {
                var cnt = db.ContentDates.FirstOrDefault(x => x.ID == xContentId);
                if (cnt != null)
                {
                    txtEgitimBaslangic.Text = cnt.ContentStartDate.ToShortDateString();
                    txtSinavBaslangic.Text = cnt.ExamStartDate.ToShortDateString();
                    txtSinavBitis.Text = cnt.ExamFinishDate.ToShortDateString();
                    txtSonBasvuru.Text = cnt.AppEndDate.ToShortDateString();
                    txtEgitimBaslangicSaat.Text = cnt.ContentStartDate.ToShortTimeString();
                    txtSinavBaslangicSaat.Text = cnt.ExamStartDate.ToShortTimeString();
                    txtSinavBitisSaat.Text = cnt.ExamFinishDate.ToShortTimeString();
                    txtSonBasvuruSaat.Text = cnt.AppEndDate.ToShortTimeString();
                }
            }
            else if (xProcessType == 3)
            {
                var cntd = db.ContentDates.FirstOrDefault(x => x.ID == xContentId);
                cntd.DbStatus = true;
                db.SaveChanges();
                ShowSuccess("Tarih Başarıyla Silindi.Listeleme sayfasına yönlendirilirken lütfen bekleyin..");
                pnlPageContent.Visible = false;
                RedirectWithJavascript("ContentDates.aspx", 2000);
            }
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (xProcessType != 2)
        {
            ContentDates cnt = new ContentDates();

            if (txtEgitimBaslangic.Text != "")
            {
                string[] parts1 = txtEgitimBaslangic.Text.Split('/');
                string[] parts2 = txtEgitimBaslangicSaat.Text.Split(':');
                DateTime newDate = new DateTime(Convert.ToInt32(parts1[2]), Convert.ToInt32(parts1[1]), Convert.ToInt32(parts1[0]), Convert.ToInt32(parts2[0]), Convert.ToInt32(parts2[1]), 0);
                cnt.ContentStartDate = newDate;
            }

            if (txtSinavBaslangic.Text != "")
            {
                string[] parts1 = txtSinavBaslangic.Text.Split('/');
                string[] parts2 = txtSinavBaslangicSaat.Text.Split(':');
                DateTime newDate = new DateTime(Convert.ToInt32(parts1[2]), Convert.ToInt32(parts1[1]), Convert.ToInt32(parts1[0]), Convert.ToInt32(parts2[0]), Convert.ToInt32(parts2[1]), 0);
                cnt.ExamStartDate = newDate;
            }
         
            if (txtSinavBitis.Text != "")
            {
                string[] parts1 = txtSinavBitis.Text.Split('/');
                string[] parts2 = txtSinavBitisSaat.Text.Split(':');
                DateTime newDate = new DateTime(Convert.ToInt32(parts1[2]), Convert.ToInt32(parts1[1]), Convert.ToInt32(parts1[0]), Convert.ToInt32(parts2[0]), Convert.ToInt32(parts2[1]), 0);
                cnt.ExamFinishDate = newDate;
            }

            if (txtSonBasvuru.Text != "")
            {
                string[] parts1 = txtSonBasvuru.Text.Split('/');
                string[] parts2 = txtSonBasvuruSaat.Text.Split(':');
                DateTime newDate = new DateTime(Convert.ToInt32(parts1[2]), Convert.ToInt32(parts1[1]), Convert.ToInt32(parts1[0]), Convert.ToInt32(parts2[0]), Convert.ToInt32(parts2[1]), 0);
                cnt.AppEndDate = newDate;
            }
            cnt.CreateDate = DateTime.Now;
            db.ContentDates.Add(cnt);
            db.SaveChanges();
            ShowSuccess("Tarih Başarıyla Eklendi.Listeleme sayfasına yönlendirilirken lütfen bekleyin..");
            pnlPageContent.Visible = false;
            RedirectWithJavascript("ContentDates.aspx", 2000);
        }
        else
        {
            var cnt = db.ContentDates.FirstOrDefault(x => x.ID == xContentId);

            if (txtEgitimBaslangic.Text != "")
            {
                string[] parts1 = txtEgitimBaslangic.Text.Split('/');
                string[] parts2 = txtEgitimBaslangicSaat.Text.Split(':');
                DateTime newDate = new DateTime(Convert.ToInt32(parts1[2]), Convert.ToInt32(parts1[1]), Convert.ToInt32(parts1[0]), Convert.ToInt32(parts2[0]), Convert.ToInt32(parts2[1]), 0);
                cnt.ContentStartDate = newDate;
            }

            if (txtSinavBaslangic.Text != "")
            {
                string[] parts1 = txtSinavBaslangic.Text.Split('/');
                string[] parts2 = txtSinavBaslangicSaat.Text.Split(':');
                DateTime newDate = new DateTime(Convert.ToInt32(parts1[2]), Convert.ToInt32(parts1[1]), Convert.ToInt32(parts1[0]), Convert.ToInt32(parts2[0]), Convert.ToInt32(parts2[1]), 0);
                cnt.ExamStartDate = newDate;
            }

            if (txtSinavBitis.Text != "")
            {
                string[] parts1 = txtSinavBitis.Text.Split('/');
                string[] parts2 = txtSinavBitisSaat.Text.Split(':');
                DateTime newDate = new DateTime(Convert.ToInt32(parts1[2]), Convert.ToInt32(parts1[1]), Convert.ToInt32(parts1[0]), Convert.ToInt32(parts2[0]), Convert.ToInt32(parts2[1]), 0);
                cnt.ExamFinishDate = newDate;
            }

            if (txtSonBasvuru.Text != "")
            {
                string[] parts1 = txtSonBasvuru.Text.Split('/');
                string[] parts2 = txtSonBasvuruSaat.Text.Split(':');
                DateTime newDate = new DateTime(Convert.ToInt32(parts1[2]), Convert.ToInt32(parts1[1]), Convert.ToInt32(parts1[0]), Convert.ToInt32(parts2[0]), Convert.ToInt32(parts2[1]), 0);
                cnt.AppEndDate = newDate;
            }

            db.SaveChanges();
            ShowSuccess("Değişiklikler Başarıyla Kaydedildi.Listeleme sayfasına yönlendirilirken lütfen bekleyin..");
            pnlPageContent.Visible = false;
            RedirectWithJavascript("ContentDates.aspx", 2000);
        }

    }
}