﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EXERT;
using System.Web.Security;

public partial class Content_MasterPage : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        year.Text = DateTime.Now.Year.ToString();
        using (DataEntities ent = new DataEntities())
        {
            var list = from a in ent.AdminLinks
                       where a.DbStatus == false && a.UpId == 0
                       orderby a.LinkNo ascending
                       select a;
            rptMenu.DataSource = list.ToList<AdminLinks>();
            rptMenu.DataBind();


            ltrUserNameAndSurname.Text = User.GetUserNameAndSurname();
            //ltrUserNameAndSurname2.Text = User.GetUserNameAndSurname();

            if (User.IsLoggedIn == false)
            {
                FormsAuthentication.RedirectToLoginPage();
            }

            int userId = User.GetUserId();
            var notifications = ent.Notifications.Where(n => n.UserID == userId && n.Readed == false).OrderByDescending(n => n.Time).Take(5).ToList();
            if (notifications.Count == 0)
            {
                notifCount.Visible = false;
                notifCount2.Visible = false;
                bildirimBaslik.InnerText = "Okunmamış bildiriminiz yok.";
            }
            else
            {
                //string str = "";
                //foreach (var item in notifications)
                //{
                //    str += item.ID.ToString();
                //}
                ////notifDropdown.Attributes.Add("data-notifid", str);

                notifCount.InnerText = notifications.Count.ToString();
                notifCount2.InnerText = notifications.Count.ToString();
                bildirimBaslik.InnerText = "yeni bildiriminiz var.";
                List<Notification> _notifications = notifications.Select(n => new Notification
                {
                    ID = n.ID,
                    AppId = n.AppId,
                    BIcerik = n.BContent,
                    BLink = String.Format("/Administrator/ApplicationView.aspx?Id={0}&Pt=2", n.AppId.HasValue ? n.AppId : 0),
                    BZaman = GetTime(n.Time)
                }).ToList();

                rptBildirim.DataSource = _notifications;
                rptBildirim.DataBind();
            }
        }

    }

    protected void notifDropdown_Click(object sender, EventArgs e)
    {
        //using (DataEntities db = new DataEntities())
        //{
        //    int userId = User.GetUserId();
        //    List<Notifications> nfs = db.Notifications.Where(n => n.UserID == userId).ToList();
        //    foreach (var item in nfs)
        //    {
        //        item.Readed = true;
        //    }
        //    db.SaveChanges();
        //}
    }

    protected void rptMenu_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        Repeater rptMenuAlt = (Repeater)e.Item.FindControl("rptMenuAlt");
        AdminLinks link = (AdminLinks)e.Item.DataItem;
        Literal ltrClass = (Literal)e.Item.FindControl("ltrClass");
        Literal ltrUpMenuName = (Literal)e.Item.FindControl("ltrUpMenuName");


        using (DataEntities ent = new DataEntities())
        {
            var list = from a in ent.AdminLinks
                       where a.DbStatus == false && a.UpId == link.ID
                       orderby a.LinkNo ascending
                       select a;

            if (list.ToList<AdminLinks>().Count > 0)
            {
                //ltrUpMenuName.Text = "<div class='collapse' id='" + link.LinkName + "'>";

                rptMenuAlt.DataSource = list.ToList<AdminLinks>();
                rptMenuAlt.DataBind();
                //ltrClass.Text = " <span class='caret'></span>";
            }
        }
    }

    private string GetTime(DateTime zaman)
    {
        var ts = new TimeSpan(DateTime.Now.Ticks - zaman.Ticks);
        double delta = Math.Abs(ts.TotalSeconds);
        if (delta < 60)
        {
            return ts.Seconds == 1 ? "Bir saniye önce" : ts.Seconds + " saniye önce";
        }
        if (delta < 120)
        {
            return "Bir dakika önce";
        }
        if (delta < 2700) // 45 * 60
        {
            return ts.Minutes + " dakika önce";
        }
        if (delta < 5400) // 90 * 60
        {
            return "bir saat önce";
        }
        if (delta < 86400) // 24 * 60 * 60
        {
            return ts.Hours + " saat önce";
        }
        if (delta < 172800) // 48 * 60 * 60
        {
            return "dün";
        }
        else // 30 * 24 * 60 * 60
        {
            return ts.Days + " gün önce";
        }
    }

    public class Notification
    {
        public int ID { get; set; }
        public int? AppId { get; set; }
        public string BLink { get; set; }
        public string BIcerik { get; set; }
        public string BZaman { get; set; }
    }
}
