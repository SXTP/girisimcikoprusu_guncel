﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Administrator_AppUnConfirm : Admin
    {
    protected void Page_Load(object sender, EventArgs e)
    {
        SetHeader("Başvuru İşlemleri", " Onaylanmayan Başvurular", "", "");
        if (!IsPostBack)
        {
            var app = db.Applications.FirstOrDefault(x => x.ID == xContentId);
            if (app != null)
            {
                ltrContentTitle.InnerText = app.Contents.ContentTitle;
                ltrContentTitle.HRef = "/Administrator/AppListByContent.aspx?Id=" + app.FkContentId;
                ltrCreateDate.Value = app.CreateDate.ToLongDateString() + " " + app.CreateDate.ToShortTimeString();
                if (app.Status == 0)
                {
                    ltrStatus.Value = "Onay Bekliyor";
                    ltrStatus.Style.Add("color", "#ffa534");
                }
                else if (app.Status == 1)
                {
                    ltrStatus.Value = "Reddedildi";
                    ltrStatus.Style.Add("color", "red");
                }
                else
                {
                    ltrStatus.Value = "Onaylandı";
                    ltrStatus.Style.Add("color", "green");
                }
                ltrUserNameSurname.Text = (app.Users.Name + " " + app.Users.Surname).ToUpper();
                ltrAppImage.Text = "https://via.placeholder.com/50x50";
                ltrUserOld.Text = (DateTime.Now.Year - app.Users.UserDetails.OrderByDescending(x => x.ID).FirstOrDefault().BirthYear).ToString() + " Yaşında";
                ltrUserPhone.InnerText = app.Users.UserDetails.OrderByDescending(x => x.ID).FirstOrDefault().PhoneNumber.ToString();
                ltrUserPhone.HRef = "tel:" + app.Users.UserDetails.OrderByDescending(x => x.ID).FirstOrDefault().PhoneNumber.ToString();
                ltrBasvuruIl.Value = app.ContentField1 == null ? "Örgün" : app.ContentField1;
                ltrBasvuruGrup.Value = app.Groups == null ? "" : app.Groups.GroupName;
                ltrAdres.Value = app.Users.UserDetails.ToList().LastOrDefault().Address;
                ltrEmail.Value = app.Users.UserDetails.ToList().LastOrDefault().Email;
                ltrSehir.Value = app.Users.UserDetails.ToList().LastOrDefault().AddressCity;
                onaylaBtn.Attributes.Add("data-id", app.ID.ToString());
            }
        }
    }
    protected void retBtn_Click(object sender, EventArgs e)
    {
        var app = db.Applications.FirstOrDefault(x => x.ID == xContentId);
        app.Status = 1;
        db.SaveChanges();


        ShowSuccess("Başvuru Reddedildi.");
        pnlPageContent.Visible = false;
        RedirectWithJavascript("ApplicationView.aspx?Id=" + app.ID + "&Pt=2", 2000);
    }
}