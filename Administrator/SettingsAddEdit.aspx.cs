﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Administrator_SettingsAddEdit : Admin
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SetHeader("Ayar İşlemleri", "Ayar Ekle / Düzenle", "", "");
        if (!IsPostBack)
        {
            if (xProcessType == 2)
            {
                var setting = db.Settings.FirstOrDefault(x => x.ID == xContentId);
                if (setting != null)
                {
                    txtContentTitle.Text = setting.SettingName;
                    txtContentDescription.Text = setting.SettingValue;
                    txtContentType.Text = setting.SettingType;
                }
            }
            else if (xProcessType == 3)
            {
                var setting = db.Settings.FirstOrDefault(x => x.ID == xContentId);
                setting.DbStatus = true;
                db.SaveChanges();
                ShowSuccess("Ayar Başarıyla Silindi.Listeleme sayfasına yönlendirilirken lütfen bekleyin..");
                pnlPageContent.Visible = false;
                RedirectWithJavascript("SettingList.aspx", 2000);
            }
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (xProcessType != 2)
        {
            Settings setting = new Settings();
            setting.SettingName = txtContentTitle.Text;
            setting.SettingValue = txtContentDescription.Text;
            setting.SettingType = txtContentType.Text;
            setting.CreateDate = DateTime.Now;
            setting.DbStatus = false;
            setting.FkCreated = EXERT.User.GetUserId();
            setting.IsDeletable = true;
            setting.IsEditable = true;
            setting.IsListed = true;
            setting.SettingType = "";
            string FilePath = "~/uploads/" + Guid.NewGuid() + ".png";
            if (flMedia.HasFile == true)
            {
                flMedia.SaveAs(Server.MapPath(FilePath));
                setting.SettingValue = FilePath.Replace("~", "");
            }

            db.Settings.Add(setting);
            db.SaveChanges();

            ShowSuccess("Ayar Başarıyla Eklendi.Listeleme sayfasına yönlendirilirken lütfen bekleyin..");
            pnlPageContent.Visible = false;
            RedirectWithJavascript("SettingList.aspx", 2000);
        }
        else
        {
            var setting = db.Settings.FirstOrDefault(x => x.ID == xContentId);
            setting.SettingName = txtContentTitle.Text;
            setting.SettingValue = txtContentDescription.Text;
            setting.SettingType = txtContentType.Text;
            setting.CreateDate = DateTime.Now;
            setting.DbStatus = false;
            setting.FkCreated = EXERT.User.GetUserId();
            setting.ModifiedDate = DateTime.Now;
            string FilePath = "~/uploads/" + Guid.NewGuid() + ".png";
            if (flMedia.HasFile == true)
            {
                flMedia.SaveAs(Server.MapPath(FilePath));
                setting.SettingValue = FilePath.Replace("~", "");
            }

            db.SaveChanges();

            ShowSuccess("Değişiklikler Başarıyla Kaydedildi.Listeleme sayfasına yönlendirilirken lütfen bekleyin..");
            pnlPageContent.Visible = false;
            RedirectWithJavascript("SettingList.aspx", 2000);
        }

    }
}