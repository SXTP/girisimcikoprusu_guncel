﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Administrator/Content/MasterPage.master" CodeFile="ApplicationBack.aspx.cs" Inherits="Administrator_ApplicationBack" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCss" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpContent" runat="Server">

    <div class="row">
        <div class="col-md-12">
            <div class="m-portlet m-portlet--responsive-mobile">
                <div class="m-portlet__body">
                    <table id="add-row" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Kimlik Numarası</th>
                                    <th style="width: 30%">Adı Soyadı</th>
                                    <th style="width: 30%">Başvurulan İçerik</th>
                                    <th>Başvuru Tarihi</th>
                                    <th style="width: 10%">İşlemler</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Kimlik Numarası</th>
                                    <th>Adı Soyadı</th>
                                    <th>Başvurulan İçerik</th>
                                    <th>Başvuru Tarihi</th>
                                    <th>İşlemler</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                <asp:Repeater ID="rptList" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                            <td><%#Eval("Users.UserName") %></td>
                                            <td><%#Eval("Users.Name") %> <%#Eval("Users.Surname") %></td>
                                            <td><%#Eval("Contents.ContentTitle") %></td>
                                            <td><%#Eval("CreateDate") %></td>
                                            <td>
                                                <div class="form-button-action" style="color: white;">
                                                    <a data-id="<%#Eval("ID") %>" onclick="GeriAlClick(this);" class="btn btn-link btn-brand" data-original-title="Geri Al">
                                                        <i class="fa fa-arrow-up"></i>
                                                    </a>
                                                    <a data-id="<%#Eval("ID") %>" onclick="Sil(this);" class="btn btn-link btn-danger" data-original-title="Sil">
                                                        <i class="fa fa-times"></i>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>
                        </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphJs" runat="Server">
    <script>
        $(document).ready(function () {

            // Add Row
            $('#add-row').DataTable({
                "pageLength": 10,
            });

            var action = '<td> <div class="form-button-action"> <button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-primary btn-lg" data-original-title="Edit Task"> <i class="fa fa-edit"></i> </button> <button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove"> <i class="fa fa-times"></i> </button> </div> </td>';

            $('#addRowButton').click(function () {
                $('#add-row').dataTable().fnAddData([
                    $("#addName").val(),
                    $("#addPosition").val(),
                    $("#addOffice").val(),
                    action
                ]);
                $('#addRowModal').modal('hide');

            });
        });

        function GeriAlClick(elem) {
            swal({
				title: 'Emin misiniz?',
				text: "Başvuruyu geri almak istediğinize emin misiniz?",
				type: 'warning',
				buttons:{
					confirm: {
						text : 'Geri al',
						className : 'btn btn-success'
					},
					cancel: {
                        visible: true,
                        text: "İptal",
						className: 'btn btn-danger'
					}
				}
			}).then((Delete) => {
                if (Delete) {
                    
                    var id = $(elem).data("id");
                    
                    $().newAjaxAdminHTML({},
                    {
                        funcName: "BasvuruGeriAl",
                        newData: "{'id': '" + id + "'}",
                        onSuccess: function (msg) {
                            window.location.href = "ApplicationBack.aspx";
                        }
                    });

				} else {
					swal.close();
				}
			});
        };

        function Sil(elem) {
            swal({
                title: 'Emin misiniz?',
                text: "Başvuruyu tamamen silmek istediğinize emin misiniz?",
                type: 'error',
                buttons: {
                    confirm: {
                        text: 'Sil',
                        className: 'btn btn-success'
                    },
                    cancel: {
                        visible: true,
                        text: "İptal",
                        className: 'btn btn-danger'
                    }
                }
            }).then((Delete) => {
                if (Delete) {

                    var id = $(elem).data("id");

                    $().newAjaxAdminHTML({},
                        {
                            funcName: "BasvuruTamamenSil",
                            newData: "{'id': '" + id + "'}",
                            onSuccess: function (msg) {
                                window.location.href = "ApplicationBack.aspx";
                            }
                        });

                } else {
                    swal.close();
                }
            });
        };
    </script>
</asp:Content>

