﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class ContentAddEdit : Admin
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SetHeader("İçerik İşlemleri", "İçerik Ekle / Düzenle", "", "");
        if (!IsPostBack)
        {
            if (xProcessType == 2)
            {
                var cnt = db.Contents.FirstOrDefault(x => x.ID == xContentId);
                if (cnt != null)
                {
                    txtContentTitle.Text = cnt.ContentTitle;
                    txtContentPrice.Text = cnt.ContentPrice.ToString();
                    txtContentKeywords.Text = cnt.ContentKeywords;
                    txtContentDescSEO.Text = cnt.ContentDescriptionSEO;
                    txtContentDescription.Text = cnt.ContentDescription;
                    txtContentDescLite.Text = cnt.ContentDescriptionLite;
                    txtContentDescDate.Text = cnt.ContentDescriptionHeader;

                    if (cnt.DiscountRate > 0)
                    {
                        indirimChk.Checked = true;
                    }
                    else if (cnt.DiscountRate == 0)
                    {
                        indirimDiv.Style.Add("display", "none");
                    }

                    if (cnt.ExamFinishDate.HasValue || cnt.ExamStartDate.HasValue || cnt.AppEndDate.HasValue || cnt.ContentStartDate.HasValue)
                    {
                        tarihChk.Checked = true;
                    }
                    else
                    {
                        tarihDiv.Style.Add("display", "none");
                    }

                    if (cnt.ContentType > 0)
                        chkOrgun.Checked = true;
                    else
                        orgunDiv.Style.Add("display", "none");

                    txtIndirimYuzde.Text = cnt.DiscountRate.ToString();
                    txtEgitimBaslangic.Text = cnt.ContentStartDate.HasValue ? cnt.ContentStartDate.Value.ToShortDateString() : "";
                    txtSinavBaslangic.Text = cnt.ExamStartDate.HasValue ? cnt.ExamStartDate.Value.ToShortDateString() : "";
                    txtSinavBitis.Text = cnt.ExamFinishDate.HasValue ? cnt.ExamFinishDate.Value.ToShortDateString() : "";
                    txtSonBasvuru.Text = cnt.AppEndDate.HasValue ? cnt.AppEndDate.Value.ToShortDateString() : "";
                    txtEgitimBaslangicSaat.Text = cnt.ContentStartTime;
                    txtSinavBaslangicSaat.Text = cnt.ExamStartTime;
                    txtSinavBitisSaat.Text = cnt.ExamFinishTime;
                    txtSonBasvuruSaat.Text = cnt.AppEndTime;
                }
            }
            else if (xProcessType == 3)
            {
                var cnt = db.Contents.FirstOrDefault(x => x.ID == xContentId);
                cnt.DbStatus = true;
                db.SaveChanges();
                ShowSuccess("İçerik Başarıyla Silindi.Listeleme sayfasına yönlendirilirken lütfen bekleyin..");
                pnlPageContent.Visible = false;
                RedirectWithJavascript("Contentlist.aspx", 2000);
            }
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (xProcessType!=2)
        {
            Contents cnt = new Contents();
            cnt.ContentTitle = txtContentTitle.Text;
            cnt.ContentPrice = Convert.ToDecimal(txtContentPrice.Text);
            cnt.CreateDate = DateTime.Now;
            cnt.DbStatus = false;
            cnt.FkCreated = 1;
            cnt.ContentKeywords = txtContentKeywords.Text;
            cnt.ContentDescriptionSEO = txtContentDescSEO.Text;
            cnt.ContentDescription = txtContentDescription.Text;
            cnt.ContentDescriptionLite = txtContentDescLite.Text;
            cnt.ContentDescriptionHeader = txtContentDescDate.Text;
            cnt.ContentType = 0;
            if (tarihChk.Checked)
            {
                if (txtEgitimBaslangic.Text != "")
                    cnt.ContentStartDate = DateTime.Parse(txtEgitimBaslangic.Text);
                if (txtEgitimBaslangicSaat.Text != "")
                    cnt.ContentStartTime = txtEgitimBaslangicSaat.Text;

                if (txtSinavBaslangic.Text != "")
                    cnt.ExamStartDate = DateTime.Parse(txtSinavBaslangic.Text);
                if (txtSinavBaslangicSaat.Text != "")
                    cnt.ExamStartTime = txtSinavBaslangicSaat.Text;

                if (txtSinavBitis.Text != "")
                    cnt.ExamFinishDate = DateTime.Parse(txtSinavBitis.Text);
                if (txtSinavBitisSaat.Text != "")
                    cnt.ExamFinishTime = txtSinavBitisSaat.Text;

                if (txtSonBasvuru.Text != "")
                    cnt.AppEndDate = DateTime.Parse(txtSonBasvuru.Text);
                if (txtSonBasvuruSaat.Text != "")
                    cnt.AppEndTime = txtSonBasvuruSaat.Text;
            }
            else
            {
                cnt.ContentStartDate = null;
                cnt.ExamStartDate = null;
                cnt.ExamFinishDate = null;
                cnt.AppEndDate = null;
            }

            if (indirimChk.Checked)
                cnt.DiscountRate = int.Parse(txtIndirimYuzde.Text);
            else
                cnt.DiscountRate = 0;

            if (chkOrgun.Checked)
                cnt.ContentType = 1;
            else
                cnt.ContentType = 0;

            string FilePath = "~/uploads/" + Guid.NewGuid() + ".png";
            if (flMedia.HasFile == true)
            {
                flMedia.SaveAs(Server.MapPath(FilePath));
                cnt.ContentBanner = FilePath.Replace("~", "");

            }

            db.Contents.Add(cnt);
            db.SaveChanges();

            ShowSuccess("İçerik Başarıyla Eklendi.Listeleme sayfasına yönlendirilirken lütfen bekleyin..");
            pnlPageContent.Visible = false;
            RedirectWithJavascript("Contentlist.aspx", 2000);
        }
        else
        {
            var cnt = db.Contents.FirstOrDefault(x => x.ID == xContentId);

            cnt.ContentTitle = txtContentTitle.Text;
            cnt.ContentPrice = Convert.ToDecimal(txtContentPrice.Text);
            cnt.CreateDate = DateTime.Now;
            cnt.DbStatus = false;
            cnt.FkCreated = 1;
            cnt.ContentKeywords = txtContentKeywords.Text;
            cnt.ContentDescriptionSEO = txtContentDescSEO.Text;
            cnt.ContentDescription = txtContentDescription.Text;
            cnt.ContentDescriptionLite = txtContentDescLite.Text;
            cnt.ContentDescriptionHeader = txtContentDescDate.Text;

            if (tarihChk.Checked)
            {
                if (txtEgitimBaslangic.Text != "")
                    cnt.ContentStartDate = DateTime.Parse(txtEgitimBaslangic.Text);
                if (txtEgitimBaslangicSaat.Text != "")
                    cnt.ContentStartTime = txtEgitimBaslangicSaat.Text;

                if (txtSinavBaslangic.Text != "")
                    cnt.ExamStartDate = DateTime.Parse(txtSinavBaslangic.Text);
                if (txtSinavBaslangicSaat.Text != "")
                    cnt.ExamStartTime = txtSinavBaslangicSaat.Text;

                if (txtSinavBitis.Text != "")
                    cnt.ExamFinishDate = DateTime.Parse(txtSinavBitis.Text);
                if (txtSinavBitisSaat.Text != "")
                    cnt.ExamFinishTime = txtSinavBitisSaat.Text;

                if (txtSonBasvuru.Text != "")
                    cnt.AppEndDate = DateTime.Parse(txtSonBasvuru.Text);
                if (txtSonBasvuruSaat.Text != "")
                    cnt.AppEndTime = txtSonBasvuruSaat.Text;
            }
            else
            {
                cnt.ContentStartDate = null;
                cnt.ExamStartDate = null;
                cnt.ExamFinishDate = null;
                cnt.AppEndDate = null;
            }

            if (indirimChk.Checked)
                cnt.DiscountRate = int.Parse(txtIndirimYuzde.Text);
            else
                cnt.DiscountRate = 0;

            if (chkOrgun.Checked)
                cnt.ContentType = 1;
            else
                cnt.ContentType = 0;

            string FilePath = "~/uploads/" + Guid.NewGuid() + ".png";

            if (flMedia.HasFile == true)
            {
                flMedia.SaveAs(Server.MapPath(FilePath));
                cnt.ContentBanner = FilePath.Replace("~", "");

            }

            db.SaveChanges();

            ShowSuccess("Değişiklikler Başarıyla Kaydedildi.Listeleme sayfasına yönlendirilirken lütfen bekleyin..");
            pnlPageContent.Visible = false;
            RedirectWithJavascript("Contentlist.aspx", 2000);
        }
        
    }
}