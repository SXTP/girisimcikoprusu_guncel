﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Administrator/Content/MasterPage.master" ValidateRequest="false" AutoEventWireup="true" CodeFile="ContentSort.aspx.cs" Inherits="Administrator_ContentSort" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphCss" runat="Server">
    <link href="/js/jquery-ui/jquery-ui.bundle.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpContent" runat="Server">
    <div class="row" id="m_sortable_portlets">
        <div class="col-sm-4" id="row1">
            <asp:Repeater ID="content1Rpt" runat="server" OnItemDataBound="content1Rpt_ItemDataBound">
                <ItemTemplate>
                    <asp:Panel class="m-portlet m-portlet--mobile m-portlet--sortable rowItem" style="" runat="server" id="c1P">
                        <div class="m-portlet__head ui-sortable-handle" style="height: 8rem;">
					        <div class="m-portlet__head-caption">
                                <img src="<%# Eval("ContentBanner") %>" width="96" />
						        <div class="m-portlet__head-title" style="padding-left: 10px;">
							        <h3 class="m-portlet__head-text m--font-success">
								        <b style="font-size: 1rem;"><%# Eval("ContentTitle") %></b>
							        </h3>
						        </div>
					        </div>
				        </div>
				    </asp:Panel>
                </ItemTemplate>
            </asp:Repeater>
            <div class="m-portlet m-portlet--sortable-empty" style="min-height: 100px;"></div>
        </div>

        <div class="col-sm-4" id="row2">
            <asp:Repeater ID="content2Rpt" runat="server" OnItemDataBound="content2Rpt_ItemDataBound">
                <ItemTemplate>
                    <asp:Panel ID="c2P" runat="server" class="m-portlet m-portlet--mobile m-portlet--sortable rowItem" style="">
                        <div class="m-portlet__head ui-sortable-handle" style="height: 8rem;">
					        <div class="m-portlet__head-caption">
                                <img src="<%# Eval("ContentBanner") %>" width="96" />
						        <div class="m-portlet__head-title" style="padding-left: 10px;">
							        <h3 class="m-portlet__head-text m--font-success">
								        <b style="font-size: 1rem;"><%# Eval("ContentTitle") %></b>
							        </h3>
						        </div>
					        </div>
				        </div>
				    </asp:Panel>
                </ItemTemplate>
            </asp:Repeater>
            <div class="m-portlet m-portlet--sortable-empty" style="min-height: 100px;"></div>
        </div>

        <div class="col-sm-4" id="row3">
            <asp:Repeater ID="content3Rpt" runat="server" OnItemDataBound="content3Rpt_ItemDataBound">
                <ItemTemplate>
                    <asp:Panel ID="c3P" runat="server" class="m-portlet m-portlet--mobile m-portlet--sortable rowItem" style="">
                        <div class="m-portlet__head ui-sortable-handle" style="height: 8rem;">
					        <div class="m-portlet__head-caption">
                                <img src="<%# Eval("ContentBanner") %>" width="96" />
						        <div class="m-portlet__head-title" style="padding-left: 10px;">
							        <h3 class="m-portlet__head-text m--font-success">
								        <b style="font-size: 1rem;"><%# Eval("ContentTitle") %></b>
							        </h3>
						        </div>
					        </div>
				        </div>
				    </asp:Panel>
                </ItemTemplate>
            </asp:Repeater>
            <div class="m-portlet m-portlet--sortable-empty" style="min-height: 100px;"></div>
        </div>
    </div>
    <div style="width: 100%; text-align: center;">
        <b><span style="color: red;">*Lütfen tüm sütunlardaki eğitimlerin aynı sayıda olmasına dikkat ediniz!</span></b><br />
        <button type="button" id="kaydetBtn" class="btn btn-success">Kaydet</button>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphJs" runat="Server">
    <script src="/js/jquery-ui/jquery-ui.bundle.js" type="text/javascript"></script>
    <script>
        var PortletDraggable = {
            init: function () {
                $("#m_sortable_portlets").sortable({
                    connectWith: ".m-portlet__head",
                    items: ".m-portlet",
                    opacity: .8,
                    handle: ".m-portlet__head",
                    coneHelperSize: !0,
                    placeholder: "m-portlet--sortable-placeholder",
                    forcePlaceholderSize: !0,
                    tolerance: "pointer",
                    helper: "clone",
                    tolerance: "pointer",
                    forcePlaceholderSize: !0,
                    helper: "clone",
                    cancel: ".m-portlet--sortable-empty",
                    revert: 250,
                    update: function (e, t) {
                        t.item.prev().hasClass("m-portlet--sortable-empty") && t.item.prev().before(t.item);
                        var c1 = 1;
                        var c2 = 2;
                        var c3 = 3;
                        $("#row1").find(".rowItem").each(function () {
                            $(this).attr("data-no", c1);
                            c1 += 3;
                        });
                        $("#row2").find(".rowItem").each(function () {
                            $(this).attr("data-no", c2);
                            c2 += 3;
                        });
                        $("#row3").find(".rowItem").each(function () {
                            $(this).attr("data-no", c3);
                            c3 += 3;
                        });
                    }
                })
            }
        };
        jQuery(document).ready(function () {
            PortletDraggable.init();

            $("#kaydetBtn").on("click", function () {
                var arr = [];
                var rowId = 0;
                $("#row1").find(".rowItem").each(function () {
                    var no = $(this).attr("data-no");
                    var id = $(this).attr("data-id");

                    var _arr = [id, no]
                    arr.push(_arr);
                    rowId++;
                });
                $("#row2").find(".rowItem").each(function () {
                    var no = $(this).attr("data-no");
                    var id = $(this).attr("data-id");

                    var _arr = [id, no]
                    arr.push(_arr);
                    rowId++;
                });
                $("#row3").find(".rowItem").each(function () {
                    var no = $(this).attr("data-no");
                    var id = $(this).attr("data-id");

                    var _arr = [id, no]
                    arr.push(_arr);
                    rowId++;
                });


                $("#kaydetBtn").attr("disabled", "disabled");
                $("#kaydetBtn").addClass("m-btn--success m-loader m-loader--light m-loader--right");

                $().newAjaxAdminHTML({}, {
                    funcName: "UpdateContentSort",
                    newData: "{'_data': '" + JSON.stringify(arr) + "'}",
                    onSuccess: function (data) {
                        $("#kaydetBtn").removeAttr("disabled");
                        $("#kaydetBtn").removeClass("m-btn--success m-loader m-loader--light m-loader--right");

                        swal({
                            title: 'Başarılı!',
                            text: "İçerikler ayarladığınız şekilde düzenlendi.",
                            type: 'success',
                            buttons: {
                                confirm: {
                                    text: 'Tamam',
                                    className: 'btn btn-success'
                                }
                            }
                        });
                    }
                });
            });
        });
    </script>
</asp:Content>

