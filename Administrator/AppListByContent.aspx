﻿<%@ Page Language="C#" MasterPageFile="~/Administrator/Content/MasterPage.master" AutoEventWireup="true" CodeFile="AppListByContent.aspx.cs" Inherits="Administrator_AppListByContent" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCss" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpContent" runat="Server">

    <div class="row">
        <div class="col-md-12">
            <div class="m-portlet m-portlet--responsive-mobile">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
					    <div class="m-portlet__head-title">
						    <h3 class="m-portlet__head-text">
							    
						    </h3>
					    </div>
				    </div>
                    <div class="m-portlet__head-tools">
						<ul class="m-portlet__nav">
							<li class="m-portlet__nav-item">
								<asp:LinkButton runat="server" OnClick="excelBtn_Click" CssClass="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
									<span>
										<i class="la la-file-excel-o"></i>
										<span>Excel'e Aktar</span>
									</span>
								</asp:LinkButton>
							</li>
						</ul>
					</div>
                </div>
                <div class="m-portlet__body">
                    <table id="add-row" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Kimlik Numarası</th>
                                <th style="width: 30%">Adı Soyadı</th>
                                <th>Başvuru Tarihi</th>
                                <th style="width: 10%">İşlemler</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>#</th>
                                <th>Kimlik Numarası</th>
                                <th>Adı Soyadı</th>
                                <th>Başvuru Tarihi</th>
                                <th>İşlemler</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <asp:Repeater ID="rptList" runat="server">
                                <ItemTemplate>
                                    <tr>
                                        <td><%#Eval("Id") %></td>
                                        <td><%#Eval("Users.UserName") %></td>
                                        <td><%#Eval("Users.Name") %> <%#Eval("Users.Surname") %></td>
                                        <td><%#Eval("CreateDate") %></td>
                                        <td>
                                            <div class="form-button-action">
                                                <a href="ApplicationView.aspx?Id=<%#Eval("ID") %>&Pt=2" class="btn btn-link btn-primary" data-original-title="Düzenle">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                                <a data-id="<%#Eval("ID") %>" onclick="SilClick(this);" class="btn btn-link btn-danger" data-original-title="Sil">
                                                    <i class="fa fa-times" style="color: white;"></i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphJs" runat="Server">
    <script>
        $(document).ready(function () {

            // Add Row
            $('#add-row').DataTable({
                "pageLength": 10,
                order: [[0, "desc"]]
            });
        });

        function SilClick(elem) {
            swal({
				title: 'Emin misiniz?',
				text: "Başvuruyu silmek istediğinize emin misiniz?",
				type: 'warning',
				buttons:{
					confirm: {
						text : 'Sil',
						className : 'btn btn-danger'
					},
					cancel: {
                        visible: true,
                        text: "İptal",
						className: 'btn btn-success'
					}
				}
			}).then((Delete) => {
                if (Delete) {
                    
                    var id = $(elem).data("id");
                    
                    $().newAjaxAdminHTML({},
                    {
                        funcName: "BasvuruSil",
                        newData: "{'id': '" + id + "'}",
                        onSuccess: function (msg) {
                            window.location.href = "ApplicationList.aspx";
                        }
                    });

				} else {
					swal.close();
				}
			});
        };
    </script>
</asp:Content>

