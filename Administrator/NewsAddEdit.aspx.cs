﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Administrator_NewsAddEdit : Admin
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SetHeader("Haber İşlemleri", "Haber Ekle / Düzenle", "", "");
        if (!IsPostBack)
        {
            if (xProcessType == 2)
            {
                var news = db.News.FirstOrDefault(x => x.ID == xContentId);
                if (news != null)
                {
                    txtContentTitle.Text = news.NTitle;
                    txtContentDescription.Text = news.NContent;
                    imgBox.Src = news.NBanner;
                }
            }
            else if (xProcessType == 3)
            {
                var ann = db.News.FirstOrDefault(x => x.ID == xContentId);
                ann.DbStatus = true;
                db.SaveChanges();
                ShowSuccess("Haber Başarıyla Silindi.Listeleme sayfasına yönlendirilirken lütfen bekleyin..");
                pnlPageContent.Visible = false;
                RedirectWithJavascript("NewsList.aspx", 2000);
            }
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (xProcessType != 2)
        {
            News news = new News();
            news.NTitle = txtContentTitle.Text;
            news.NContent = txtContentDescription.Text;
            news.CreateDate = DateTime.Now;
            news.DbStatus = false;
            news.FkCreated = EXERT.User.GetUserId();
            string FilePath = "~/uploads/" + Guid.NewGuid() + ".png";
            if (flMedia.HasFile == true)
            {
                flMedia.SaveAs(Server.MapPath(FilePath));
            }
            news.NBanner = FilePath.Replace("~", "");

            db.News.Add(news);
            db.SaveChanges();
            ShowSuccess("Haber Başarıyla Eklendi.Listeleme sayfasına yönlendirilirken lütfen bekleyin..");
            pnlPageContent.Visible = false;
            RedirectWithJavascript("NewsList.aspx", 2000);
        }
        else
        {
            var news = db.News.FirstOrDefault(x => x.ID == xContentId);
            news.NTitle = txtContentTitle.Text;
            news.NContent = txtContentDescription.Text;
            news.CreateDate = DateTime.Now;
            news.DbStatus = false;
            news.FkCreated = EXERT.User.GetUserId();
            string FilePath = "~/uploads/" + Guid.NewGuid() + ".png";
            if (flMedia.HasFile == true)
            {
                flMedia.SaveAs(Server.MapPath(FilePath));
            }
            news.NBanner = FilePath.Replace("~", "");

            db.SaveChanges();
            ShowSuccess("Değişiklikler Başarıyla Kaydedildi.Listeleme sayfasına yönlendirilirken lütfen bekleyin..");
            pnlPageContent.Visible = false;
            RedirectWithJavascript("NewsList.aspx", 2000);
        }

    }
}