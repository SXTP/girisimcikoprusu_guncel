﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class BannerBack : Admin
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SetHeader("Silinmiş Banner İşlemleri", "Silinmiş Bannerlar", "", "");
        rptList.DataSource = db.Banners.Where(x => x.DbStatus == true).ToList();
        rptList.DataBind();
    }
}