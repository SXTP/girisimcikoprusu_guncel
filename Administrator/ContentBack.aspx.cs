﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Administrator_ContentBack : Admin
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SetHeader("Silinmiş İçerik İşlemleri", "Silinmiş İçerikler", "", "");
        rptList.DataSource = db.Contents.Where(x => x.DbStatus == true).OrderByDescending(p => p.ID).ToList();
        rptList.DataBind();
    }
}