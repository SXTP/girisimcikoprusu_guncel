﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Administrator_BannerAddEdit : Admin
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SetHeader("Banner İşlemleri", "Banner Ekle / Düzenle", "", "");
        if (!IsPostBack)
        {
            if (xProcessType == 2)
            {
                var banner = db.Banners.FirstOrDefault(x => x.ID == xContentId);
                if (banner != null)
                {
                    txtContentTitle.Text = banner.BTitle;
                    txtContentSubtitle.Text = banner.BSubtitle;
                    txtContentDescription.Text = banner.BContent;
                    imgBox.Src = banner.BBanner;
                }
            }
            else if (xProcessType == 3)
            {
                var banner = db.Banners.FirstOrDefault(x => x.ID == xContentId);
                banner.DbStatus = true;
                db.SaveChanges();
                ShowSuccess("Banner Başarıyla Silindi.Listeleme sayfasına yönlendirilirken lütfen bekleyin..");
                pnlPageContent.Visible = false;
                RedirectWithJavascript("BannerList.aspx", 2000);
            }
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (xProcessType != 2)
        {
            Banners banner = new Banners();
            banner.BTitle = txtContentTitle.Text;
            banner.BSubtitle = txtContentSubtitle.Text;
            banner.BContent = txtContentDescription.Text;
            banner.BLink = txtBannerLink.Text;
            banner.CreateDate = DateTime.Now;
            banner.FkCreated = EXERT.User.GetUserId();
            banner.DbStatus = false;
            string FilePath = "~/uploads/" + Guid.NewGuid() + ".png";
            if (flMedia.HasFile == true)
            {
                flMedia.SaveAs(Server.MapPath(FilePath));
            }
            banner.BBanner = FilePath.Replace("~", "");

            db.Banners.Add(banner);
            db.SaveChanges();
            ShowSuccess("Banner Başarıyla Eklendi.Listeleme sayfasına yönlendirilirken lütfen bekleyin..");
            pnlPageContent.Visible = false;
            RedirectWithJavascript("BannerList.aspx", 2000);
        }
        else
        {
            var banner = db.Banners.FirstOrDefault(x => x.ID == xContentId);
            banner.BTitle = txtContentTitle.Text;
            banner.BSubtitle = txtContentSubtitle.Text;
            banner.BContent = txtContentDescription.Text;
            banner.CreateDate = DateTime.Now;
            banner.FkCreated = EXERT.User.GetUserId();
            banner.DbStatus = false;
            string FilePath = "~/uploads/" + Guid.NewGuid() + ".png";
            if (flMedia.HasFile == true)
            {
                flMedia.SaveAs(Server.MapPath(FilePath));
            }
            banner.BBanner = FilePath.Replace("~", "");

            db.SaveChanges();
            ShowSuccess("Değişiklikler Başarıyla Kaydedildi.Listeleme sayfasına yönlendirilirken lütfen bekleyin..");
            pnlPageContent.Visible = false;
            RedirectWithJavascript("BannerList.aspx", 2000);
        }

    }
}