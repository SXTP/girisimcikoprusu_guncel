﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Administrator_BannerList : Admin
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SetHeader("Tüm Bannerlar", "Bannerlar", "", "");

        rptList.DataSource = db.Banners.Where(b => b.DbStatus == false).ToList();
        rptList.DataBind();
    }
}