﻿using EXERT;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class GetCreditCart : Admin
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            String[] odemeparametreleri = new String[] { "AuthCode", "Response", "HostRefNum", "ProcReturnCode", "TransId", "ErrMsg" };
            String hashparams = Request.Form.Get("HASHPARAMS");
            String hashparamsval = Request.Form.Get("HASHPARAMSVAL");
            String storekey = TransOperations.Bankstorekey;
            String paramsval = "";
            int index1 = 0, index2 = 0;
            if (!string.IsNullOrEmpty(hashparams))
            {
                do
                {
                    index2 = hashparams.IndexOf(":", index1);
                    String val = Request.Form.Get(hashparams.Substring(index1, index2 - index1)) == null ? "" : Request.Form.Get(hashparams.Substring(index1, index2 - index1));
                    paramsval += val;
                    index1 = index2 + 1;
                }
                while (index1 < hashparams.Length);
                String hashval = paramsval + storekey;         //elde edilecek hash değeri için paramsval e store key ekleniyor. (işyeri anahtarı)
                String hashparam = Request.Form.Get("HASH");

                System.Security.Cryptography.SHA1 sha = new System.Security.Cryptography.SHA1CryptoServiceProvider();
                byte[] hashbytes = System.Text.Encoding.GetEncoding("ISO-8859-9").GetBytes(hashval);
                byte[] inputbytes = sha.ComputeHash(hashbytes);

                String hash = Convert.ToBase64String(inputbytes); //Güvenlik ve kontrol amaçlı oluşturulan hash

                if (!paramsval.Equals(hashparamsval) || !hash.Equals(hashparam)) //oluşturulan hash ile gelen hash ve hash parametreleri değerleri ile ayrıştırılıp edilen edilen aynı olmalı.
                {
                    ltrError.Text = "Ödeme sistemi üzerinden gelmedi. (Hata Kodu: S1)";
                }
                else
                {
                    using (db)
                    {
                        /*
                        Response.Write("Gelen parametreler:<br><br>");
                        foreach (string x in Request.Form)
                        {
                            Response.Write(x + ": " + Request.Form[x] + "<br />");
                        }
                        Response.Write("=====<br><br>");
                        */

                        String mdStatus = Request.Form.Get("mdStatus");
                        string sResponse = Request.Form.Get("Response");
                        string sEci = Request.Form.Get("eci");
                        string sCavv = Request.Form.Get("cavv");
                        string sMd = Request.Form.Get("md");
                        string sMdErrorMsg = Request.Form.Get("mdErrorMsg");
                        string xId = Request.Form.Get("xid");
                        string sTransId = QueryStringOperations.GetStringValue("th", "x");

                        TransHistory th = db.TransHistory.Where(p => p.TransId == sTransId).FirstOrDefault();
                        if (th != null)
                        {
                            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();

                            System.Xml.XmlDeclaration dec =
                                doc.CreateXmlDeclaration("1.0", "ISO-8859-9", "yes");

                            doc.AppendChild(dec);


                            System.Xml.XmlElement cc5Request = doc.CreateElement("CC5Request");
                            doc.AppendChild(cc5Request);

                            System.Xml.XmlElement name = doc.CreateElement("Name");
                            name.AppendChild(doc.CreateTextNode("ULUSEMApi"));
                            cc5Request.AppendChild(name);

                            System.Xml.XmlElement password = doc.CreateElement("Password");
                            password.AppendChild(doc.CreateTextNode("DjgjghDjg09"));
                            cc5Request.AppendChild(password);

                            System.Xml.XmlElement clientid = doc.CreateElement("ClientId");
                            clientid.AppendChild(doc.CreateTextNode(TransOperations.Bankclientid));
                            cc5Request.AppendChild(clientid);

                            System.Xml.XmlElement ipaddress = doc.CreateElement("IPAddress");
                            ipaddress.AppendChild(doc.CreateTextNode(EXERT.User.GetUserIP));
                            cc5Request.AppendChild(ipaddress);

                            System.Xml.XmlElement orderid = doc.CreateElement("oid");
                            orderid.AppendChild(doc.CreateTextNode(sTransId));
                            cc5Request.AppendChild(orderid);

                            System.Xml.XmlElement taksit = doc.CreateElement("Taksit");
                            taksit.AppendChild(doc.CreateTextNode(th.ResponseCode));
                            cc5Request.AppendChild(taksit);

                            System.Xml.XmlElement mode = doc.CreateElement("Type");
                            mode.AppendChild(doc.CreateTextNode("Auth"));
                            cc5Request.AppendChild(mode);

                            System.Xml.XmlElement number = doc.CreateElement("Number");
                            number.AppendChild(doc.CreateTextNode(sMd));
                            cc5Request.AppendChild(number);


                            System.Xml.XmlElement total = doc.CreateElement("amount");
                            total.AppendChild(doc.CreateTextNode(string.Format("{0:0.00}", th.TotalAmount)));
                            cc5Request.AppendChild(total);

                            System.Xml.XmlElement currency = doc.CreateElement("Currency");
                            currency.AppendChild(doc.CreateTextNode("949"));
                            cc5Request.AppendChild(currency);

                            System.Xml.XmlElement payertxnid = doc.CreateElement("PayerTxnId");
                            payertxnid.AppendChild(doc.CreateTextNode(xId));
                            cc5Request.AppendChild(payertxnid);

                            System.Xml.XmlElement payersecuritylevel = doc.CreateElement("PayerSecurityLevel");
                            payersecuritylevel.AppendChild(doc.CreateTextNode(sEci));
                            cc5Request.AppendChild(payersecuritylevel);

                            System.Xml.XmlElement payerauthenticationcode = doc.CreateElement("PayerAuthenticationCode");
                            payerauthenticationcode.AppendChild(doc.CreateTextNode(sCavv));
                            cc5Request.AppendChild(payerauthenticationcode);

                            String xmlval = doc.OuterXml;

                            String url = "https://sanalpos.halkbank.com.tr/servlet/cc5ApiServer";
                            System.Net.HttpWebResponse resp = null;
                            try
                            {
                                System.Net.HttpWebRequest request = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(url);

                                string postdata = "DATA=" + xmlval.ToString();
                                byte[] postdatabytes = System.Text.Encoding.GetEncoding("ISO-8859-9").GetBytes(postdata);
                                request.Method = "POST";
                                request.ContentType = "application/x-www-form-urlencoded";
                                request.ContentLength = postdatabytes.Length;
                                System.IO.Stream requeststream = request.GetRequestStream();
                                requeststream.Write(postdatabytes, 0, postdatabytes.Length);
                                requeststream.Close();

                                resp = (System.Net.HttpWebResponse)request.GetResponse();
                                System.IO.StreamReader responsereader = new System.IO.StreamReader(resp.GetResponseStream(), System.Text.Encoding.GetEncoding("ISO-8859-9"));




                                String gelenXml = responsereader.ReadToEnd(); //Gelen xml string olarak alındı.
                                System.Xml.XmlDocument gelen = new System.Xml.XmlDocument();
                                gelen.LoadXml(gelenXml);    //string xml dökumanına çevrildi.

                                System.Xml.XmlNodeList list = gelen.GetElementsByTagName("Response");
                                String xmlResponse = list[0].InnerText;
                                list = gelen.GetElementsByTagName("AuthCode");
                                String xmlAuthCode = list[0].InnerText;
                                list = gelen.GetElementsByTagName("HostRefNum");
                                String xmlHostRefNum = list[0].InnerText;
                                list = gelen.GetElementsByTagName("ProcReturnCode");
                                String xmlProcReturnCode = list[0].InnerText;
                                list = gelen.GetElementsByTagName("TransId");
                                String xmlTransId = list[0].InnerText;
                                list = gelen.GetElementsByTagName("ErrMsg");
                                String xmlErrMsg = list[0].InnerText;


                                th.AdminApproved = 0;
                                th.Authcode = xmlAuthCode;
                                th.Hostlogkey = xmlHostRefNum;
                                th.ApprovedCode = xmlProcReturnCode;
                                th.ResponseCode = xmlResponse;
                                th.ResponseText = xmlTransId;
                                th.ResponseXML = gelenXml;
                                th.CreatedIp = EXERT.User.GetUserIP;
                                db.SaveChanges();

                                //ltrError.Text = xmlResponse;

                                if (xmlResponse.Equals("Approved"))
                                {
                                    if (xmlProcReturnCode == "00")
                                    {
                                        th.TransApproved = true;
                                        db.SaveChanges();

                                        var yn = db.Applications.Where(p => p.ID == th.KayitId).FirstOrDefault();
                                        if (yn != null)
                                        {
                                            string sEkranMesaji = @"

<table style=""font-size:14px;width:100%;margin:0px auto;padding:20px;-webkit-border-radius: 10px;-moz-border-radius: 10px;border-radius: 10px;"">
	<tbody>
		
		<tr>
			<td style=""background-color:#ffffff; padding:10px;"">
			<p>Sayın <b>##ADISOYADI##</b>,</p>

			<p>##EGITIMADI##<br />
			Durum: Başvurunuz alınmıştır.</p>

			<p>Başvurunuz onaylandıktan sonra aşadağıdaki bilgiler ile sisteme giriş yapabilirsiniz.</p>
			</td>
		</tr>
		<tr>
			<td height=""20"">&nbsp;</td>
		</tr>
		<tr>
			<td style=""font-size:20px; margin-bottom:10px"">Sisteme Giriş Bilgileriniz</td>
		</tr>
		<tr>
			<td style=""background-color:#ffffff; padding:10px; border-top:2px solid #2A51A2"">
			<p><b>Kullanıcı Adı :</b> ##KULLANICIADI##<br />
			<b>Şifre :</b> ##KULLANICISIFRE##<br />
			<br />
			<br />
			<b style=""color:red;"">***Sisteme Giriş Bilgileriniz Eğitim Başlangıç Tarihinden İtibaren Aktif Olacaktır.</b></p>
			</td>
		</tr>
		<tr>
			<td height=""20"">&nbsp;</td>
		</tr>
		<tr>
			<td style=""font-size:20px; margin-bottom:10px"">Kişisel Bilgileriniz</td>
		</tr>
		<tr>
			<td style=""background-color:#ffffff; padding:10px; border-top:2px solid #2A51A2""><b>TC Kimlik No :</b> ##KULLANICITCNO##<br />
			<b>Adı Soyadı :</b> ##ADISOYADI##<br />
			<b>Telefon :</b> ##KULLANICITEL##<br />
			<b>E-Mail :</b> ##KULLANICIEMAIL##<br />
			<b>Adres :</b> ##KULLANICIADRES##<br />
			<b>Şehir :</b> ##KULLANICISEHIR##</td>
		</tr>
		<tr>
			<td height=""20"">&nbsp;</td>
		</tr>
		<tr>
			<td height=""20"">&nbsp;</td>
		</tr>
		<tr>
			
		</tr>
	</tbody>
</table>
";
                                            string sAdiSoyadi = yn.Users.Name.ToUpper() + " " + yn.Users.Surname.ToUpper();
                                            sEkranMesaji = sEkranMesaji.Replace("##ADISOYADI##", sAdiSoyadi)
                                                .Replace("##KARTDURUM##", "<b style='color:green;'>ÖDEME BAŞARILI</b>")
                                                .Replace("##EGITIMADI##", yn.Contents.ContentTitle.TurkishUpper())
                                                .Replace("##KULLANICIADI##", yn.Users.UserName)
                                                .Replace("##KULLANICISIFRE##", yn.Users.UserName.Left(5))
                                                .Replace("##EGITIMTARIHI##", "")
                                                .Replace("##KULLANICITCNO##", yn.Users.UserName)
                                                .Replace("##KULLANICITEL##", yn.Users.UserDetails.FirstOrDefault().PhoneNumber)
                                                .Replace("##KULLANICIEMAIL##", yn.Users.UserDetails.FirstOrDefault().Email)
                                                .Replace("##KULLANICIADRES##", "")
                                                .Replace("##KULLANICISEHIR##", "");

                                            ltrBasarili.Text = sEkranMesaji;

                                            yn.Status = 4;
                                            db.SaveChanges();

                                            try
                                            {
                                                string sEkranMesaji2 = @"<div style=""background-color:#f5f5f5"">

<table style=""font-family:arial;font-size:12px;width:547px;margin:0px auto;padding:20px;-webkit-border-radius: 10px;-moz-border-radius: 10px;border-radius: 10px;"">
	<tbody>
		<tr>
			<td><font style=""font-size:20px; margin-bottom:15px"">Başvuru Sonucu</font></td>
		</tr>
		<tr>
			<td style=""background-color:#ffffff; padding:10px; border-top:2px solid #890061"">
			<p>Sayın <b>##ADISOYADI##</b>,</p>

			<p>##EGITIMADI##<br />
			Durum: Başvurunuz alınmıştır.</p>

			<p>Başvurunuz onaylandıktan sonra aşadağıdaki bilgiler ile sisteme giriş yapabilirsiniz.</p>
			</td>
		</tr>
		<tr>
			<td height=""20"">&nbsp;</td>
		</tr>
		<tr>
			<td><font style=""font-size:20px; margin-bottom:15px"">Sisteme Giriş Bilgileriniz</font></td>
		</tr>
		<tr>
			<td style=""background-color:#ffffff; padding:10px; border-top:2px solid #890061"">
			<p><b>Kullanıcı Adı :</b> ##KULLANICIADI##<br />
			<b>Şifre :</b> ##KULLANICISIFRE##<br />
			<br />
			<br />
			<b style=""color:red;"">***Sisteme Giriş Bilgileriniz Eğitim Başlangıç Tarihinden İtibaren Aktif Olacaktır.</b></p>
			</td>
		</tr>
		<tr>
			<td height=""20"">&nbsp;</td>
		</tr>
		<tr>
			<td><font style=""font-size:20px; margin-bottom:15px"">Kişisel Bilgileriniz</font></td>
		</tr>
		<tr>
			<td style=""background-color:#ffffff; padding:10px; border-top:2px solid #890061""><b>TC Kimlik No :</b> ##KULLANICITCNO##<br />
			<b>Adı Soyadı :</b> ##ADISOYADI##<br />
			<b>Telefon :</b> ##KULLANICITEL##<br />
			<b>E-Mail :</b> ##KULLANICIEMAIL##<br />
			<b>Adres :</b> ##KULLANICIADRES##<br />
			<b>Şehir :</b> ##KULLANICISEHIR##</td>
		</tr>
		<tr>
			<td height=""20"">&nbsp;</td>
		</tr>
		<tr>
			<td height=""20"">&nbsp;</td>
		</tr>
		<tr>
			
		</tr>
	</tbody>
</table>
</div>";
                                                sEkranMesaji2 = sEkranMesaji2.Replace("##ADISOYADI##", sAdiSoyadi)
                                                .Replace("#KARTDURUM#", "<b style='color:green;'>ÖDEME BAŞARILI</b>")
                                                .Replace("##EGITIMADI##", yn.Contents.ContentTitle.TurkishUpper())
                                                .Replace("##KULLANICIADI##", yn.Users.UserName)
                                                .Replace("##KULLANICISIFRE##", yn.Users.UserName.Left(5))
                                                .Replace("##EGITIMTARIHI##", "")
                                                .Replace("##KULLANICITCNO##", yn.Users.UserName)
                                                .Replace("##KULLANICITEL##", yn.Users.UserDetails.FirstOrDefault().PhoneNumber)
                                                .Replace("##KULLANICIEMAIL##", yn.Users.UserDetails.FirstOrDefault().Email)
                                                .Replace("##KULLANICIADRES##", "")
                                                .Replace("##KULLANICISEHIR##", "");

                                                string sBody = sEkranMesaji2;
                                                string sOut = string.Empty;
                                                //Mail Gönder
                                            }
                                            catch
                                            {

                                            }
                                        }
                                        else
                                        {
                                            ltrError.Text = "Ödemeniz gerçekleştirildi fakat kaydınız yapılamadı. Lütfen merkezimiz ile iletişime geçiniz.";
                                        }
                                    }
                                    else
                                    {
                                        ltrError.Text = "Ödemeniz gerçekleştirilemedi. Lütfen daha sonra tekrar deneyiniz.";
                                    }
                                }
                                else
                                {
                                    ltrError.Text = TransOperations.HataYazisi(xmlProcReturnCode, xmlResponse) + " <br><br>Hata:" + xmlErrMsg;
                                }
                            }
                            catch (Exception ex)
                            {
                                ltrError.Text = xmlval + "<br><br>Hata: " + ex.Message;
                            }

                        }


                    }
                }
            }
        }
    }
}