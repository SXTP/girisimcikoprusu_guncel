﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Administrator/Content/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCss" Runat="Server">
    <link href="../Administrator/static/css/jqvmap.css" media="screen" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cpContent" Runat="Server">
    <div class="m-portlet">
	    <div class="m-portlet__body  m-portlet__body--no-padding">
		    <div class="row m-row--no-padding m-row--col-separator-xl">
			    <div class="col-xl-4">

				    <!--begin:: Widgets/Daily Sales-->
				    <div class="m-widget14">
					    <div class="m-widget14__header m--margin-bottom-30">
						    <h3 class="m-widget14__title">
							    Başvuru Grafiği
						    </h3>
						    <span class="m-widget14__desc">
							    Son 12 ay için aylara göre başvuru dağılımı
						    </span>
					    </div>
					    <div class="m-widget14__chart" style="height:120px;">
						    <canvas id="m_chart_basvurular"></canvas>
					    </div>
				    </div>

				    <!--end:: Widgets/Daily Sales-->
			    </div>
			    <div class="col-xl-4">

				    <!--begin:: Widgets/Profit Share-->
				    <div class="m-widget14">
					    <div class="m-widget14__header">
						    <h3 class="m-widget14__title">
							    30 Günlük Başvurular
						    </h3>
						    <span class="m-widget14__desc">
							    Son 30 gün içerisinde başvuru yapılan eğitimler
						    </span>
					    </div>
					    <div class="row  align-items-center">
						    <div class="col-4">
							    <div id="asdf" class="m-widget14__chart" style="height: 160px;">
								    <div class="m-widget14__stat" id="pieTotal"></div>
							    </div>
						    </div>
						    <div class="col-8">
							    <div class="m-widget14__legends">
								    <div class="m-widget14__legend">
									    <span class="m-widget14__legend-bullet m--bg-success"></span>
									    <span class="m-widget14__legend-text" id="yuzde1"></span>
								    </div>
								    <div class="m-widget14__legend">
									    <span class="m-widget14__legend-bullet m--bg-warning"></span>
									    <span class="m-widget14__legend-text" id="yuzde2"></span>
								    </div>
								    <div class="m-widget14__legend">
									    <span class="m-widget14__legend-bullet m--bg-danger"></span>
									    <span class="m-widget14__legend-text" id="yuzde3"></span>
								    </div>
								    <div class="m-widget14__legend">
									    <span class="m-widget14__legend-bullet m--bg-accent"></span>
									    <span class="m-widget14__legend-text" id="yuzde4"></span>
								    </div>
								    <div class="m-widget14__legend">
									    <span class="m-widget14__legend-bullet m--bg-brand"></span>
									    <span class="m-widget14__legend-text" id="diger"></span>
								    </div>
							    </div>
						    </div>
					    </div>
				    </div>

				    <!--end:: Widgets/Profit Share-->
			    </div>
			    <div class="col-xl-4">
				    <div class="m-widget14">
					    <div class="m-widget14__header">
						    <h3 class="m-widget14__title">
							    Katılımcıların Şehirleri
						    </h3>
						    <span class="m-widget14__desc">
							    Katılımcıların yaşadıkları şehirler
						    </span>
					    </div>
					    <div class="row  align-items-center">
                            <div id="vmap" style="width: 100%; height: 200px;"></div>
					    </div>
				    </div>
			    </div>
		    </div>
	    </div>
    </div>
    <div class="row">
        
		<div class="col-xl-6">
			<!--begin:: Packages-->
			<div class="m-portlet  m-portlet--bordered-semi   m-portlet--full-height ">
				<div class="m-portlet__head m--padding-top-20">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<h3 class="m-portlet__head-text">
								Eğitimler ve Başvuruları
							</h3>
						</div>
					</div>
				</div>
				<div class="m-portlet__body m-portlet__body--no-padding">

					<!--begin::Widget 30-->
					<div class="m-widget30">
						<div class="m-widget_head">
							<div class="m-widget_head-owlcarousel-items owl-carousel">
                                <asp:Repeater ID="rptEgitimler" runat="server">
                                    <ItemTemplate>
                                        <div class="m-widget_head-owlcarousel-item carousel"><span><%#Eval("ContentTitle") %></span><span><%#Eval("ContentPrice") %> ₺</span></div>
                                    </ItemTemplate>
                                </asp:Repeater>
							</div>
						</div>
						<div class="m-widget_body">
							<div class="m-widget_body-owlcarousel-items owl-carousel" id="m_widget_body_owlcarousel_items">
                                <asp:Repeater ID="rptEgitimApps" runat="server" OnItemDataBound="rptEgitimApps_ItemDataBound">
                                    <ItemTemplate>
								        <div class="m-widget_body-owlcarousel-item carousel">
									        <div class="m-widget_body-items">
                                                <asp:Repeater ID="rptApps" runat="server" OnItemDataBound="rptApps_ItemDataBound">
                                                    <ItemTemplate>
										                <div class="m-widget_body-item">
											                <div class="m-widget_body-item-desc" style="width: unset;">
												                <span><a href="ApplicationView.aspx?Id=<%#Eval("ID") %>" style="color: #7b7e8a;"><%#Eval("Users.Name") %> <%#Eval("Users.Surname") %></a></span>
												                <asp:Label id="appStatus" runat="server"></asp:Label>
											                </div>
											                <div class="m-widget_body-item-price" style="font-size: 1.1rem !important;">
												                <asp:Label id="appDate" runat="server"></asp:Label>
											                </div>
										                </div>
                                                    </ItemTemplate>
                                                </asp:Repeater>
									        </div>
                                            <div id="tumunuGorDiv" runat="server" style="text-align: center; color: white;">
                                            </div>
								        </div>
                                    </ItemTemplate>
                                </asp:Repeater>
							</div>
						</div>
					</div>

					<!--end::Widget 30-->
				</div>
			</div>

			<!--end:: Packages-->
		</div>
	    <div class="col-xl-6">

		    <!--begin::Portlet-->
		    <div class="m-portlet m-portlet--tab">
			    <div class="m-portlet__head">
				    <div class="m-portlet__head-caption">
					    <div class="m-portlet__head-title">
						    <span class="m-portlet__head-icon m--hide">
							    <i class="la la-gear"></i>
						    </span>
						    <h3 class="m-portlet__head-text">
							    En Çok Görüntülenen Eğitimler
						    </h3>
					    </div>
				    </div>
					<div class="m-portlet__head-tools">
						<ul class="nav nav-pills nav-pills--brand m-nav-pills--align-right m-nav-pills--btn-pill m-nav-pills--btn-sm" role="tablist">
							<li class="nav-item m-tabs__item">
								<a id="dayTab" class="nav-link m-tabs__link active" href="javascript:;" onclick="GetChartData('day', this)" role="tab">
									Bugün
								</a>
							</li>
							<li class="nav-item m-tabs__item">
								<a id="weekTab" class="nav-link m-tabs__link" href="javascript:;" onclick="GetChartData('week', this)" role="tab">
									Bu Hafta
								</a>
							</li>
							<li class="nav-item m-tabs__item">
								<a id="monthTab"class="nav-link m-tabs__link" href="javascript:;" onclick="GetChartData('month', this)" role="tab">
									Bu Ay
								</a>
							</li>
							<li class="nav-item m-tabs__item">
								<a id="allTab" class="nav-link m-tabs__link" href="javascript:;" onclick="GetChartData('all', this)" role="tab">
									Tüm Zamanlar
								</a>
							</li>
						</ul>
					</div>
			    </div>
			    <div class="m-portlet__body">
				    <div id="m_morris_3" style="height:auto;"></div>
                    <div class="m-widget25">
                        <div class="m-widget25--progress" style="padding-top: 25px; margin-top: 15px;">
                            <div class="m-widget25__progress">
				                <div class="m-widget26" style="text-align: center;">
					                <div class="m-widget26__number" id="totalVisit">
						                    
						                <small>Toplam Görüntüleme</small>
					                </div>
				                </div>
                                <div id="toplamDiv" style="display: none;">
					                <span class="m-widget25__progress-number" id="oranNum">
					                </span>
					                <div class="m--space-10"></div>
					                <div class="progress m-progress--sm">
						                <div id="oranBar" class="progress-bar m--bg-danger" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
					                </div>
					                <span class="m-widget25__progress-sub" id="oranDesc">
					                </span>
                                </div>
				            </div>
                        </div>
                    </div>
			    </div>
		    </div>

		    <!--end::Portlet-->
	    </div>
        <div class="col-lg-6">
        <div class="m-portlet m-portlet--responsive-mobile">
		    <div class="m-portlet__head">
			    <div class="m-portlet__head-caption">
				    <div class="m-portlet__head-title">
					    <h3 class="m-portlet__head-text">
						    Son Başvurular
					    </h3>
				    </div>
			    </div>
		    </div>
            <div class="m-portlet__body">
                <table class="table table-striped- table-bordered table-hover" id="sonBasvurular">
                    <thead>
					    <tr>
						    <th>ID</th>
                            <th>Kimlik Numarası</th>
						    <th>Ad Soyad</th>
						    <th>İçerik</th>
						    <th>Tarih</th>
					    </tr>
				    </thead>
                    <tbody>
                        <asp:Repeater ID="rptSonBasvuru" runat="server" OnItemDataBound="rptSonBasvuru_ItemDataBound">
                            <ItemTemplate>
                                <tr>
                                    <td><a href="ApplicationView.aspx?Id=<%#Eval("ID") %>"><%#Eval("ID") %></a></td>
                                    <td><%#Eval("Users.UserName") %></td>
                                    <td><%#Eval("Users.Name") %> <%#Eval("Users.Surname") %></td>
                                    <td><asp:HyperLink ID="contentTitle" runat="server"></asp:HyperLink></td>
                                    <td><%#Eval("CreateDate") %></td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="m-portlet m-portlet--responsive-mobile">
		    <div class="m-portlet__head">
			    <div class="m-portlet__head-caption">
				    <div class="m-portlet__head-title">
					    <h3 class="m-portlet__head-text">
						    Başvurular
					    </h3>
				    </div>
			    </div>
		    </div>
            <div class="m-portlet__body">
                <table class="table table-striped- table-bordered table-hover" id="icerikler">
                    <thead>
					    <tr>
						    <th>İçerik</th>
                            <th>Bu Ay</th>
						    <th>Geçen Ay</th>
						    <th>Toplam</th>
					    </tr>
				    </thead>
                    <tbody>
                        <asp:Repeater ID="rptContents" runat="server" OnItemDataBound="rptContents_ItemDataBound">
                            <ItemTemplate>
                                <tr>
                                    <td><a href="AppListByContent.aspx?Id=<%#Eval("ID") %>"><%#Eval("ContentTitle") %></a></td>
                                    <td><asp:Literal runat="server" ID="buAy"></asp:Literal></td>
                                    <td><asp:Literal runat="server" ID="gecenAy"></asp:Literal></td>
                                    <td><asp:Literal runat="server" ID="toplam"></asp:Literal></td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphJs" Runat="Server">
    
    <script src="../Administrator/static/scripts/jquery.vmap.turkey.js" type="text/javascript"></script>
    <script>
        var DataTables = {
            init: function () {
                var e;
                (e = $("#sonBasvurular")).DataTable({
                    responsive: true,
                    lengthMenu: [5, 10, 25, 50],
                    pageLength: 10,
                    order: [[0, "desc"]]
                })
            }
        };

        jQuery(document).ready(function () {
            $("#sonBasvurular").DataTable({
                responsive: true,
                lengthMenu: [5, 10, 25, 50],
                pageLength: 10,
                language: {
                    lengthMenu: "Display _MENU_"
                },
                order: [[0, "desc"]]
            });
            $().newAjaxAdminHTML({},
            {
                funcName: "GetChartData",
                onSuccess: function (data) {
                    var _data = $.parseJSON($.parseJSON(data).d);
                    var aylar = new Array(0);
                    var degerler = new Array(0);
                    for (var i = 0; i < _data.length; i++) {
                        aylar.push(_data[i].ay);
                        degerler.push(_data[i].sayi);
                    }
                    if (0 != $("#m_chart_basvurular").length) {
                        var e = {
                            type: "line",
                            data: {
                                labels: aylar,
                                datasets: [{
                                    label: "Başvuru",
                                    borderColor: mApp.getColor("brand"),
                                    borderWidth: 2,
                                    pointBackgroundColor: mApp.getColor("brand"),
                                    backgroundColor: mApp.getColor("accent"),
                                    pointHoverBackgroundColor: mApp.getColor("danger"),
                                    pointHoverBorderColor: Chart.helpers.color(mApp.getColor("danger")).alpha(.2).rgbString(),
                                    data: degerler
                                }]
                            },
                            options: {
                                title: {
                                    display: false
                                },
                                tooltips: {
                                    intersect: true,
                                    mode: "nearest",
                                    xPadding: 10,
                                    yPadding: 10,
                                    caretPadding: 10
                                },
                                legend: {
                                    display: false,
                                    labels: {
                                        usePointStyle: false
                                    }
                                },
                                responsive: true,
                                maintainAspectRatio: false,
                                hover: {
                                    mode: "index"
                                },
                                scales: {
                                    xAxes: [{
                                        display: true,
                                        gridLines: true,
                                        scaleLabel: {
                                            display: false,
                                            labelString: "Ay"
                                        }
                                    }],
                                    yAxes: [{
                                        display: true,
                                        gridLines: true,
                                        scaleLabel: {
                                            display: false,
                                            labelString: "Başvuru"
                                        }
                                    }]
                                },
                                elements: {
                                    point: {
                                        radius: 3,
                                        borderWidth: 0,
                                        hoverRadius: 8,
                                        hoverBorderWidth: 2
                                    }
                                }
                            }
                        };
                        new Chart($("#m_chart_basvurular"), e)
                    }
                }
            });

            $().newAjaxAdminHTML({}, {
                funcName: "GetMapData",
                onSuccess: function (data) {
                    var _data = $.parseJSON($.parseJSON(data).d);
                    jQuery('#vmap').vectorMap(
                        {
                            map: 'turkey_tr',
                            backgroundColor: 'antiquewhite',
                            borderColor: '#818181',
                            borderOpacity: 0.25,
                            borderWidth: 1,
                            color: '#f4f3f0',
                            enableZoom: true,
                            hoverColor: '#999999',
                            values: _data,
                            hoverOpacity: null,
                            scaleColors: ['#FFFFFF', '#FF0000'],
                            selectedColor: '#c9dfaf',
                            selectedRegion: null,
                            showTooltip: true,
                            normalizeFunction: 'polynomial',
                            onLabelShow: function (event, label, code) {
                                var sehir = label[0].innerHTML;
                                var html = "<b>" + sehir + "</b><br />" + _data[code] + " katılımcı";
                                label[0].innerHTML = html;
                            },
                            onRegionClick: function (element, code, region) {
                                $().newAjaxAdminHTML({}, {
                                    funcName: "GetAppsByCountry",
                                    newData: "{'id': '" + code + "'}",
                                    onSuccess: function (data) {
                                        console.log(data);
                                    }
                                });
                            }
                        });
                }
            });

            $().newAjaxAdminHTML({}, {
                funcName: "GetPieData",
                onSuccess: function (data) {
                    var _data = $.parseJSON($.parseJSON(data).d);
                    $("#pieTotal").html(_data.total);
                    $("#yuzde1").html("%" + _data.chartist[0].value + " " + _data.labels[0]);
                    $("#yuzde2").html("%" + _data.chartist[1].value + " " + _data.labels[1]);
                    $("#yuzde3").html("%" + _data.chartist[2].value + " " + _data.labels[2]);
                    $("#yuzde4").html("%" + _data.chartist[3].value + " " + _data.labels[3]);
                    $("#diger").html("%" + _data.chartist[4].value + " Diğer");
                    new Chartist.Pie("#asdf", {
                        series: _data.chartist,
                        labels: _data.labels
                    },{
                        donut: !0,
                            donutWidth: 10,
                            showLabel: false
                    }).on("draw", function(e) {
                        if ("slice" === e.type) {
                            var t = e.element._node.getTotalLength();
                            e.element.attr({
                                "stroke-dasharray": t + "px " + t + "px"
                            });
                            var a = {
                                "stroke-dashoffset": {
                                    id: "anim" + e.index,
                                    dur: 1e3,
                                    from: -t + "px",
                                    to: "0px",
                                    easing: Chartist.Svg.Easing.easeOutQuint,
                                    fill: "freeze",
                                    stroke: e.meta.color
                                }
                            };
                            0 !== e.index && (a["stroke-dashoffset"].begin = "anim" + (e.index - 1) + ".end"),
                            e.element.attr({
                                "stroke-dashoffset": -t + "px",
                                stroke: e.meta.color
                            }),
                            e.element.animate(a, !1)
                        }
                    })
                }
            });

            GetChartData("day");
        });

        function GetChartData(filter, e) {
            $("#dayTab").removeClass("active");
            $("#weekTab").removeClass("active");
            $("#monthTab").removeClass("active");
            $("#allTab").removeClass("active");
            var labels = new Array(2);
            var kontrol = false;
            if (e == undefined || e.id == "dayTab") {
                $("#dayTab").addClass("active");
                labels = ["Dün", "Bugün"];
                $("#oranDesc").text("Dünden Bugüne Artış")
            } else if (e.id == "weekTab") {
                $(e).addClass("active");
                labels = ["Geçen Hafta", "Bu Hafta"];
                $("#oranDesc").text("Geçen Haftadan Bu Haftaya Artış")
            } else if (e.id == "monthTab") {
                $(e).addClass("active");
                labels = ["Geçen Ay", "Bu Ay"];
                $("#oranDesc").text("Geçen Aydan Bu Aya Artış")
            } else {
                $(e).addClass("active");
                labels = ["Tüm Zamanlar"];
                kontrol = true;
            }
            $("#m_morris_3").html("");

            $().newAjaxAdminHTML({}, {
                funcName: "GetMorrisChartData",
                newData: "{'filter': '" + filter + "'}",
                onSuccess: function (data) {
                    var _data = $.parseJSON($.parseJSON(data).d);
                    if (kontrol == false) {
                        new Morris.Bar({
                            element: "m_morris_3",
                            data: _data.morris,
                            xkey: "y",
                            ykeys: ["a", "b"],
                            labels: labels
                        });
                        $("#toplamDiv").css("display", "unset");
                    } else {
                        new Morris.Bar({
                            element: "m_morris_3",
                            data: _data.morris,
                            xkey: "y",
                            ykeys: ["a"],
                            labels: labels
                        });
                        $("#toplamDiv").css("display", "none");
                    }

                    $("#totalVisit").html(_data.toplam.totalVisit + "<small>Toplam Görüntüleme</small>");
                    $("#oranNum").text("%" + _data.toplam.oranNum);
                    if (_data.toplam.oranNum > 100) {
                        $("#oranBar").attr("aria-valuenow", "100");
                        $("#oranBar").css("width", "100%");
                    } else {
                        $("#oranBar").attr("aria-valuenow", _data.toplam.oranNum);
                        $("#oranBar").css("width", _data.toplam.oranNum + "%");
                    }
                }
            });
        }
    </script>
</asp:Content>

