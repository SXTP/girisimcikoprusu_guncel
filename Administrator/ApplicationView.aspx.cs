﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class ApplicationView : Admin
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SetHeader("Başvuru İşlemleri", "Başvuru Görüntüle", "", "");
        if (!IsPostBack)
        {
            var app = db.Applications.FirstOrDefault(x => x.ID == xContentId);
            if (app != null)
            {
                ltrContentTitle.Text = app.Contents.ContentTitle;
                ltrCreateDate.Text = app.CreateDate.ToString();
                int status = app.Status;
                if (status != 0)
                {
                    onaylaBtn.Visible = false;
                    retBtn.Visible = false;
                    geriAlBtn.Visible = true;
                }
                var UserDetails = db.UserDetails.Where(z => z.FkUserId == app.FkUserId).FirstOrDefault();
                ltrStatus.InnerHtml = status == 0 ? "<span style='color: #ffa534;'>Onay Bekliyor</span>" : status == 1 ? "<span style='color: palevioletred;'>Reddedildi</span>" : "<span style='color: green;'>Onaylandı</span>";
                adSoyad.InnerText = (app.Users.Name + " " + app.Users.Surname).ToUpper();
                yas.InnerText = (DateTime.Now.Year - app.Users.UserDetails.OrderByDescending(x => x.ID).FirstOrDefault().BirthYear).ToString() + " Yaşında";
                string tel = app.Users.UserDetails.OrderByDescending(x => x.ID).FirstOrDefault().PhoneNumber.ToString();
                telefonNo.InnerHtml = "<a  style='color: palevioletred;' href='tel:" + tel + "'>" + tel + "</a>";
                ltrBasvuruIl.Text = UserDetails.AddressCity;
                ltrUni.Text = UserDetails.Universities.name;
                ltrEgitimAldimi.Text = UserDetails.GirisimciEgitimiAldi;
                ltrEmail.HRef = "mailto:" + app.Users.UserDetails.ToList().LastOrDefault().Email;
                ltrEmail.InnerText = app.Users.UserDetails.ToList().LastOrDefault().Email;
                ltrSehir.Text = app.Users.UserDetails.ToList().LastOrDefault().AddressCity;
                ltrFikir.Text = app.ProjectSummary;
                ltrSurec.Text = app.ProjectPlan;
                ltrFaaliyet.Text = UserDetails.DahaOnceGirisim == true ? "Evet" : UserDetails.DahaOnceGirisim == false ? "Hayır" : "";
                cntId.HRef = "AppListByContent.aspx?Id=" + app.FkContentId;

                //Düzenleme kısmı
                divEgitimeAktarildi.Visible = app.Status == 4 ? true : false;
                egitimeAktarildiDuzenle.Checked = app.EgitimeAktarildi;
                adDuzenle.Value = app.Users.Name;
                soyadDuzenle.Value = app.Users.Surname;
                eMailDuzenle.Value = app.Users.UserDetails.ToList().LastOrDefault().Email;
                telefonDuzenle.Value = tel;
                adresDuzenle.Value = app.Users.UserDetails.ToList().LastOrDefault().Address;
            }
            if (Request.QueryString["i"] == "2")
            {
                int id = Convert.ToInt32(Request.QueryString["Id"]);
                var Basvuru = db.KesinBasvurular.FirstOrDefault(x => x.ID == id);
                var App = db.Applications.FirstOrDefault(x => x.FkUserId == Basvuru.FkUserId);

                if (App != null)
                {
                    ltrContentTitle.Text = App.Contents.ContentTitle;
                    ltrCreateDate.Text = App.CreateDate.ToString();
                    int status = App.Status;
                    if (status != 0)
                    {
                        onaylaBtn.Visible = false;
                        retBtn.Visible = false;
                        geriAlBtn.Visible = true;
                    }
                    var UserDetails = db.UserDetails.Where(z => z.FkUserId == App.FkUserId).FirstOrDefault();
                    ltrStatus.InnerHtml = status == 0 ? "<span style='color: #ffa534;'>Onay Bekliyor</span>" : status == 1 ? "<span style='color: palevioletred;'>Reddedildi</span>" : "<span style='color: green;'>Onaylandı</span>";
                    adSoyad.InnerText = (App.Users.Name + " " + App.Users.Surname).ToUpper();
                    yas.InnerText = (DateTime.Now.Year - App.Users.UserDetails.OrderByDescending(x => x.ID).FirstOrDefault().BirthYear).ToString() + " Yaşında";
                    string tel = App.Users.UserDetails.OrderByDescending(x => x.ID).FirstOrDefault().PhoneNumber.ToString();
                    telefonNo.InnerHtml = "<a  style='color: palevioletred;' href='tel:" + tel + "'>" + tel + "</a>";
                    ltrBasvuruIl.Text = UserDetails.AddressCity;
                    ltrUni.Text = UserDetails.Universities.name;
                    ltrEgitimAldimi.Text = UserDetails.GirisimciEgitimiAldi;
                    ltrEmail.HRef = "mailto:" + App.Users.UserDetails.ToList().LastOrDefault().Email;
                    ltrEmail.InnerText = App.Users.UserDetails.ToList().LastOrDefault().Email;
                    ltrSehir.Text = App.Users.UserDetails.ToList().LastOrDefault().AddressCity;
                    ltrFikir.Text = App.ProjectSummary;
                    ltrSurec.Text = App.ProjectPlan;
                    ltrFaaliyet.Text = UserDetails.DahaOnceGirisim == true ? "Evet" : UserDetails.DahaOnceGirisim == false ? "Hayır" : "";
                    cntId.HRef = "AppListByContent.aspx?Id=" + App.FkContentId;

                    //Düzenleme kısmı
                    divEgitimeAktarildi.Visible = App.Status == 4 ? true : false;
                    egitimeAktarildiDuzenle.Checked = App.EgitimeAktarildi;
                    adDuzenle.Value = App.Users.Name;
                    soyadDuzenle.Value = App.Users.Surname;
                    eMailDuzenle.Value = App.Users.UserDetails.ToList().LastOrDefault().Email;
                    telefonDuzenle.Value = tel;
                    adresDuzenle.Value = App.Users.UserDetails.ToList().LastOrDefault().Address;
                }
            }
        }
    }

    protected void retBtn_Click(object sender, EventArgs e)
    {
        var app = db.Applications.FirstOrDefault(x => x.ID == xContentId);
        app.Status = 1;
        db.SaveChanges();


        ShowSuccess("Başvuru Reddedildi.");
        pnlPageContent.Visible = false;
        RedirectWithJavascript("ApplicationView.aspx?Id=" + app.ID + "&Pt=2", 2000);
    }

    protected void kaydetBtn_Click(object sender, EventArgs e)
    {
        var app = db.Applications.FirstOrDefault(x => x.ID == xContentId);
        app.EgitimeAktarildi = egitimeAktarildiDuzenle.Checked;
        app.Users.Name = adDuzenle.Value;
        app.Users.Surname = soyadDuzenle.Value;
        var details = app.Users.UserDetails.ToList().LastOrDefault();
        details.Email = eMailDuzenle.Value;
        details.PhoneNumber = telefonDuzenle.Value;
        details.Address = adresDuzenle.Value;
        db.SaveChanges();
        ShowSuccess("Bilgiler düzenlendi.");
        pnlPageContent.Visible = false;
        RedirectWithJavascript("ApplicationView.aspx?Id=" + app.ID + "&Pt=2", 2000);
    }

    protected void onaylaBtn_Click(object sender, EventArgs e)
    {
        var app = db.Applications.FirstOrDefault(x => x.ID == xContentId);
        app.Status = 4;
        db.SaveChanges();


        ShowSuccess("Başvuru Onaylandı.");
        pnlPageContent.Visible = false;
        RedirectWithJavascript("ApplicationView.aspx?Id=" + app.ID + "&Pt=2", 2000);
    }

    protected void geriAlBtn_Click(object sender, EventArgs e)
    {
        var app = db.Applications.FirstOrDefault(x => x.ID == xContentId);
        app.Status = 0;
        app.EgitimeAktarildi = false;
        db.SaveChanges();


        ShowSuccess("Başvuru Durumu Sıfırlandı.");
        pnlPageContent.Visible = false;
        RedirectWithJavascript("ApplicationView.aspx?Id=" + app.ID + "&Pt=2", 2000);
    }
}