﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Administrator/Content/MasterPage.master" AutoEventWireup="true" CodeFile="AppConfirm.aspx.cs" Inherits="Administrator_AppConfirm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCss" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpContent" Runat="Server">
    
    <div class="row">
        <div class="col-md-12">
            <div class="card">

                <div class="card-body">
                    <div class="col-md-12">
                        <asp:Button id="excelBtn" runat="server" style="float: right; margin-bottom: 10px;" CssClass="btn btn-brand" Text="Excel İndir" OnClick="excelBtn_Click" />
                    </div>
                    <!-- Modal -->
                    <%--<div class="modal fade" id="addRowModal" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header no-bd">
                                            <h5 class="modal-title">
                                                <span class="fw-mediumbold">New</span>
                                                <span class="fw-light">Row
                                                </span>
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <p class="small">Create a new row using this form, make sure you fill them all</p>
                                            <div>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group form-group-default">
                                                            <label>Name</label>
                                                            <input id="addName" type="text" class="form-control" placeholder="fill name">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 pr-0">
                                                        <div class="form-group form-group-default">
                                                            <label>Position</label>
                                                            <input id="addPosition" type="text" class="form-control" placeholder="fill position">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group form-group-default">
                                                            <label>Office</label>
                                                            <input id="addOffice" type="text" class="form-control" placeholder="fill office">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer no-bd">
                                            <button type="button" id="addRowButton" class="btn btn-primary">Add</button>
                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>--%>

                    <div class="table-responsive">
                        <table id="add-row" class="display table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Kimlik Numarası</th>
                                    <th style="width: 30%">Adı Soyadı</th>
                                    <th style="width: 30%">Başvurulan İçerik</th>
                                
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th>Kimlik Numarası</th>
                                    <th>Adı Soyadı</th>
                                    <th>Başvurulan İçerik</th>
                                  
                                </tr>
                            </tfoot>
                            <tbody>
                                <asp:Repeater ID="rptList" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                            <td><%#Eval("ID") %></td>
                                            <td><%#Eval("Users.UserName") %></td>
                                            <td><%#Eval("Users.Name") %> <%#Eval("Users.Surname") %></td>
                                            <td><a href="/Administrator/AppListByContent.aspx?Id=<%#Eval("Contents.ID") %>"><%#Eval("Contents.ContentTitle") %></a></td>                                      
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphJs" Runat="Server">
     <script>
        $(document).ready(function () {
            $('#basic-datatables').DataTable({
            });

            $('#multi-filter-select').DataTable({
                "pageLength": 10,
                initComplete: function () {
                    this.api().columns().every(function () {
                        var column = this;
                        var select = $('<select class="form-control"><option value=""></option></select>')
                            .appendTo($(column.footer()).empty())
                            .on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );

                                column
                                    .search(val ? '^' + val + '$' : '', true, false)
                                    .draw();
                            });

                        column.data().unique().sort().each(function (d, j) {
                            select.append('<option value="' + d + '">' + d + '</option>')
                        });
                        
                    });
                }
            });

            // Add Row
            $('#add-row').DataTable({
                "pageLength": 10,
                 "order": [[0, "desc"]]
            });

            var action = '<td> <div class="form-button-action"> <button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-primary btn-lg" data-original-title="Edit Task"> <i class="fa fa-edit"></i> </button> <button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove"> <i class="fa fa-times"></i> </button> </div> </td>';

            $('#addRowButton').click(function () {
                $('#add-row').dataTable().fnAddData([
                    $("#addName").val(),
                    $("#addPosition").val(),
                    $("#addOffice").val(),
                    action
                ]);
                $('#addRowModal').modal('hide');

            });
        });

        function SilClick(elem) {
            swal({
				title: 'Emin misiniz?',
				text: "Başvuruyu silmek istediğinize emin misiniz?",
				type: 'warning',
				buttons:{
					confirm: {
						text : 'Sil',
						className : 'btn btn-danger'
					},
					cancel: {
                        visible: true,
                        text: "İptal",
						className: 'btn btn-success'
					}
				}
			}).then((Delete) => {
                if (Delete) {
                    
                    var id = $(elem).data("id");
                    
                    $().newAjaxAdminHTML({},
                    {
                        funcName: "BasvuruSil",
                        newData: "{'id': '" + id + "'}",
                        onSuccess: function (msg) {
                            window.location.href = "AppUnConfirm.aspx";
                        }
                    });

				} else {
					swal.close();
				}
			});
        };
    </script>
</asp:Content>

