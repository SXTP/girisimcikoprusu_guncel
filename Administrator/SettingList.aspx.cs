﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Administrator_SettingList : Admin
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SetHeader("Tüm Ayarlar", "Ayarlar", "", "");

        rptList.DataSource = db.Settings.Where(x => x.DbStatus == false).ToList();
        rptList.DataBind();
    }
}