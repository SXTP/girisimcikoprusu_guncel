﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ExcelLibrary.SpreadSheet;
using OfficeOpenXml;
using OfficeOpenXml.Style;

public partial class Administrator_ProjectApplication : Admin
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SetHeader("Proje Başvuru İşlemleri", "Tüm Proje Başvuruları", "", "");
        if (!IsPostBack)
            BindRepeater();

    }
    void BindRepeater()
    {
        DataEntities ent = new DataEntities();

        var list = from a in ent.ProjectApplications
                   where a.IsDeleted == false
                   orderby a.ID descending
                   select a;

        rptList.DataSource = ent.ProjectApplications.Where(x => x.IsDeleted==false).OrderBy(x => x.ID).ToList();
        rptList.DataBind();
    }

    //protected void excelBtn_Click(object sender, EventArgs e)
    //{

    //    ExcelPackage excel = new ExcelPackage();
    //    var workSheet = excel.Workbook.Worksheets.Add("Sheet1");
    //    workSheet.TabColor = System.Drawing.Color.Black;
    //    workSheet.DefaultRowHeight = 12;

    //    workSheet.Row(1).Height = 20;
    //    workSheet.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
    //    workSheet.Row(1).Style.Font.Bold = true;
    //    workSheet.Cells[1, 1].Value = "ID";
    //    workSheet.Cells[1, 2].Value = "TC NO";
    //    workSheet.Cells[1, 2].Value = "ADI SOYADI";




    //    //Workbook workBook = new Workbook();
    //    //Worksheet workSheet = new Worksheet("Basvurular");

    //    var basvurular = db.ProjectApplications.Where(x => x.IsDeleted == false).ToList();
    //    int recordIndex = 2;
    //    foreach (var item in basvurular)
    //    {
    //        workSheet.Cells[recordIndex, 1].Value = item.ID;
    //        workSheet.Cells[recordIndex, 2].Value = item.TCNo;
    //        workSheet.Cells[recordIndex, 3].Value = item.Ad + item.Soyad;
    //        recordIndex++;
    //    }
    //    workSheet.Column(1).AutoFit();
    //    workSheet.Column(2).AutoFit();
    //    workSheet.Column(3).AutoFit();
    //    //workSheet.Column(4).AutoFit();
    //    //workSheet.Column(5).AutoFit();
    //    //workSheet.Column(6).AutoFit();
    //    //workSheet.Column(7).AutoFit();
    //    //workSheet.Column(8).AutoFit();
    //    //workSheet.Column(9).AutoFit();
    //    //workSheet.Column(10).AutoFit();
    //    //workSheet.Column(11).AutoFit();
    //    //workSheet.Column(12).AutoFit();
    //    //workSheet.Column(13).AutoFit();
    //    //workSheet.Column(14).AutoFit();
    //    string excelName = "BasvuruListesi";
    //    using (var memoryStream = new MemoryStream())
    //    {
    //        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    //        Response.AddHeader("content-disposition", "attachment; filename=" + excelName + ".xlsx");
    //        excel.SaveAs(memoryStream);
    //        memoryStream.WriteTo(Response.OutputStream);
    //        Response.Flush();
    //        Response.End();
    //    }

    //}

}