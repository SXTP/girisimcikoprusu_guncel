﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Administrator_SikayetveOneriView : Admin
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SetHeader("Şikayet ve Öneriler", "Detaylı Görüntüle", "", "");
        if (!IsPostBack)
        {
            var app = db.SikayetOneri.FirstOrDefault(x => x.ID == xContentId);
            if (app != null)
            {
                Baslik.InnerText = app.Baslik;
                Tarih.Value = app.Tarih.ToString();
                AdSoyad.Value = app.AdSoyad;
                Email.Value = app.Email;
                Telefon.Value = app.Telefon;
                Icerik.Value = app.Icerik;
              
            }
        }
    }
    protected void SilClick(object sender, EventArgs e)
    {
        var app = db.SikayetOneri.FirstOrDefault(x => x.ID == xContentId);
        db.SikayetOneri.Remove(app);
        db.SaveChanges();
       
    }
}