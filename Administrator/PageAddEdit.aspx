﻿<%@ Page Language="C#" MasterPageFile="~/Administrator/Content/MasterPage.master" ValidateRequest="false" AutoEventWireup="true" CodeFile="PageAddEdit.aspx.cs" Inherits="Administrator_PageAddEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCss" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpContent" runat="Server">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Sayfa Bilgileri</h4>
                </div>
                <div id="exampleValidation" novalidate="novalidate">
                    <div class="card-body">
                        <div class="form-group form-show-validation row">
                            <label for="name" class="col-lg-3 col-md-3 col-sm-4 mt-sm-2 text-right">Sayfa Adı <span class="required-label">*</span></label>
                            <div class="col-lg-4 col-md-9 col-sm-8">
                                <asp:TextBox ID="txtContentTitle" class="form-control" placeholder="Başlık Giriniz" required="" runat="server"></asp:TextBox>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">Üst Sayfa</div>
                </div>
                <div class="card-body">
                    <asp:DropDownList ID="ustSayfaDdl" runat="server"></asp:DropDownList>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">Yayınlansın mı?</div>
                </div>
                <div class="card-body">
                    <input id="chkPublish" runat="server" type="checkbox" checked data-toggle="toggle" data-onstyle="primary" />
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">Sayfa İçeriği</div>
                </div>
                <div class="card-body">
                    <asp:TextBox ID="txtContentDescription" TextMode="MultiLine" runat="server"></asp:TextBox>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-action">
                    <div class="row">
                        <div class="col-md-12">
                            <asp:Button ID="btnSave" class="btn btn-success" OnClick="btnSave_Click" runat="server" Text="Ekle / Kaydet" />
                            <button class="btn btn-danger">İptal</button>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphJs" runat="Server">
    <!-- Summernote -->
    <script src="../../assets/js/plugin/summernote/summernote-bs4.min.js"></script>
    <script>
        $('#cpContent_txtContentDescription').summernote({
            placeholder: 'İçerik Hakkında Bilgi Giriniz.',
            fontNames: ['Arial', 'Arial Black', 'Comic Sans MS', 'Courier New'],
            tabsize: 2,
            height: 300
        });
    </script>
</asp:Content>