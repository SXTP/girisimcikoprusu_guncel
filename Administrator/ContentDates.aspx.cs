﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Administrator_ContentDates : Admin
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SetHeader("Tüm Tarihler", "Haberler", "", "");

        rptList.DataSource = db.ContentDates.Where(x => x.DbStatus == false).ToList();
        rptList.DataBind();
    }
}