﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Administrator_PageAddEdit : Admin
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SetHeader("Sayfa İşlemleri", "Sayfa Ekle / Düzenle", "", "");
        if (!IsPostBack)
        {
            if (xProcessType == 2)
            {
                var page = db.WebPageContents.FirstOrDefault(x => x.ID == xContentId);
                if (page != null)
                {
                    txtContentTitle.Text = page.PageTitle;
                    chkPublish.Checked = page.PageWebPublish;
                    txtContentDescription.Text = page.PageContent;
                    var pages = db.WebPageContents.Where(w => w.PageWebPublish).ToList();
                    ustSayfaDdl.Items.Add(new ListItem { Text = "Yok", Value = "0", Selected = page.PageUpId == 0 ? true : false, Enabled = true });
                    foreach (var item in pages)
                    {
                        ustSayfaDdl.Items.Add(new ListItem { Text = item.PageTitle, Value = item.ID.ToString(), Enabled = true, Selected = item.ID == page.PageUpId ? true : false });
                    }
                }
            }
            else if (xProcessType == 3)
            {
                var page = db.WebPageContents.FirstOrDefault(x => x.ID == xContentId);
                page.DbStatus = true;
                db.SaveChanges();
                ShowSuccess("Sayfa Başarıyla Silindi.Listeleme sayfasına yönlendirilirken lütfen bekleyin..");
                pnlPageContent.Visible = false;
                RedirectWithJavascript("PageList.aspx", 2000);
            }
            else
            {
                var pages = db.WebPageContents.Where(w => w.PageWebPublish).ToList();
                ustSayfaDdl.Items.Add(new ListItem { Text = "Yok", Value = "0", Selected = true, Enabled = true });
                foreach (var item in pages)
                {
                    ustSayfaDdl.Items.Add(new ListItem { Text = item.PageTitle, Value = item.ID.ToString(), Enabled = true, Selected = false });
                }
            }
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (xProcessType != 2)
        {
            WebPageContents page = new WebPageContents();
            page.PageTitle = txtContentTitle.Text;
            page.PageContent = txtContentDescription.Text;
            page.CreateDate = DateTime.Now;
            page.DbStatus = false;
            page.PageWebPublish = chkPublish.Checked;
            page.FkCreated = EXERT.User.GetUserId();
            page.PageRow = db.WebPageContents.Count() + 1;
            page.PageUpId = Convert.ToInt32(ustSayfaDdl.SelectedValue);

            db.WebPageContents.Add(page);
            db.SaveChanges();
            ShowSuccess("Sayfa Başarıyla Eklendi.Listeleme sayfasına yönlendirilirken lütfen bekleyin..");
            pnlPageContent.Visible = false;
            RedirectWithJavascript("PageList.aspx", 2000);
        }
        else
        {
            var page = db.WebPageContents.FirstOrDefault(x => x.ID == xContentId);
            page.PageTitle = txtContentTitle.Text;
            page.PageContent = txtContentDescription.Text;
            page.CreateDate = DateTime.Now;
            page.DbStatus = false;
            page.PageWebPublish = chkPublish.Checked;
            page.FkCreated = EXERT.User.GetUserId();
            page.PageUpId = Convert.ToInt32(ustSayfaDdl.SelectedValue);

            db.SaveChanges();
            ShowSuccess("Değişiklikler Başarıyla Kaydedildi.Listeleme sayfasına yönlendirilirken lütfen bekleyin..");
            pnlPageContent.Visible = false;
            RedirectWithJavascript("PageList.aspx", 2000);
        }

    }
}