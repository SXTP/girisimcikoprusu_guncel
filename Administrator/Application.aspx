﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Administrator/Content/MasterApp.master" AutoEventWireup="true" CodeFile="Application.aspx.cs" Inherits="Application" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphC" runat="Server">
    <div class="container" id="page_container">
        <div class="row page-row" id="dvBasvuru" runat="server">
            <div class="col-md-12">
                <h3 class="title">Kişisel Bilgiler</h3>
                <div class="form-white">
                    <div class="form-group basvuruError">
                        <asp:ValidationSummary ValidationGroup="grpKayit" ID="vs1" runat="server" ShowMessageBox="false" ShowSummary="true" />
                    </div>
                    <div class="form-group name">
                        <label for="TCNumaraniz">TC Numaranız<span class="required">(Fatura için gereklidir.)</span></label>
                        <asp:TextBox ID="txtTC" class="form-control" value="" data-mask="99999999999" ValidationGroup="grpKayit" placeholder="TC Numaranızı Giriniz.." runat="server"></asp:TextBox>
                    </div>
                    <div class="form-group name">
                        <label for="adiSoyadi">Adınız<span class="required">*</span></label>
                        <asp:TextBox ID="txtAd" class="form-control" value="" ValidationGroup="grpKayit" placeholder="Adınızı Giriniz..." runat="server"></asp:TextBox>
                    </div>
                    <div class="form-group name">
                        <label for="adiSoyadi">Soyadınız<span class="required">*</span></label>
                        <asp:TextBox ID="txtSoyad" class="form-control" value="" ValidationGroup="grpKayit" placeholder="Soyadınızı Giriniz..." runat="server"></asp:TextBox>
                    </div>
                    <div class="form-group name">
                        <label for="adiSoyadi">Doğum Yılınız<span class="required">*</span></label>
                        <asp:DropDownList ID="drpDogumYil" runat="server" CssClass="form-control"></asp:DropDownList>
                    </div>
                    <div class="form-group email">
                        <label for="eMail">E-Mail<span class="required">(Sık Kullandığınız E-Mail Adresinizi Dikkatlice Giriniz. Sistem Üzerindeki Tüm İşlemler E-Mail Üzerinden Yapılmaktadır.)</span></label>
                        <asp:TextBox ID="txtMail" TextMode="Email" ValidationGroup="grpKayit" class="form-control" value="" placeholder="E-Mail Adresinizi Giriniz..." runat="server"></asp:TextBox>
                    </div>
                    <div class="form-group phone">
                        <label for="telefon">Telefon<span class="required">(Doğru Girdiğinizden Mutlaka Emin Olun!)</span></label>
                        <asp:TextBox ID="txtCepTel" TextMode="Phone" ValidationGroup="grpKayit" data-mask="(999) 999 99 99" class="form-control" value="" placeholder="Telefon Numaranızı Giriniz..." runat="server"></asp:TextBox>
                    </div>
                    <div class="form-group phone">
                        <label for="adres">Adres <span class="required">(Fatura ve Sertifika Gönderimi Yapılacağı İçin Lütfen Dikkatli Giriniz!)</span></label>
                        <asp:TextBox ID="txtAdres" TextMode="MultiLine" ValidationGroup="grpKayit" class="form-control" runat="server"></asp:TextBox>
                    </div>
                    <div class="form-group phone">
                        <label for="ilce">İlçe</label>
                        <asp:TextBox ID="txtIlce" class="form-control" ValidationGroup="grpKayit" value="" placeholder="İlçe Adını Giriniz..." runat="server"></asp:TextBox>
                    </div>
                    <div class="form-group phone">
                        <label for="ilce">Posta Kodu</label>
                        <asp:TextBox ID="txtPostaKodu" class="form-control" ValidationGroup="grpKayit" value="" placeholder="Adresinize Ait Posta Kodu Giriniz..." runat="server"></asp:TextBox>
                    </div>
                    <div class="form-group message">
                        <label for="il">İl</label>
                        <asp:DropDownList ID="drpil" class="form-control" runat="server" ValidationGroup="grpKayit">
                            <asp:ListItem Value="0">İl Se&#231;iniz</asp:ListItem>
                            <asp:ListItem Value="Adana">Adana</asp:ListItem>
                            <asp:ListItem Value="Adıyaman">Adıyaman</asp:ListItem>
                            <asp:ListItem Value="Afyon">Afyon</asp:ListItem>
                            <asp:ListItem Value="Ağrı">Ağrı</asp:ListItem>
                            <asp:ListItem Value="Amasya">Amasya</asp:ListItem>
                            <asp:ListItem Value="Ankara">Ankara</asp:ListItem>
                            <asp:ListItem Value="Antalya">Antalya</asp:ListItem>
                            <asp:ListItem Value="Artvin">Artvin</asp:ListItem>
                            <asp:ListItem Value="Aydın">Aydın</asp:ListItem>
                            <asp:ListItem Value="Balıkesir">Balıkesir</asp:ListItem>
                            <asp:ListItem Value="Bilecik">Bilecik</asp:ListItem>
                            <asp:ListItem Value="Bingöl">Bingöl</asp:ListItem>
                            <asp:ListItem Value="Bitlis">Bitlis</asp:ListItem>
                            <asp:ListItem Value="Bolu">Bolu</asp:ListItem>
                            <asp:ListItem Value="Burdur">Burdur</asp:ListItem>
                            <asp:ListItem Value="Bursa">Bursa</asp:ListItem>
                            <asp:ListItem Value="Çanakkale">Çanakkale</asp:ListItem>
                            <asp:ListItem Value="Çankırı">Çankırı</asp:ListItem>
                            <asp:ListItem Value="Çorum">Çorum</asp:ListItem>
                            <asp:ListItem Value="Denizli">Denizli</asp:ListItem>
                            <asp:ListItem Value="Diyarbakır">Diyarbakır</asp:ListItem>
                            <asp:ListItem Value="Edirne">Edirne</asp:ListItem>
                            <asp:ListItem Value="Elazığ">Elazığ</asp:ListItem>
                            <asp:ListItem Value="Erzincan">Erzincan</asp:ListItem>
                            <asp:ListItem Value="Erzurum">Erzurum</asp:ListItem>
                            <asp:ListItem Value="Eskişehir">Eskişehir</asp:ListItem>
                            <asp:ListItem Value="Gaziantep">Gaziantep</asp:ListItem>
                            <asp:ListItem Value="Giresun">Giresun</asp:ListItem>
                            <asp:ListItem Value="Gümüşhane">Gümüşhane</asp:ListItem>
                            <asp:ListItem Value="Hakkari">Hakkari</asp:ListItem>
                            <asp:ListItem Value="Hatay">Hatay</asp:ListItem>
                            <asp:ListItem Value="Isparta">Isparta</asp:ListItem>
                            <asp:ListItem Value="İçel">İçel</asp:ListItem>
                            <asp:ListItem Value="İstanbul">İstanbul</asp:ListItem>
                            <asp:ListItem Value="İzmir">İzmir</asp:ListItem>
                            <asp:ListItem Value="Kars">Kars</asp:ListItem>
                            <asp:ListItem Value="Kastamonu">Kastamonu</asp:ListItem>
                            <asp:ListItem Value="Kayseri">Kayseri</asp:ListItem>
                            <asp:ListItem Value="Kırklareli">Kırklareli</asp:ListItem>
                            <asp:ListItem Value="Kırşehir">Kırşehir</asp:ListItem>
                            <asp:ListItem Value="Kocaeli">Kocaeli</asp:ListItem>
                            <asp:ListItem Value="Konya">Konya</asp:ListItem>
                            <asp:ListItem Value="Kütahya">Kütahya</asp:ListItem>
                            <asp:ListItem Value="Malatya">Malatya</asp:ListItem>
                            <asp:ListItem Value="Manisa">Manisa</asp:ListItem>
                            <asp:ListItem Value="Kahramanmaraş">Kahramanmaraş</asp:ListItem>
                            <asp:ListItem Value="Mardin">Mardin</asp:ListItem>
                            <asp:ListItem Value="Muğla">Muğla</asp:ListItem>
                            <asp:ListItem Value="Muş">Muş</asp:ListItem>
                            <asp:ListItem Value="Nevşehir">Nevşehir</asp:ListItem>
                            <asp:ListItem Value="Niğde">Niğde</asp:ListItem>
                            <asp:ListItem Value="Ordu">Ordu</asp:ListItem>
                            <asp:ListItem Value="Rize">Rize</asp:ListItem>
                            <asp:ListItem Value="Sakarya">Sakarya</asp:ListItem>
                            <asp:ListItem Value="Samsun">Samsun</asp:ListItem>
                            <asp:ListItem Value="Siirt">Siirt</asp:ListItem>
                            <asp:ListItem Value="Sinop">Sinop</asp:ListItem>
                            <asp:ListItem Value="Sivas">Sivas</asp:ListItem>
                            <asp:ListItem Value="Tekirdağ">Tekirdağ</asp:ListItem>
                            <asp:ListItem Value="Tokat">Tokat</asp:ListItem>
                            <asp:ListItem Value="Trabzon">Trabzon</asp:ListItem>
                            <asp:ListItem Value="Tunceli">Tunceli</asp:ListItem>
                            <asp:ListItem Value="Şanlıurfa">Şanlıurfa</asp:ListItem>
                            <asp:ListItem Value="Uşak">Uşak</asp:ListItem>
                            <asp:ListItem Value="Van">Van</asp:ListItem>
                            <asp:ListItem Value="Yozgat">Yozgat</asp:ListItem>
                            <asp:ListItem Value="Zonguldak">Zonguldak</asp:ListItem>
                            <asp:ListItem Value="Aksaray">Aksaray</asp:ListItem>
                            <asp:ListItem Value="Bayburt">Bayburt</asp:ListItem>
                            <asp:ListItem Value="Karaman">Karaman</asp:ListItem>
                            <asp:ListItem Value="Kırıkkale">Kırıkkale</asp:ListItem>
                            <asp:ListItem Value="Batman">Batman</asp:ListItem>
                            <asp:ListItem Value="Şırnak">Şırnak</asp:ListItem>
                            <asp:ListItem Value="Bartın">Bartın</asp:ListItem>
                            <asp:ListItem Value="Ardahan">Ardahan</asp:ListItem>
                            <asp:ListItem Value="Iğdır">Iğdır</asp:ListItem>
                            <asp:ListItem Value="Yalova">Yalova</asp:ListItem>
                            <asp:ListItem Value="Karabük">Karabük</asp:ListItem>
                            <asp:ListItem Value="Kilis">Kilis</asp:ListItem>
                            <asp:ListItem Value="Osmaniye">Osmaniye</asp:ListItem>
                            <asp:ListItem Value="Düzce">Düzce</asp:ListItem>
                        </asp:DropDownList>

                    </div>
                </div>
                <h3 class="title">Eğitim Bilgileri</h3>
                <div class="form-white">
                    <div class="form-group email">
                        <label for="egitim">Eğitim Adı</label>
                        <asp:TextBox ID="txtEgitimAdi" class="form-control" disabled runat="server"></asp:TextBox>
                    </div>
                    <div class="form-group email">
                        <label for="egitimUcreti">Eğitim Ücreti</label>
                        <asp:TextBox ID="txtEgitimUcreti" class="form-control" disabled runat="server"></asp:TextBox>
                    </div>
                </div>
                <h3 class="title">Ödeme Seçenekleri</h3>
                <div class="form-white">
                    <div class="odemeMetodu">

                        <div class="checkBoxKadrius" style="display: none;">
                            <asp:CheckBox ID="chkKrediKarti" Text="&nbsp;" onclick="krediKartiGoster();" OnCheckedChanged="chkKrediKarti_CheckedChanged" runat="server"></asp:CheckBox>KREDİ KARTI
                        </div>
                        <div class="checkBoxKadrius">
                            <asp:CheckBox ID="chkEftHavale" Text="&nbsp;" onclick="eftHavaleGoster();" OnCheckedChanged="chkEftHavale_CheckedChanged" runat="server"></asp:CheckBox>EFT/HAVALE
                        </div>

                    </div>
                    <div class="temizleme"></div>
                    <div id="dvkrediKarti" runat="server" style="display: none;">
                        <div class="form-group name">
                            <label for="krediKartiSahibi">Kredi Kartı Sahibi</label>
                            <asp:TextBox ID="txtKrediKartiAdSoyad" class="form-control" placeholder="Adınızı Soyadınızı Giriniz..." autocomplete="off" runat="server"></asp:TextBox>
                        </div>
                        <div class="form-group name">
                            <label for="krediKartiNumarasi">Kredi Kartı Numarası</label>
                            <asp:TextBox ID="txtKrediKartiNumarasi" class="form-control" data-mask="9999 9999 9999 9999" placeholder="16 Haneli Kredi Kartı Numaranızı Giriniz..." autocomplete="off" runat="server"></asp:TextBox>
                        </div>
                        <div class="form-group email">
                            <label>Gerçerlilik Tarihi AY / YIL</label>
                            <div class="row">
                                <div class="col-md-4 col-xs-6">
                                    <asp:DropDownList ID="drpKKAy" class="form-control" runat="server">
                                        <asp:ListItem Value="0">AY</asp:ListItem>
                                        <asp:ListItem>01</asp:ListItem>
                                        <asp:ListItem>02</asp:ListItem>
                                        <asp:ListItem>03</asp:ListItem>
                                        <asp:ListItem>04</asp:ListItem>
                                        <asp:ListItem>05</asp:ListItem>
                                        <asp:ListItem>06</asp:ListItem>
                                        <asp:ListItem>07</asp:ListItem>
                                        <asp:ListItem>08</asp:ListItem>
                                        <asp:ListItem>09</asp:ListItem>
                                        <asp:ListItem>10</asp:ListItem>
                                        <asp:ListItem>11</asp:ListItem>
                                        <asp:ListItem>12</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="col-md-4 col-xs-6">
                                    <asp:DropDownList ID="drpKKYil" class="form-control" runat="server">
                                        <asp:ListItem Value="0">YIL</asp:ListItem>
                                        <asp:ListItem>2016</asp:ListItem>
                                        <asp:ListItem>2017</asp:ListItem>
                                        <asp:ListItem>2018</asp:ListItem>
                                        <asp:ListItem>2019</asp:ListItem>
                                        <asp:ListItem>2020</asp:ListItem>
                                        <asp:ListItem>2021</asp:ListItem>
                                        <asp:ListItem>2022</asp:ListItem>
                                        <asp:ListItem>2023</asp:ListItem>
                                        <asp:ListItem>2024</asp:ListItem>
                                        <asp:ListItem>2025</asp:ListItem>
                                        <asp:ListItem>2026</asp:ListItem>
                                        <asp:ListItem>2027</asp:ListItem>
                                        <asp:ListItem>2028</asp:ListItem>
                                        <asp:ListItem>2029</asp:ListItem>
                                        <asp:ListItem>2030</asp:ListItem>
                                        <asp:ListItem>2031</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="form-group phone">
                            <label for="guvenlikKodu">Güvenlik Kodu</label>
                            <asp:TextBox ID="txtKrediKartiCSV" class="form-control" data-mask="999" placeholder="3 Haneli Güvenlik Kodunuzu Giriniz..." autocomplete="off" runat="server"></asp:TextBox>
                        </div>
                        <div class="form-group phone">
                            <label for="guvenlikKodu">Taksit Seçeneği</label>
                            <asp:DropDownList ID="drpKKTaksit" ForeColor="#510076" Font-Bold="true" class="form-control" runat="server"></asp:DropDownList>
                        </div>
                        <div style="text-align: center;">
                            <img style="width: 250px;" src="static/images/3d.png" />
                        </div>
                    </div>
                    <div id="dveftHavale" runat="server">
                        <div class="form-group">
                            Banka Adı : Halk Bankası<br />
                            Alıcı Adı : Uludağ Üniversitesi Döner Sermaye İşletme Müdürlüğü<br />
                            IBAN : TR04 0001 2001 3290 0006 0001 07
                            <asp:Literal ID="ltrBankaBilgileri" runat="server"></asp:Literal>
                        </div>
                    </div>
                    <div class="temizleme"></div>

                    <div class="form-group name">
                        <label style="color: #ff0000;">
                            <div class="checkBoxKadrius">
                                <asp:CheckBox ID="chkSatis" runat="server" Text="&nbsp;"></asp:CheckBox>&nbsp;<a href="https://www.artisertifika.com/sayfa-4-mesafeli-satis-sozlesmesi" onclick="window.open(this.href,'','resizable=no,location=no,menubar=no,scrollbars=no,status=no,toolbar=no,fullscreen=no,dependent=no,width=800,height=800,status'); return false">Satış sözleşmesini okudum ve anladım.</a>
                            </div>
                        </label>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <asp:Button ID="btnDevamEt" OnClick="btnDevamEt_Click" runat="server" class="btn btn-primary" Text="BAŞVURUYA DEVAM ET" ValidationGroup="grpKayit"></asp:Button>
                        </div>
                    </div>
                    <span style="color: red;">
                        <asp:Literal ID="ltrBasvuruBilgi" runat="server"></asp:Literal></span>
                </div>
            </div>
        </div>
        <div class="row page-row" id="dvBasarili" runat="server">
            <asp:Literal ID="ltrBasarili" runat="server"></asp:Literal>
        </div>
    </div>
    <div class="page_loading">
    </div>

    <script src="/static/scripts/inputmask.js"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $('.page_loading').css({ 'display': 'none' });
            $("#cphC_btnDevamEt").click(function () {
                if (Page_ClientValidate("grpKayit")) {
                    $('.page_loading').css({ 'display': 'block' });
                    $("div#cphC_dveftHavale").show(100);

                }
            });

        });
        function krediKartiGoster() {
            $("div#cphC_dvkrediKarti").show(100);
            $("div#cphC_dveftHavale").hide(100);
            document.getElementById("cphC_chkEftHavale").checked = false;
            document.getElementById("cphC_chkKrediKarti").checked = true;

        }
        function eftHavaleGoster() {
            $("div#cphC_dvkrediKarti").hide(100);
            $("div#cphC_dveftHavale").show(100);
            document.getElementById("cphC_chkKrediKarti").checked = false;
            document.getElementById("cphC_chkEftHavale").checked = true;

        }
        function formKontrolEt() {
            var TCNumaraniz = document.getElementById('TCNumaraniz').value;
            var adiSoyadi = document.getElementById('adiSoyadi').value;
            var eMail = document.getElementById('eMail').value;
            var telefon = document.getElementById('telefon').value;
            var il = document.getElementById('il').value;
            var mezuniyet = document.getElementById('mezuniyet').value;
            var egitimSekli = document.getElementById('egitimSekli').value;
            var satisSozlesmesi = document.getElementById('satisSozlesmesi').checked;

            if (TCNumaraniz == "") { alert('TC Numarasını Giriniz!'); return false; }
            if (adiSoyadi == "") { alert('Adınızı ve Soyadınızı Giriniz!'); return false; }
            if (eMail == "") { alert('E-Mail Adresinizi Giriniz!'); return false; }
            if (telefon == "") { alert('Telefon Numaranızı Giriniz!'); return false; }
            if (il == "0") { alert('Yaşadığınız İli Giriniz!'); return false; }
            if (il == "0") { alert('Mezuniyetinizi Seçiniz!'); return false; }
            if (egitimSekli == "0") { alert('Lütfen Eğitim Şeklinizi Seçiniz!'); return false; }
            if (satisSozlesmesi == false) { alert('Lütfen Satış Sözleşmesini Onaylayınız!'); return false; }
            $('#btn').attr('disabled', true);
            $("#yaz").ajaxStart(function () {
                $(this).html('<img src="http://www.ilkizakademi.com/yukleniyor.gif" />');
            });
        }

        function cv2(oSrc, args) {
            var drpAs = document.all ? document.all["<%=drpil.ClientID  %>"] : document.getElementById("<%= drpil.ClientID  %>");
            args.IsValid = (drpAs.selectedIndex > 0);
        }
    </script>

</asp:Content>

