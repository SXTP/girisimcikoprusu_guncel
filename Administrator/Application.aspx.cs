﻿using EXERT;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Text.RegularExpressions;

public partial class Application : Admin
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            var cnt = db.Contents.FirstOrDefault(x => x.DbStatus == false && x.ID == xContentId);
            if (cnt != null)
            {
                this.Title = cnt.ContentTitle + " - " + SiteSettings.SiteName + " Başvuru Ekranı";
                txtEgitimAdi.Text = cnt.ContentTitle;
                txtEgitimUcreti.Text = cnt.ContentPrice.ToString();

                drpDogumYil.DataTextField = "Texter";
                drpDogumYil.DataValueField = "Texter";
                drpDogumYil.DataSource = Numbers.NumberList(DateTime.Now.Year - 15, DateTime.Now.Year - 70, false);
                drpDogumYil.DataBind();
                chkEftHavale.Checked = true;
            }
        }
    }

    protected void chkKrediKarti_CheckedChanged(object sender, EventArgs e)
    {

    }

    protected void chkEftHavale_CheckedChanged(object sender, EventArgs e)
    {

    }

    protected void btnDevamEt_Click(object sender, EventArgs e)
    {
        if (db.Users.FirstOrDefault(x => x.UserName == txtTC.Text.Trim()) == null)
        {
            if (db.Applications.FirstOrDefault(x => x.FkContentId == xContentId && x.Users.UserName == txtTC.Text.Trim()) != null)
            {
                ltrBasvuruBilgi.Text = "Başvurunuz Mevcuttur.Tekrar Başvuramazsınız";
            }
            if (chkEftHavale.Checked == true)
            {
                Users usr = new Users();
                usr.UserName = txtTC.Text;
                usr.Name = txtAd.Text;
                usr.Surname = txtSoyad.Text;
                usr.Status = 0;
                usr.Password = HashTool.GetMD5(txtTC.Text.Substring(0, 5));
                db.Users.Add(usr);

                UserDetails usd = new UserDetails();
                usd.Address = txtAdres.Text;
                usd.AddressCity = drpil.SelectedValue;
                usd.AddressPostCode = txtPostaKodu.Text;
                usd.AddressStreet = txtIlce.Text;
                usd.BirthYear = Convert.ToInt32(drpDogumYil.SelectedValue);
                usd.CreateDate = DateTime.Now;
                usd.Email = txtMail.Text;
                usd.PhoneNumber = txtCepTel.Text;
                usd.FkUserId = usr.ID;
                db.UserDetails.Add(usd);

                Applications app = new Applications();
                app.FkUserId = usr.ID;
                app.FkContentId = xContentId;
                app.CreateDate = DateTime.Now;
                app.DbStatus = false;
                app.Status = 0;
                app.IPAdress = " ";
                app.PaymentType = 0;
                db.Applications.Add(app);

                db.SaveChanges();
                dvBasvuru.Visible = false;
                dvBasarili.Visible = true;
                ltrBasarili.Text = "Başvurunuz alınmıştır.";
            }
            else if (chkKrediKarti.Checked==true)
            {
                Users usr = new Users();
                usr.UserName = txtTC.Text;
                usr.Name = txtAd.Text;
                usr.Surname = txtSoyad.Text;
                usr.Status = 0;
                usr.Password = HashTool.GetMD5(txtTC.Text.Substring(0, 5));
                db.Users.Add(usr);

                UserDetails usd = new UserDetails();
                usd.Address = txtAdres.Text;
                usd.AddressCity = drpil.SelectedValue;
                usd.AddressPostCode = txtPostaKodu.Text;
                usd.AddressStreet = txtIlce.Text;
                usd.BirthYear = Convert.ToInt32(drpDogumYil.SelectedValue);
                usd.CreateDate = DateTime.Now;
                usd.Email = txtMail.Text;
                usd.PhoneNumber = txtCepTel.Text;
                usd.FkUserId = usr.ID;
                db.UserDetails.Add(usd);

                Applications app = new Applications();
                app.FkUserId = usr.ID;
                app.FkContentId = xContentId;
                app.CreateDate = DateTime.Now;
                app.DbStatus = false;
                app.Status = 0;
                app.IPAdress = " ";
                app.PaymentType = 0;
                db.Applications.Add(app);

                string sKartNo = Regex.Replace(txtKrediKartiNumarasi.Text.Trim(), "[^0-9]+", "");
                string sCvvKodu = Regex.Replace(txtKrediKartiCSV.Text.Trim(), "[^0-9]+", "");
                int iSonKullanmaAy = int.Parse(drpKKAy.SelectedItem.Value);
                int iSonKullanmaYil = int.Parse(drpKKYil.SelectedItem.Value);
                string sKartSahibi = txtKrediKartiAdSoyad.Text.Trim();
                string sEmailAdres = txtMail.Text.Trim();
                string sTransId = TransOperations.TransactionIdCreate();
                decimal dcToplam = Convert.ToDecimal(app.Contents.ContentPrice);

                string remoteHost = Request.ServerVariables["REMOTE_ADDR"];
                string us = string.IsNullOrEmpty(remoteHost) ? "Bilinmiyor" : remoteHost;

                TransHistory th = new TransHistory();
                th.TransId = sTransId;
                th.KayitId = app.ID;
                th.CCName = sKartSahibi;
                th.CCNo = "XXXX-XXXX-XXXX-" + sKartNo.Substring(sKartNo.Length - 4);
                th.TransType = "Subscription";
                th.TransApproved = false;
                th.AdminApproved = 0;
                th.Authcode = string.Empty;
                th.Hostlogkey = string.Empty;
                th.ApprovedCode = string.Empty;

                th.TotalAmount = dcToplam;

                th.ResponseCode = drpKKTaksit.SelectedItem.Value == "1" ? "" : drpKKTaksit.SelectedItem.Value;
                th.ResponseText = string.Empty;
                th.ResponseXML = string.Empty;
                th.CreatedIp = us;
                th.CreatedOn = DateTime.Now;
                th.CreatedBy = us;
                th.EmailAdres = sEmailAdres;

                db.TransHistory.Add(th);
                db.SaveChanges();


                string sSonucURL = "https://ulusemaip.exert.com.tr/GetCreditCart.aspx?th=" + sTransId;
                String sAmount = string.Format("{0:0.00}", dcToplam);         //İşlem tutarı
                String rnd = DateTime.Now.ToString();  //Kontrol ve güvenlik amaçlı sürekli değişen bir değer tarih gibi
                String taksit = "";      //Taksit sayısı
                String islemtipi = "Auth"; //İşlem tipi
                String hashstr = TransOperations.Bankclientid + sTransId + sAmount + sSonucURL + sSonucURL + islemtipi + taksit + DateTime.Now.ToString() + TransOperations.Bankstorekey;
                System.Security.Cryptography.SHA1 sha = new System.Security.Cryptography.SHA1CryptoServiceProvider();
                byte[] hashbytes = System.Text.Encoding.GetEncoding("ISO-8859-9").GetBytes(hashstr);
                byte[] inputbytes = sha.ComputeHash(hashbytes);
                string sCurrency = "949";
                String hash = Convert.ToBase64String(inputbytes);

                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append("<form method=\"post\" action=\"https://sanalpos.halkbank.com.tr/fim/est3Dgate\" name=\"payForm\">");
                sb.AppendFormat("<input type=\"hidden\" name=\"clientid\" value=\"{0}\">", TransOperations.Bankclientid);
                sb.AppendFormat("<input type=\"hidden\" name=\"storetype\" value=\"{0}\"/>", "3d");
                sb.AppendFormat("<input type=\"hidden\" name=\"hash\" value=\"{0}\">", hash);
                sb.AppendFormat("<input type=\"hidden\" name=\"islemtipi\" value=\"{0}\" />", islemtipi);
                sb.AppendFormat("<input type=\"hidden\" name=\"amount\" value=\"{0}\">", sAmount);
                sb.AppendFormat("<input type=\"hidden\" name=\"currency\" value=\"{0}\">", sCurrency);
                sb.AppendFormat("<input type=\"hidden\" name=\"oid\" value=\"{0}\">", sTransId);
                sb.AppendFormat("<input type=\"hidden\" name=\"okUrl\" value=\"{0}\">", sSonucURL);
                sb.AppendFormat("<input type=\"hidden\" name=\"failUrl\" value=\"{0}\">", sSonucURL);
                sb.AppendFormat("<input type=\"hidden\" name=\"lang\" value=\"{0}\">", "tr");
                sb.AppendFormat("<input type=\"hidden\" name=\"rnd\" value=\"{0}\">", rnd);
                sb.AppendFormat("<input type=\"text\" name=\"pan\" size=\"20\" value=\"{0}\"/>", sKartNo);
                sb.AppendFormat("<input type=\"text\" name=\"Ecom_Payment_Card_ExpDate_Year\" value=\"{0}\"/>", string.Format("{0:00}", iSonKullanmaYil));
                sb.AppendFormat("<input type=\"text\" name=\"Ecom_Payment_Card_ExpDate_Month\" value=\"{0}\"/>", string.Format("{0:00}", iSonKullanmaAy));



                sb.AppendFormat("<input type=\"text\" name=\"cv2\" size=\"4\" value=\"{0}\"/>", sCvvKodu);
                sb.AppendFormat("<input type=\"hidden\" name=\"taksit\" value=\"{0}\" />", taksit == "1" ? "" : taksit);
                sb.AppendFormat("<input type=\"hidden\" name=\"Faturafirma\" value=\"{0}\">", sKartSahibi);
                sb.AppendFormat("<input type=\"hidden\" name=\"Fadres\" value=\"{0}\">", txtAdres.Text.Trim());
                sb.AppendFormat("<input type=\"hidden\" name=\"Fil\" value=\"{0}\">", drpil.SelectedItem.Value);
                sb.AppendFormat("<input type=\"hidden\" name=\"tel\" value=\"{0}\">", txtCepTel.Text.Trim());
                sb.AppendFormat("<input type=\"hidden\" name=\"Fulkekodu\" value=\"{0}\">", "tr");
                sb.AppendFormat("<input type=\"hidden\" name=\"itemnumber{1}\" value=\"{0}\">", app.ID.ToString(), 1);
                sb.AppendFormat("<input type=\"hidden\" name=\"productcode{1}\" value=\"{0}\">", "EGT" + app.FkContentId, 1);
                sb.AppendFormat("<input type=\"hidden\" name=\"qty{1}\" value=\"{0}\">", 1, 1);
                sb.AppendFormat("<input type=\"hidden\" name=\"desc{1}\" value=\"{0}\">", txtEgitimAdi.Text, 1);
                sb.AppendFormat("<input type=\"hidden\" name=\"id{1}\" value=\"{0}\">", app.FkContentId, 1);
                sb.AppendFormat("<input type=\"hidden\" name=\"price{1}\" value=\"{0}\">", string.Format("{0:0.00}", sAmount), 1);
                sb.AppendFormat("<input type=\"hidden\" name=\"amount{1}\" value=\"{0}\">", string.Format("{0:0.00}", sAmount), 1);
                sb.Append("</form>");
                sb.Append("<script>document.getElementById('payForm').submit();</script>");

                Guid g = Guid.NewGuid();
                string sUserId = g.ToString();
                TransOperations.FormHTMLAddtoCache(sb.ToString(), sUserId);
                Response.Redirect("/GetPrice.aspx?guid=" + sUserId);
            }
        }
        else
        {
            var usr = db.Users.FirstOrDefault(x => x.UserName == txtTC.Text && x.Status == 0);
            if (usr != null)
            {
                if (chkEftHavale.Checked == true)
                {
                    Applications app = new Applications();
                    app.FkUserId = usr.ID;
                    app.FkContentId = xContentId;
                    app.CreateDate = DateTime.Now;
                    app.DbStatus = false;
                    app.Status = 0;
                    app.IPAdress = " ";
                    app.PaymentType = 0;
                    db.Applications.Add(app);
                    db.SaveChanges();

                    dvBasvuru.Visible = false;
                    dvBasarili.Visible = true;
                    ltrBasarili.Text = "Başvurunuz alınmıştır.";
                }
            }
        }
    }

    protected void cv2_ServerValidate(object source, ServerValidateEventArgs args)
    {

    }
}