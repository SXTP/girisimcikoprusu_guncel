﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class _Default : Admin
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SetHeader("Hızlı Bakış", "Genel Veriler Görüntüleniyor", "", "");

        var basvurular = db.Applications.Where(p => p.DbStatus == false).OrderByDescending(p => p.CreateDate).Take(10).ToList();
        rptSonBasvuru.DataSource = basvurular;
        rptSonBasvuru.DataBind();

        var icerikler = db.Contents.Where(p => p.DbStatus == false).OrderByDescending(p => p.CreateDate).ToList();
        rptContents.DataSource = icerikler;
        rptContents.DataBind();

        var _icerikler = icerikler.Where(p => p.Applications.Where(a => a.DbStatus == false).Count() > 0).OrderByDescending(p => p.Applications.Where(a => a.DbStatus == false).Count());
        rptEgitimler.DataSource = _icerikler;
        rptEgitimler.DataBind();

        rptEgitimApps.DataSource = _icerikler;
        rptEgitimApps.DataBind();

    }

    protected void rptContents_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        Contents content = (Contents)e.Item.DataItem;
        Literal buAy = (Literal)e.Item.FindControl("buAy");
        Literal gecenAy = (Literal)e.Item.FindControl("gecenAy");
        Literal toplam = (Literal)e.Item.FindControl("toplam");

        DateTime now = DateTime.Now;
        buAy.Text = content.Applications.Where(p => p.CreateDate.Year == now.Year && p.CreateDate.Month == now.Month).Count().ToString();
        gecenAy.Text = content.Applications.Where(p => p.CreateDate.Year == now.AddMonths(-1).Year && p.CreateDate.Month == now.AddMonths(-1).Month).Count().ToString();
        toplam.Text = content.Applications.Count.ToString();
    }

    protected void rptEgitimApps_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        Contents cnt = (Contents)e.Item.DataItem;
        //<a class="btn btn-brand">Tümünü gör</a>
        HtmlGenericControl tumunuGor = e.Item.FindControl("tumunuGorDiv") as HtmlGenericControl;
        int count = cnt.Applications.Where(p => p.DbStatus == false).Count();
        if (count < 5)
            tumunuGor.Visible = false;
        else
            tumunuGor.InnerHtml = String.Format("<a href='AppListByContent.aspx?Id={0}' class='btn btn-brand'>Tümünü gör ({1} daha)</a>", cnt.ID, count - 5);

        Repeater rptApps = (Repeater)e.Item.FindControl("rptApps");
        rptApps.DataSource = cnt.Applications.Where(p => p.DbStatus == false).OrderByDescending(p => p.CreateDate).Take(5);
        rptApps.DataBind();
    }

    protected void rptApps_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        Applications app = (Applications)e.Item.DataItem;
        Label status = (Label)e.Item.FindControl("appStatus");
        if (app.Status == 0)
        {
            status.ForeColor = System.Drawing.Color.Orange;
            status.Text = "Onay Bekliyor";
        }
        else if (app.Status == 1)
        {
            status.ForeColor = System.Drawing.Color.PaleVioletRed;
            status.Text = "Reddedildi";
        }
        else
        {
            status.ForeColor = System.Drawing.Color.DarkGreen;
            status.Text = "Onaylandı";
        }

        Label date = (Label)e.Item.FindControl("appDate");
        date.Text = app.CreateDate.ToLongDateString() + " - " + app.CreateDate.ToShortTimeString();
    }


    protected void rptSonBasvuru_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        Applications app = (Applications)e.Item.DataItem;
        HyperLink link = (HyperLink)e.Item.FindControl("contentTitle");
        link.Text = app.Contents.ContentTitle.Length > 20 ? app.Contents.ContentTitle.Substring(0, 20) + "..." : app.Contents.ContentTitle;
        link.NavigateUrl = "AppListByContent.aspx?Id=" + app.FkContentId;
    }
}