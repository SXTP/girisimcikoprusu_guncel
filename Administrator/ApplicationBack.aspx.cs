﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Administrator_ApplicationBack : Admin
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SetHeader("Silinmiş Başvuru İşlemleri", "Silinmiş Başvurular", "", "");
        rptList.DataSource = db.Applications.Where(x => x.DbStatus == true).ToList();
        rptList.DataBind();
    }
}