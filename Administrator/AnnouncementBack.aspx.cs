﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Administrator_AnnouncementBack : Admin
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SetHeader("Silinmiş Duyuru İşlemleri", "Silinmiş Duyurular", "", "");
        rptList.DataSource = db.Announcements.Where(x => x.DbStatus == true).ToList();
        rptList.DataBind();
    }
}