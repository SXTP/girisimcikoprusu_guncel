﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Administrator/Content/MasterPage.master" AutoEventWireup="true" CodeFile="AppUnConfirmView.aspx.cs" Inherits="Administrator_AppUnConfirm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCss" Runat="Server">
     <style>
        .form-control:disabled{ background: #ffffff !important; border-color: white !important; text-align: center !important; color: black; }
        .form-group{ text-align: center !important; }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpContent" Runat="Server">
    <div class="row">
        <div class="col-md-8">
            <div class="card card-with-nav">
                <div class="card-header">
                    <div class="row row-nav-line">
                        <ul class="nav nav-tabs nav-line nav-color-secondary" role="tablist">
                            <li class="nav-item"><a class="nav-link active show" data-toggle="tab" href="#home" role="tab" aria-selected="true">Başvuru Bilgileri</a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row mt-3">
                        <div class="col-md-12">
                            <div class="form-group form-group-default">
                                <label><b>Başvurulan İçerik</b></label>
                                <a ID="ltrContentTitle" runat="server" class="form-control" style="color: palevioletred;" ></a>
                            </div>
                        </div>
                    </div>
					<div class="row mt-3">
						<div class="col-md-6">
							<div class="form-group form-group-default">
								<label><b>Başvuru Tarihi</b></label>
								<input ID="ltrCreateDate" runat="server" type="text" class="form-control" disabled />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group form-group-default">
								<label><b>Başvuru Durumu</b></label>
								<input ID="ltrStatus" runat="server" type="text" class="form-control" disabled />
							</div>
						</div>
					</div>
					<div class="row mt-3">
						<div class="col-md-6">
							<div class="form-group form-group-default">
								<label><b>Eğitime Başvurulan İl</b></label>
								<input ID="ltrBasvuruIl" runat="server" type="text" class="form-control" disabled />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group form-group-default">
								<label><b>Eğitime Başvurulan Grup</b></label>
								<input ID="ltrBasvuruGrup" runat="server" type="text" class="form-control" disabled />
							</div>
						</div>
					</div>
					<div class="row mt-3">
						<div class="col-md-6">
							<div class="form-group form-group-default">
								<label><b>E-mail</b></label>
								<input ID="ltrEmail" runat="server" type="text" class="form-control" disabled />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group form-group-default">
								<label><b>Şehir</b></label>
								<input ID="ltrSehir" runat="server" type="text" class="form-control" disabled />
							</div>
						</div>
					</div>
                    <div class="row mt-3">
						<div class="col-md-12">
							<div class="form-group form-group-default">
								<label><b>Adres</b></label>
								<input ID="ltrAdres" runat="server" type="text" class="form-control" disabled />
							</div>
						</div>
                    </div>

                    <div class="text-right mt-3 mb-3">
                        <button type="button" id="onaylaBtn" class="btn btn-success" runat="server" onclick="">Onayla</button>
                        <asp:Button id="retBtn" runat="server" class="btn btn-danger" Text ="Reddet" OnClick="retBtn_Click"></asp:Button>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card card-profile">
                <div class="card-header" style="background-image: url('../assets/img/blogpost.jpg')">
                    <div class="profile-picture">
                        <div class="avatar avatar-xl">
                           
                            <img src=" <asp:Literal ID="ltrAppImage" runat="server"></asp:Literal>" alt="..." class="avatar-img rounded-circle">
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="user-profile text-center">
                        <div class="name">
                            <asp:Literal ID="ltrUserNameSurname" runat="server"></asp:Literal></div>
                        
										<div class="job"> <asp:Literal ID="ltrUserOld" runat="server"></asp:Literal></div>
                        <a ID="ltrUserPhone" runat="server" class="desc btn btn-danger btn-border btn-round"></a>
                        <div class="view-profile">
                            <a href="#" class="btn btn-secondary btn-block">Detaylı İncele</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphJs" Runat="Server">
     <script>
        $("#cpContent_onaylaBtn").on("click", function () {
            swal({
				title: 'Emin misiniz?',
				text: "Başvuruyu onaylamak istediğinize emin misiniz?",
				type: 'warning',
				buttons:{
					confirm: {
						text: 'Onayla',
						className: 'btn btn-success'
					},
					cancel: {
                        visible: true,
                        text: "İptal",
						className: 'btn btn-danger'
					}
				}
			}).then((Delete) => {
                if (Delete) {
                    
                    var id = $("#cpContent_onaylaBtn").data("id");
                    
                    $().newAjaxAdminHTML({},
                    {
                        funcName: "BasvuruOnayla",
                        newData: "{'id': '" + id + "'}",
                        onSuccess: function (msg) {
                            window.location.href = window.location.href;
                        }
                    });

				} else {
					swal.close();
				}
			});
        });
    </script>
</asp:Content>

