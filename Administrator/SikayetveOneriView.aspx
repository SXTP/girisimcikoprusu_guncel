﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Administrator/Content/MasterPage.master" AutoEventWireup="true" CodeFile="SikayetveOneriView.aspx.cs" Inherits="Administrator_SikayetveOneriView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphCss" Runat="Server">
    <style>
        .form-control:disabled{ background: #ffffff !important; border-color: white !important; text-align: center !important; color: black; }
        .form-group{ text-align: center !important; }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpContent" Runat="Server">
     <div class="row">
        <div class="col-md-8">
            <div class="card card-with-nav">
                <div class="card-header">
                    <div class="row row-nav-line">
                        <ul class="nav nav-tabs nav-line nav-color-secondary" role="tablist">
                            <li class="nav-item"><a class="nav-link active show" data-toggle="tab" href="#home" role="tab" aria-selected="true">ŞİKAYET VE ÖNERİ DETAY</a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row mt-3">
                        <div class="col-md-12">
                            <div class="form-group form-group-default">
                                <label><b>ŞİKAYET BAŞLIĞI</b></label>
                                <a ID="Baslik" runat="server" class="form-control" style="color: palevioletred;" ></a>
                            </div>
                        </div>
                    </div>
					<div class="row mt-3">
						<div class="col-md-6">
							<div class="form-group form-group-default">
								<label><b>ŞİKAYET TARİHİ</b></label>
								<input ID="Tarih" runat="server" type="text" class="form-control" disabled />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group form-group-default">
								<label><b>ŞİKAYET SAHİBİ</b></label>
								<input ID="AdSoyad" runat="server" type="text" class="form-control" disabled />
							</div>
						</div>
					</div>
					<div class="row mt-3">
						<div class="col-md-6">
							<div class="form-group form-group-default">
								<label><b>E-MAİL ADRESİ</b></label>
								<input ID="Email" runat="server" type="text" class="form-control" disabled />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group form-group-default">
								<label><b>TELEFON</b></label>
								<input ID="Telefon" runat="server" type="text" class="form-control" disabled />
							</div>
						</div>
					</div>
				
                    <div class="row mt-3">
						<div class="col-md-12">
							<div class="form-group form-group-default">
								<label><b>ŞİKAYET İÇERİĞİ</b></label>
								<input ID="Icerik" runat="server" type="text" class="form-control" disabled />
							</div>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphJs" Runat="Server">
       
</asp:Content>

