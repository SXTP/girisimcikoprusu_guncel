﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Administrator/Content/MasterApp.master" AutoEventWireup="true" CodeFile="GetCreditCart.aspx.cs" Inherits="GetCreditCart" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphC" Runat="Server">
     <div class="container" id="page_container">

        <div class="row page-row" id="dvKrediKartiBasarili" runat="server">
            <h3 class="title">Başvuru Sonucu</h3>
            <div class="form-white">
                <span style="color: red;"><asp:Literal ID="ltrError" runat="server"></asp:Literal></span>
                <asp:Literal ID="ltrBasarili" runat="server"></asp:Literal>
            </div>
        </div>
    </div>
</asp:Content>

