﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EXERT;

public partial class MasterPage : System.Web.UI.MasterPage
{
    public string active = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        using (DataEntities db=new DataEntities())
        {
            string id = Request.RawUrl.Split('/').Count() < 3 ? null : Request.RawUrl.Split('/')[2];
            if (id != null && id != "")
                active = id;
            else
                liHome.Attributes["Class"] = "active";

            var lnk = db.WebPageContents.Where(x => x.DbStatus == false && x.PageWebPublish == true && x.PageUpId == 0).ToList();
            rptMenu.DataSource = lnk;
            rptMenu.DataBind();

            //var _lnk = db.WebPageContents.Where(x => x.ID > 7 && x.ID < 12).ToList();
            //footerMenuPolitika.DataSource = _lnk;
            //footerMenuPolitika.DataBind();

            List<Settings> info = db.Settings.ToList();
            //adres.InnerHtml = info.FirstOrDefault(i => i.SettingName == "Adres").SettingValue;
            //bottomTel.InnerText = info.FirstOrDefault(i => i.SettingName == "Telefon").SettingValue;
            ////telefon.InnerText = info.FirstOrDefault(i => i.SettingName == "Telefon").SettingValue;
            ////telefon.HRef = "tel:" + info.FirstOrDefault(i => i.SettingName == "Telefon").SettingValue;
            //mailAdr.InnerText = info.FirstOrDefault(i => i.SettingName == "Mail").SettingValue;
            //mailAdr.HRef = "mailto:" + info.FirstOrDefault(i => i.SettingName == "Mail").SettingValue;
            //webSitesi.InnerText = info.FirstOrDefault(i => i.SettingName == "WebSitesi").SettingValue;
            //webSitesi.HRef = "http://" + info.FirstOrDefault(i => i.SettingName == "WebSitesi").SettingValue;
            ////topTel.InnerText = info.FirstOrDefault(i => i.SettingName == "Telefon").SettingValue;

            //Sosyal Medya
            //facebook.HRef = info.FirstOrDefault(i => i.SettingName == "Facebook").SettingValue;
            //twitter.HRef = info.FirstOrDefault(i => i.SettingName == "Twitter").SettingValue;
            //instagram.HRef = info.FirstOrDefault(i => i.SettingName == "Instagram").SettingValue;
            //linkedin.HRef = info.FirstOrDefault(i => i.SettingName == "LinkedIn").SettingValue;
            //facebook2.HRef = info.FirstOrDefault(i => i.SettingName == "Facebook").SettingValue;
            //twitter2.HRef = info.FirstOrDefault(i => i.SettingName == "Twitter").SettingValue;
            //instagram2.HRef = info.FirstOrDefault(i => i.SettingName == "Instagram").SettingValue;
            //linkedin2.HRef = info.FirstOrDefault(i => i.SettingName == "LinkedIn").SettingValue;

            //footerLogo.Src = db.Settings.FirstOrDefault(s => s.SettingName == "FooterLogo").SettingValue;
            //HeaderLogo.Src = db.Settings.FirstOrDefault(s => s.SettingName == "HeaderLogo").SettingValue;
            //HeaderLogoIki.Src = db.Settings.FirstOrDefault(s => s.SettingName == "HeaderLogoIki").SettingValue;

            lnkFavicon.Href = info.FirstOrDefault(i => i.SettingName == "İkon").SettingValue;
        }
    }

    protected void rptMenu_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        Repeater rptMenuAlt = (Repeater)e.Item.FindControl("rptMenuAlt");
        WebPageContents link = (WebPageContents)e.Item.DataItem;
        Literal ltrClass = (Literal)e.Item.FindControl("ltrClass");
        Literal ltrUpMenuName = (Literal)e.Item.FindControl("ltrUpMenuName");


        using (DataEntities ent = new DataEntities())
        {
            var list = from a in ent.WebPageContents
                       where a.DbStatus == false && a.PageUpId == link.ID
                       orderby a.PageRow ascending
                       select a;

            if (list.ToList<WebPageContents>().Count > 0)
            {
                //ltrUpMenuName.Text = "<div class='collapse' id='" + link.LinkName + "'>";

                rptMenuAlt.DataSource = list.ToList<WebPageContents>();
                rptMenuAlt.DataBind();
                //ltrClass.Text = " <span class='caret'></span>";
            }
        }
    }

    protected void aboneOlBtnClick(object sender, EventArgs e)
    {
        //string mail = "";
        //if (aboneMail.Value == "" || aboneMail.Value == null)
        //    return;
        //else
        //{
        //    using (DataEntities db = new DataEntities())
        //    {

        //        if (db.Subscribers.Any(s => s.Mail == aboneMail.Value))
        //            return;
        //        else
        //            mail = aboneMail.Value;
        //    }
        //}
        //string ip = GetUserIP();

        //Subscribers subscriber = new Subscribers { IP = ip, Mail = mail, KayıtTarihi = DateTime.Now };
        //using (DataEntities db = new DataEntities())
        //{
        //    db.Subscribers.Add(subscriber);
        //    db.SaveChanges();
        //}
        //Response.Redirect("Default.aspx");
    }

    private string GetUserIP()
    {
        string ipList = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

        if (!string.IsNullOrEmpty(ipList))
        {
            return ipList.Split(',')[0];
        }

        return Request.ServerVariables["REMOTE_ADDR"];
    }
}
