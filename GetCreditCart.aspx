﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="GetCreditCart.aspx.cs" Inherits="ApplicationFormTest" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .page_loading {
            position: fixed;
            z-index: 1000;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            background: rgba( 255, 255, 255, .8 ) url('/images/Preloader_3.gif') 50% 50% no-repeat;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpContent" runat="Server">
    <section id="home" class="divider bg-lighter">
        <div class="display-table">
            <div id="dvSuc" runat="server" class="display-table-cell">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-md-push-3">
                            <div class="bg-lightest border-1px p-25">
                                <h4 class="text-theme-colored text-uppercase m-0">BAŞVURU DURUMU</h4>
                                <div class="line-bottom mb-30"></div>
                                <p>
                                    <asp:Literal ID="ltrAppStatu" runat="server"></asp:Literal></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</asp:Content>

