﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="PageView.aspx.cs" Inherits="PageView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpContent" runat="Server">
    <section class="">
        <div class="container">
            <div class="section-content">
                <div class="row">
                    <div class="col-md-12">
                        <asp:Literal ID="ltrContent" runat="server"></asp:Literal>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>

