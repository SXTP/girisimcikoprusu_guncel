﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ContentView.aspx.cs" Inherits="ContentView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        div#cpContent_ltrContent ul{
            list-style: disc;
            margin-left: 20px;
        }
        @media screen and (min-width: 990px) {
            .hr1{display:none;}
            .hr2{display:none;}
            .tarihBox{border-right: 1px solid gray;}
            .altBox{border-right: 1px solid white;}
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpContent" runat="Server">
    <section class="">
        <div class="container">
            <section style="margin-bottom:-30px;">
                <div class="container pb-40">
                    <div class="pt-20 pb-20 bg-theme-color-2" data-margin-top="-115px" style="margin-top: -115px; margin-left:-20px;">
                        <div class="row">
                            <div class="col-md-9">
                                <h2 class="mt-5 ml-50 ml-sm-0 text-white sm-text-center font-weight-600"><asp:Literal ID="ltrContentTitle" runat="server"></asp:Literal></h2>
                            </div>
                            <div class="col-md-3 sm-text-center">
                                <asp:HyperLink ID="lnkAppNow" class="btn btn-flat btn-theme-colored btn-lg mt-5 ml-30 ml-sm-0" runat="server">Başvuru Yap<i class="fa fa-angle-double-right font-16 ml-10"></i></asp:HyperLink>
                               
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <div class="section-content">
                <div class="row">
                    <div class="col-md-12" style="text-align: center;">
                        <div class="col-md-4 tarihBox" style="min-width: 200px; padding-top: 10px; padding-bottom: 10px; text-align: center;">
                            <img style="width:280px;" src="/images/2019_logo.svg" />
                        </div>
                        <hr class="hr1" />
                        <div class="col-md-4 tarihBox" style="font-size: 16px; height:110px;" id="contentDesc" runat="server">
                            <p id="contentDescHeader" runat="server"></p>
                        </div>
                        <div class="col-md-4 tarihBox" style="font-size: 16px;" id="contentTarih" runat="server">
                            <b>Son Başvuru Tarihi:</b> <span id="sonBasvuru" runat="server"></span><br />
                            <b>Eğitim Başlangıç Tarihi:</b> <span id="egitimBaslangic" runat="server"></span><br />
                            <b>Sınav Başlangıç Tarihi:</b> <span id="sinavBaslangic" runat="server"></span><br />
                            <b>Sınav Bitiş Tarihi:</b> <span id="sinavBitis" runat="server"></span>
                        </div>
                        <hr class="hr2" />
                        <div class="col-md-4" style="font-size: 16px;">
                                <b><span style="color: #3e2772;" id="ayOzel" runat="server"></span></b>
                                <b id="indirimBox" runat="server"><span style="color: #c32881;"><span id="indirimYuzde" runat="server"></span> İNDİRİM <span id="contentPriceNormal" runat="server" style="text-decoration: line-through;"></span></span></b><br />
                                <b>Eğitim Ücreti: <span id="contentPrice" runat="server" style="color: #3e2772;"></span></b><br />
                                <b style="color: #3e2772;"></b>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <h4 class="text-theme-colored line-bottom">
                            <asp:Literal ID="ltrContentDesc" runat="server"></asp:Literal></h4>
                        <div ID="ltrContent" runat="server" class=""></div>
                    </div>
                </div>
            </div>
            <section class="bottomInfo" style="margin-bottom:-30px; padding-top: 50px;">
                <div class="container pb-40">
                    <div class="pt-20 pb-20 bg-theme-color-2" data-margin-top="-115px" style="margin-top: -115px; margin-left:-20px;">
                        <div class="row" style="color: white; text-align: center;">
                            <div class="col-md-4 altBox" style="min-width: 200px; padding-top: 10px; padding-bottom: 10px; text-align: center;">
                                <b style="color: white;"></b>
                            </div>
                            <hr class="hr1" />
                            <div class="col-md-4 altBox">
                                <asp:HyperLink ID="lnkAppNow3" class="btn btn-flat btn-theme-colored btn-lg mt-5 ml-30 ml-sm-0" runat="server">Başvuru Yap<i class="fa fa-angle-double-right font-16 ml-10"></i></asp:HyperLink>
                            </div>
                            <hr class="hr2" />
                            <div class="col-md-4" id="altIndirim" runat="server">
                                <b id="ayOzel2" runat="server"></b>
                                <b id="indirimBox2" runat="server"><span id="indirimYuzde2" runat="server"></span> İNDİRİM <span id="contentPriceNormal2" runat="server" style="text-decoration: line-through;"></span><br /></b>
                                <b>Eğitim Ücreti: <span id="contentPrice2" runat="server"></span></b><br />
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </section>
</asp:Content>

