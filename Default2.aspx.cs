﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Default2 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        List<ayır> liste = new List<ayır>();
        using (DataEntities db = new DataEntities())
        {
            var list = db.KesinBasvurular.ToList();
            foreach (var item in list)
            {
                var details = db.UserDetails.FirstOrDefault(x => x.FkUserId == item.Users.ID);
                var ara = liste.FirstOrDefault(x => x.ad == item.Users.Name && x.soyad == item.Users.Surname);
                if (ara == null)
                {
                    liste.Add(new ayır
                    {
                        id = item.Users.ID,
                        ad = item.Users.Name,
                        soyad = item.Users.Surname,
                        tel = details.PhoneNumber,
                        sehir = details.AddressCity
                    });
                }
            }

            foreach (var item in liste)
            {
                ltrYazdir.Text += item.tel + "<br>";
            }
        }
    }
}

public class ayır
{
    public int id { get; set; }
    public string ad { get; set; }
    public string soyad { get; set; }
    public string sehir { get; set; }
    public string tel { get; set; }

}
