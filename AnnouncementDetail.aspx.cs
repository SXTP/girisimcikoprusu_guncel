﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AnnouncementDetail : WebSite
{
    protected void Page_Load(object sender, EventArgs e)
    {
        var news = db.Announcements.FirstOrDefault(n => n.ID == xContentId);

        dvInfoTitle.Visible = true;
        SetHeader(news.ATitle, EXERT.SiteSettings.SiteName, news.ATitle, news.ATitle);

        haberResmi.Src = news.ABanner;
        ltrBaslik.InnerText = news.ATitle;
        ltrContent.Text = news.AContent;
        ltrTarih.Text = news.CreateDate.ToLongDateString() + " | " + news.CreateDate.ToShortTimeString();
    }
}