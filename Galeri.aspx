﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Galeri.aspx.cs" Inherits="Galeri" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpContent" runat="Server">
    <div class="container">
    <div class="row text-center">
        <div class="col-md-12">
           
            <br />
            <div id="carousel-example-2" class="carousel slide carousel-fade" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carousel-example-2" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-2" data-slide-to="1"></li>
                    <li data-target="#carousel-example-2" data-slide-to="3"></li>
                    <li data-target="#carousel-example-2" data-slide-to="4"></li>
                    <li data-target="#carousel-example-2" data-slide-to="5"></li>
                    <li data-target="#carousel-example-2" data-slide-to="6"></li>
                    <li data-target="#carousel-example-2" data-slide-to="7"></li>
                    <li data-target="#carousel-example-2" data-slide-to="8"></li>
                    <li data-target="#carousel-example-2" data-slide-to="9"></li>
                    <li data-target="#carousel-example-2" data-slide-to="10"></li>
                    <li data-target="#carousel-example-2" data-slide-to="11"></li>
                    <li data-target="#carousel-example-2" data-slide-to="12"></li>
                    <li data-target="#carousel-example-2" data-slide-to="13"></li>
                    <li data-target="#carousel-example-2" data-slide-to="14"></li>
                    <li data-target="#carousel-example-2" data-slide-to="15"></li>
                    <li data-target="#carousel-example-2" data-slide-to="16"></li>
                    <li data-target="#carousel-example-2" data-slide-to="17"></li>
                    <li data-target="#carousel-example-2" data-slide-to="18"></li>
                </ol>
                <div class="carousel-inner" role="listbox">
                    <div class="carousel-item active text-center">
                        <div class="view">
                            <img class="d-block w-100" src="https://www.sesob.org.tr/resimler/haberler/2020/03-09/genc-girisimci-liderler-gelistirme-projesi-egitimlerinin-1-asamasi-tamamlandi-1711.jpg"
                                alt="">
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="view">
                            <img class="d-block w-100" src="https://www.sesob.org.tr/resimler/haberler/2020/03-09/genc-girisimci-liderler-gelistirme-projesi-egitimlerinin-1-asamasi-tamamlandi-1717.jpg"
                                alt="">
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="view">
                            <img class="d-block w-100" src="https://www.sesob.org.tr/resimler/haberler/2020/03-09/genc-girisimci-liderler-gelistirme-projesi-egitimlerinin-1-asamasi-tamamlandi-1716.jpg"
                                alt="">
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="view">
                            <img class="d-block w-100" src="https://www.sesob.org.tr/resimler/haberler/2020/03-09/genc-girisimci-liderler-gelistirme-projesi-egitimlerinin-1-asamasi-tamamlandi-1715.jpg"
                                alt="">
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="view">
                            <img class="d-block w-100" src="https://www.sesob.org.tr/resimler/haberler/2020/03-09/genc-girisimci-liderler-gelistirme-projesi-egitimlerinin-1-asamasi-tamamlandi-1714.jpg"
                                alt="">
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="view">
                            <img class="d-block w-100" src="https://www.sesob.org.tr/resimler/haberler/2020/03-09/genc-girisimci-liderler-gelistirme-projesi-egitimlerinin-1-asamasi-tamamlandi-1712.jpg"
                                alt="">
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="view">
                            <img class="d-block w-100" src="https://www.sesob.org.tr/resimler/haberler/2020/03-09/genc-girisimci-liderler-gelistirme-projesi-egitimlerinin-1-asamasi-tamamlandi-1710.jpg"
                                alt="">
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="view">
                            <img class="d-block w-100" src="https://www.sesob.org.tr/resimler/haberler/2020/03-09/genc-girisimci-liderler-gelistirme-projesi-egitimlerinin-1-asamasi-tamamlandi-1709.jpg"
                                alt="">
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="view">
                            <img class="d-block w-100" src="https://www.sesob.org.tr/resimler/haberler/2020/03-09/genc-girisimci-liderler-gelistirme-projesi-egitimlerinin-1-asamasi-tamamlandi-1707.jpg"
                                alt="">
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="view">
                            <img class="d-block w-100" src="https://www.sesob.org.tr/resimler/haberler/2020/03-09/genc-girisimci-liderler-gelistirme-projesi-egitimlerinin-1-asamasi-tamamlandi-1705.jpg"
                                alt="">
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="view">
                            <img class="d-block w-100" src="https://www.sesob.org.tr/resimler/haberler/2020/03-09/genc-girisimci-liderler-gelistirme-projesi-egitimlerinin-1-asamasi-tamamlandi-1703.jpg"
                                alt="">
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="view">
                            <img class="d-block w-100" src="https://www.sesob.org.tr/resimler/haberler/2020/03-09/genc-girisimci-liderler-gelistirme-projesi-egitimlerinin-1-asamasi-tamamlandi-1702.jpg"
                                alt="">
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="view">
                            <img class="d-block w-100" src="https://www.sesob.org.tr/resimler/haberler/2020/03-09/genc-girisimci-liderler-gelistirme-projesi-egitimlerinin-1-asamasi-tamamlandi-1701.jpg"
                                alt="">
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="view w-100">
                            <img class="d-block w-100" src="https://www.sesob.org.tr/resimler/haberler/2020/03-09/genc-girisimci-liderler-gelistirme-projesi-egitimlerinin-1-asamasi-tamamlandi-1700.jpg"
                                alt="">
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="view">
                            <img class="d-block w-100" src="https://www.sesob.org.tr/resimler/haberler/2020/03-09/genc-girisimci-liderler-gelistirme-projesi-egitimlerinin-1-asamasi-tamamlandi-1699.jpg"
                                alt="">
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="view">
                            <img class="d-block w-100" src="https://www.sesob.org.tr/resimler/haberler/2020/03-09/genc-girisimci-liderler-gelistirme-projesi-egitimlerinin-1-asamasi-tamamlandi-1704.jpg"
                                alt="">
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="view">
                            <img class="d-block w-100" src="https://www.sesob.org.tr/resimler/haberler/2020/03-09/genc-girisimci-liderler-gelistirme-projesi-egitimlerinin-1-asamasi-tamamlandi-1706.jpg"
                                alt="">
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="view">
                            <img class="d-block w-100" src="https://www.sesob.org.tr/resimler/haberler/2020/03-09/genc-girisimci-liderler-gelistirme-projesi-egitimlerinin-1-asamasi-tamamlandi-1708.jpg"
                                alt="">
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="view">
                            <img class="d-block w-100" src="https://www.sesob.org.tr/resimler/haberler/2020/03-09/genc-girisimci-liderler-gelistirme-projesi-egitimlerinin-1-asamasi-tamamlandi-1713.jpg"
                                alt="">
                        </div>
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carousel-example-2" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Geri</span>
                </a>
                <a class="carousel-control-next" href="#carousel-example-2" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">İleri</span>
                </a>
            </div>
        </div>
    </div>
        </div>
    <br />
    <br />
</asp:Content>

