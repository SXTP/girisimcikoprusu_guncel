﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EXERT;


public partial class KesinBasvuru : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnDevamEt_Click(object sender, EventArgs e)
    {
        using (DataEntities db = new DataEntities())
        {
            //var SecilenIl = drpil.SelectedValue.ToString();
            //var ileBasvuruSayisi = db.KesinBasvurular.Where(x => x.EgitimeKatilacagiIl == SecilenIl).Count();
            //if (ileBasvuruSayisi >= 80)
            //{
            //    ltrBasvuruBilgi.Text = "<p style='color:red;'>"+ SecilenIl +" 'de kontenjan dolduğu için başvuru yapamazsınız.</p>";
            //    ltrBasvuruBilgi.Visible = true;
            //    return;
            //}
            var KayitVarMi = db.UserDetails.FirstOrDefault(x => x.Email == txtMail.Text.Trim());
            if (KayitVarMi == null)
            {
                ltrBasvuruBilgi.Text = "<p style='color:red;'>Sistemde kaydınız bulunmadığı için başvuruyu tamamlayamazsınız.</p>";
                ltrBasvuruBilgi.Visible = true;
                return;
            }
            else
            {
                var BasvuruTamamladiMi = db.KesinBasvurular.FirstOrDefault(x => x.TCNo == txtTC.Text.Trim());
                if (BasvuruTamamladiMi != null)
                {
                    ltrBasvuruBilgi.Text = "<p style='color:red;'>Tekrar başvuru yapamazsınız.</p>";
                    ltrBasvuruBilgi.Visible = true;
                    return;
                }
                else
                {
                    KesinBasvurular basvuru = new KesinBasvurular();
                    basvuru.FkUserId = db.UserDetails.FirstOrDefault(x => x.Email == txtMail.Text.Trim()).FkUserId;
                    basvuru.EgitimeKatilacagiIl = drpil.SelectedValue;
                    basvuru.TCNo = txtTC.Text.Trim();
                    db.KesinBasvurular.Add(basvuru);
                    db.SaveChanges();
                    ltrBasvuruBilgi.Text = "<p style='color:red;'>Başvurunuz Tamamlanmıştır Sizinle En Yakında Zamanda İletişime Geçilecektir.</p>";
                    ltrBasvuruBilgi.Visible = true;
                }
            }
        }
    }
}