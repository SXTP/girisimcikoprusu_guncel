﻿<h3 style="text-align: center;"><font color="#666666" face="Open Sans, sans-serif"><span style="font-size: 14px; font-family: &quot;Arial Black&quot;;"><b>Temel Girişimcilik Eğitimi Sakarya Takvimi</b></span></font></h3>
<p style="text-align: left;"><span style="font-family: Poppins;">﻿</span></p>
<table class="table table-bordered" style="text-align: center;">
    <tbody>
        <tr style="background-color: #eeece1;">
            <td>
                <p><b>GİRİŞİMCİLİK EĞİTİM PROGRAMI TEMEL DÜZEY</b></p>
                <p><b>Sakarya Programı 7-8 Mart 2020</b></p>
            </td>
            <td>
                <p><b>
                    <br>
                </b></p>
                <p><b>Süre</b></p>
            </td>
            <td>
                <p><b>
                    <br>
                </b></p>
                <p><b>Saat</b></p>
            </td>
        </tr>
        <tr style="background-color: #8db4e2;">
            <td colspan="3"><b>CUMARTESİ</b></td>
        </tr>
        <tr>
            <td style="text-align: left;">Açılış</td>
            <td style="text-align: left;">15 Dk</td>
            <td style="text-align: left;">08:30-08:45</td>
        </tr>
        <tr>
            <td style="text-align: left;">Proje Tanıtımı ve Amaçları</td>
            <td style="text-align: left;">15 Dk</td>
            <td style="text-align: left;">08:45-09:00</td>
        </tr>
        <tr>
            <td style="text-align: left;">Pretest</td>
            <td style="text-align: left;">15 Dk</td>
            <td style="text-align: left;">09:00-09:15</td>
        </tr>
        <tr>
            <td style="text-align: left;">Çay/Kahve Molası</td>
            <td style="text-align: left;">15 Dk</td>
            <td style="text-align: left;">09:15-09:30</td>
        </tr>
        <tr>
            <td style="text-align: left;">Amerika Birleşik Devletleri ve Eğitim Sistemi ve STEAM Eğitimi</td>
            <td style="text-align: left;">30 Dk</td>
            <td style="text-align: left;">09:30-10:00</td>
        </tr>
        <tr>
            <td style="text-align: left;">Çok Kültürlülük ve Kültürler Arası Yetkinlik</td>
            <td style="text-align: left;">20 Dk</td>
            <td style="text-align: left;">10:00-10:20</td>
        </tr>
        <tr>
            <td style="text-align: left;">Çay/Kahve Molası</td>
            <td style="text-align: left;">10 Dk</td>
            <td style="text-align: left;">10:20-10:30</td>
        </tr>
        <tr>
            <td style="text-align: left;">Liderlik Gelişimi</td>
            <td style="text-align: left;">30 Dk</td>
            <td style="text-align: left;">10:30-11:00</td>
        </tr>
        <tr>
            <td style="text-align: left;">Amerikan Girişimcilik Eğitimi</td>
            <td style="text-align: left;">20 Dk</td>
            <td style="text-align: left;">11:00-11:20</td>
        </tr>
        <tr>
            <td style="text-align: left;">Çay/Kahve Molası</td>
            <td style="text-align: left;">10 Dk</td>
            <td style="text-align: left;">11:20-11:30</td>
        </tr>
        <tr>
            <td style="text-align: left;">Girişimcilikte Temel Kavramlar</td>
            <td style="text-align: left;">1 Saat</td>
            <td style="text-align: left;">11:30-12:20</td>
        </tr>
        <tr style="background-color: #ffff00;">
            <td style="text-align: left;">Öğle Arası</td>
            <td style="text-align: left;">1 Saat</td>
            <td style="text-align: left;">12:30-13:30</td>
        </tr>
        <tr>
            <td style="text-align: left;">Girişim Fırsatlarını Görme ve Fikir Yaratma/Geliştirme 1/2</td>
            <td style="text-align: left;">1 Saat</td>
            <td style="text-align: left;">13:30-14:20</td>
        </tr>
        <tr>
            <td style="text-align: left;">Çay/Kahve Molası</td>
            <td style="text-align: left;">10 Dk</td>
            <td style="text-align: left;">14:20-14:30</td>
        </tr>
        <tr>
            <td style="text-align: left;">Girişim Fırsatlarını Görme ve Fikir Yaratma/Geliştirme 2/2<br>
            </td>
            <td style="text-align: left;">1 Saat</td>
            <td style="text-align: left;">14:30-15:20</td>
        </tr>
        <tr>
            <td style="text-align: left;">Çay/Kahve Molası</td>
            <td style="text-align: left;">10 Dk</td>
            <td style="text-align: left;">15:20-15:30</td>
        </tr>
        <tr>
            <td style="text-align: left;">İş Modelleri, Müşteriler, Değer Önerileri ve Gelir Kaynakları 1/2</td>
            <td style="text-align: left;">1 Saat</td>
            <td style="text-align: left;">15:30-16:20</td>
        </tr>
        <tr>
            <td style="text-align: left;">Çay/Kahve Molası</td>
            <td style="text-align: left;">10 Dk</td>
            <td style="text-align: left;">16:20-16:30</td>
        </tr>
        <tr>
            <td style="text-align: left;">İş Modelleri, Müşteriler, Değer Önerileri ve Gelir Kaynakları 2/2<br>
            </td>
            <td style="text-align: left;">1 Saat</td>
            <td style="text-align: left;">16:30-17:20</td>
        </tr>
        <tr>
            <td style="text-align: left;">Günün Özeti ve Değerlendirme Anketi</td>
            <td style="text-align: left;">10 Dk</td>
            <td style="text-align: left;">17:20-17:30</td>
        </tr>
        <tr style="background-color: #8db4e2;">
            <td style="text-align: center;" colspan="3"><b>PAZAR</b></td>
        </tr>
        <tr>
            <td style="text-align: left;">Yapılabilirlik Analizi</td>
            <td style="text-align: left;">1 Saat</td>
            <td style="text-align: left;">08:30-09:20</td>
        </tr>
        <tr>
            <td style="text-align: left;">Çay/Kahve Molası</td>
            <td style="text-align: left;">10 Dk</td>
            <td style="text-align: left;">09:20-09:30</td>
        </tr>
        <tr>
            <td style="text-align: left;">Ekonomi, Endüstri, Rekabet ve Müşteri Analizi</td>
            <td style="text-align: left;">1 Saat</td>
            <td style="text-align: left;">09:30-10:20</td>
        </tr>
        <tr>
            <td style="text-align: left;">Çay/Kahve Molası</td>
            <td style="text-align: left;">10 Dk</td>
            <td style="text-align: left;">10:20-10:30</td>
        </tr>
        <tr>
            <td style="text-align: left;">Hukuki Altyapı 1/2</td>
            <td style="text-align: left;">1 Saat</td>
            <td style="text-align: left;">10:30-11:20</td>
        </tr>
        <tr>
            <td style="text-align: left;">Çay/Kahve Molası</td>
            <td style="text-align: left;">10 Dk</td>
            <td style="text-align: left;">11:20-11:30</td>
        </tr>
        <tr>
            <td style="text-align: left;">Hukuki Altyapı 2/2</td>
            <td style="text-align: left;">1 Saat</td>
            <td style="text-align: left;">11:30-12:30</td>
        </tr>
        <tr style="background-color: #ffff00;">
            <td style="text-align: left;">Öğle Arası</td>
            <td style="text-align: left;">1 Saat</td>
            <td style="text-align: left;">12:30-13:30</td>
        </tr>
        <tr>
            <td style="text-align: left;">Pazarlama İlkeleri ve Yönetimi 1/2</td>
            <td style="text-align: left;">1 Saat</td>
            <td style="text-align: left;">13:30-14:20</td>
        </tr>
        <tr>
            <td style="text-align: left;">Çay/Kahve Molası</td>
            <td style="text-align: left;">10 Dk</td>
            <td style="text-align: left;">14:20-14:30</td>
        </tr>
        <tr>
            <td style="text-align: left;">Pazarlama İlkeleri ve Yönetimi 2/2</td>
            <td style="text-align: left;">1 Saat</td>
            <td style="text-align: left;">14:30-15:20</td>
        </tr>
        <tr>
            <td style="text-align: left;">Çay/Kahve Molası</td>
            <td style="text-align: left;">10 Dk</td>
            <td style="text-align: left;">15:20-15:30</td>
        </tr>
        <tr>
            <td style="text-align: left;">Proje Yazımı, Temel Kurallar</td>
            <td style="text-align: left;">1 Saat</td>
            <td style="text-align: left;">15:30-16:20</td>
        </tr>
        <tr>
            <td style="text-align: left;">Çay/Kahve Molası</td>
            <td style="text-align: left;">10 Dk</td>
            <td style="text-align: left;">16:20-16:30</td>
        </tr>
        <tr>
            <td style="text-align: left;">Girişimin Etik İlkeleri</td>
            <td style="text-align: left;">1 Saat</td>
            <td style="text-align: left;">16:30-17:20</td>
        </tr>
        <tr>
            <td style="text-align: left;">Posttest ve Kapanış</td>
            <td style="text-align: left;">25 Dk</td>
            <td style="text-align: left;">17:20-17:45</td>
        </tr>
        <tr>
            <td style="text-align: right;"><b>Toplam</b></td>
            <td style="text-align: center;" colspan="2"><b>16 Saat</b></td>
        </tr>
    </tbody>
</table>
<p style="text-align: center;"><span style="font-family: Poppins;">
    <br>
</span></p>
<h3 style="text-align: center;"><font color="#666666" face="Open Sans, sans-serif"><span style="font-size: 14px; font-family: &quot;Arial Black&quot;;"><b>Temel Girişimcilik Eğitimi Yalova-Bilecik-Bolu-Düzce Takvimi</b></span></font></h3>
<p style="text-align: left;"><span style="font-family: Poppins;">﻿</span></p>
<table class="table table-bordered" style="text-align: center;">
    <tbody>
        <tr style="background-color: #eeece1;">
            <td>
                <p><b>GİRİŞİMCİLİK EĞİTİM PROGRAMI TEMEL DÜZEY</b></p><p><b>Yalova Programı 7-8 Mart 2020</b></p>
                <p><b>Bilecik/Bolu/Düzce Programı 14-15 Mart 2020</b></p>
            </td>
            <td>
                <p><b>
                    <br>
                </b></p>
                <p><b>Süre</b></p>
            </td>
            <td>
                <p><b>
                    <br>
                </b></p>
                <p><b>Saat</b></p>
            </td>
        </tr>
        <tr style="background-color: #8db4e2;">
            <td colspan="3"><b>CUMARTESİ</b></td>
        </tr>
        <tr>
            <td style="text-align: left;">Açılış</td>
            <td style="text-align: left;">15 Dk</td>
            <td style="text-align: left;">08:30-08:45</td>
        </tr>
        <tr>
            <td style="text-align: left;">Proje Tanıtımı ve Amaçları</td>
            <td style="text-align: left;">15 Dk</td>
            <td style="text-align: left;">08:45-09:00</td>
        </tr>
        <tr>
            <td style="text-align: left;">Pretest</td>
            <td style="text-align: left;">15 Dk</td>
            <td style="text-align: left;">09:00-09:15</td>
        </tr>
        <tr>
            <td style="text-align: left;">Çay/Kahve Molası</td>
            <td style="text-align: left;">15 Dk</td>
            <td style="text-align: left;">09:15-09:30</td>
        </tr>
        <tr>
            <td style="text-align: left;">Girişimcilikte Temel Kavramlar</td>
            <td style="text-align: left;">1 Saat</td>
            <td style="text-align: left;">09:30-10:20</td>
        </tr>
        
        <tr>
            <td style="text-align: left;">Çay/Kahve Molası</td>
            <td style="text-align: left;">10 Dk</td>
            <td style="text-align: left;">10:20-10:30</td>
        </tr>
        <tr>
            <td style="text-align: left;">Girişim Fırsatlarını Görme ve Fikir Yaratma/Geliştirme 1/2<br></td>
            <td style="text-align: left;">1 Saat</td>
            <td style="text-align: left;">10:30-11:20</td>
        </tr>
        
        <tr>
            <td style="text-align: left;">Çay/Kahve Molası</td>
            <td style="text-align: left;">10 Dk</td>
            <td style="text-align: left;">11:20-11:30</td>
        </tr>
        <tr>
            <td style="text-align: left;">Girişim Fırsatlarını Görme ve Fikir Yaratma/Geliştirme 2/2<br></td>
            <td style="text-align: left;">1 Saat</td>
            <td style="text-align: left;">11:30-12:20</td>
        </tr>
        <tr style="background-color: #ffff00;">
            <td style="text-align: left;">Öğle Arası</td>
            <td style="text-align: left;">1 Saat</td>
            <td style="text-align: left;">12:20-13:30</td>
        </tr>
        <tr>
            <td style="text-align: left;">İş Modelleri, Müşteriler, Değer Önerileri ve Gelir Kaynakları 1/2<br></td>
            <td style="text-align: left;">1 Saat</td>
            <td style="text-align: left;">13:30-14:20</td>
        </tr>
        <tr>
            <td style="text-align: left;">Çay/Kahve Molası</td>
            <td style="text-align: left;">10 Dk</td>
            <td style="text-align: left;">14:20-14:30</td>
        </tr>
        <tr>
            <td style="text-align: left;">İş Modelleri, Müşteriler, Değer Önerileri ve Gelir Kaynakları 2/2<br>
            </td>
            <td style="text-align: left;">1 Saat</td>
            <td style="text-align: left;">14:30-15:20</td>
        </tr>
        <tr>
            <td style="text-align: left;">Çay/Kahve Molası</td>
            <td style="text-align: left;">10 Dk</td>
            <td style="text-align: left;">15:20-15:30</td>
        </tr>
        <tr>
            <td style="text-align: left;">Ekonomi, Endüstri, Rekabet ve Müşteri Analizi&nbsp;1/2</td>
            <td style="text-align: left;">1 Saat</td>
            <td style="text-align: left;">15:30-16:20</td>
        </tr>
        <tr>
            <td style="text-align: left;">Çay/Kahve Molası</td>
            <td style="text-align: left;">10 Dk</td>
            <td style="text-align: left;">16:20-16:30</td>
        </tr>
        <tr>
            <td style="text-align: left;">Ekonomi, Endüstri, Rekabet ve Müşteri Analizi&nbsp;2/2<br>
            </td>
            <td style="text-align: left;">1 Saat</td>
            <td style="text-align: left;">16:30-17:20</td>
        </tr>
        <tr>
            <td style="text-align: left;">Günün Özeti ve Değerlendirme Anketi</td>
            <td style="text-align: left;">10 Dk</td>
            <td style="text-align: left;">17:20-17:30</td>
        </tr>
        <tr style="background-color: #8db4e2;">
            <td style="text-align: center;" colspan="3"><b>PAZAR</b></td>
        </tr>
        <tr>
            <td style="text-align: left;">Yapılabilirlik Analizi</td>
            <td style="text-align: left;">1 Saat</td>
            <td style="text-align: left;">09:00-09:50</td>
        </tr>
        <tr>
            <td style="text-align: left;">Çay/Kahve Molası</td>
            <td style="text-align: left;">10 Dk</td>
            <td style="text-align: left;">09:50-10:00</td>
        </tr>
        
        
        <tr>
            <td style="text-align: left;">Hukuki Altyapı 1/2</td>
            <td style="text-align: left;">1 Saat</td>
            <td style="text-align: left;">10:00-10:50</td>
        </tr>
        <tr>
            <td style="text-align: left;">Çay/Kahve Molası</td>
            <td style="text-align: left;">10 Dk</td>
            <td style="text-align: left;">10:50-11:00</td>
        </tr>
        <tr>
            <td style="text-align: left;">Hukuki Altyapı 2/2</td>
            <td style="text-align: left;">1 Saat</td>
            <td style="text-align: left;">11:00-11:50</td>
        </tr>
        <tr style="background-color: #ffff00;">
            <td style="text-align: left;">Öğle Arası</td>
            <td style="text-align: left;">1 Saat</td>
            <td style="text-align: left;">12:00-13:00</td>
        </tr>
        <tr>
            <td style="text-align: left;">Pazarlama İlkeleri ve Yönetimi 1/2</td>
            <td style="text-align: left;">1 Saat</td>
            <td style="text-align: left;">13:00-13:50</td>
        </tr>
        <tr>
            <td style="text-align: left;">Çay/Kahve Molası</td>
            <td style="text-align: left;">10 Dk</td>
            <td style="text-align: left;">13:50-14:00</td>
        </tr>
        <tr>
            <td style="text-align: left;">Pazarlama İlkeleri ve Yönetimi 2/2</td>
            <td style="text-align: left;">1 Saat</td>
            <td style="text-align: left;">14:00-14:50</td>
        </tr>
        <tr>
            <td style="text-align: left;">Çay/Kahve Molası</td>
            <td style="text-align: left;">10 Dk</td>
            <td style="text-align: left;">14:50-15:00</td>
        </tr>
        <tr>
            <td style="text-align: left;">Proje Yazımı, Temel Kurallar</td>
            <td style="text-align: left;">1 Saat</td>
            <td style="text-align: left;">15:00-15:50</td>
        </tr>
        <tr>
            <td style="text-align: left;">Çay/Kahve Molası</td>
            <td style="text-align: left;">10 Dk</td>
            <td style="text-align: left;">15:50-16:00</td>
        </tr>
        <tr>
            <td style="text-align: left;">Girişimin Etik İlkeleri</td>
            <td style="text-align: left;">1 Saat</td>
            <td style="text-align: left;">16:00-16:50</td>
        </tr><tr><td style="text-align: left;">Çay/Kahve Molası</td><td style="text-align: left;">10 Dk</td><td style="text-align: left;">16:50-17:00</td></tr><tr><td style="text-align: left;">Günün Özeti</td><td style="text-align: left;">15 Dk</td><td style="text-align: left;">17:00-17:15</td></tr>
        <tr>
            <td style="text-align: left;">Posttest ve Kapanış</td>
            <td style="text-align: left;">30 Dk</td>
            <td style="text-align: left;">17:15-17:45</td>
        </tr>
        <tr>
            <td style="text-align: right;"><b>Toplam</b></td>
            <td style="text-align: center;" colspan="2"><b>16 Saat</b></td>
        </tr>
    </tbody>
</table>
<p style="text-align: center;"><span style="font-family: Poppins;">
    <br>
</span></p>
<h3 style="text-align: center;"><font color="#666666" face="Open Sans, sans-serif"><span style="font-size: 14px; font-family: &quot;Arial Black&quot;;"><b>İleri Düzey Girişimcilik Eğitimi Sakarya Takvimi</b></span></font></h3>
<p style="text-align: left;"><span style="font-family: Poppins;">﻿</span></p>
<table class="table table-bordered" style="text-align: center;">
    <tbody>
        <tr style="background-color: #eeece1;">
            <td>
                <p><b>GİRİŞİMCİLİK EĞİTİM PROGRAMI </b></p><p><b>İLERİ DÜZEY (2nci Safha)</b></p>
                <p><b>Sakarya Programı 13-14 Haziran 2020</b></p>
            </td>
            <td>
                <p><b>
                    <br>
                </b></p>
                <p><b>Süre</b></p>
            </td>
            <td>
                <p><b>
                    <br>
                </b></p>
                <p><b>Saat</b></p>
            </td>
        </tr>
        <tr style="background-color: #8db4e2;">
            <td colspan="3"><b>CUMARTESİ</b></td>
        </tr>
        <tr>
            <td style="text-align: left;">Yaratıcı Düşünce Bulma, Kurma ve Geliştirme</td><td style="text-align: left;">1 Saat</td>
            <td style="text-align: left;">08:30-09:20</td>
        </tr>
        <tr>
            <td style="text-align: left;">Çay/Kahve Molası<br></td>
            <td style="text-align: left;">10 Dk</td>
            <td style="text-align: left;">09:20-09:30</td>
        </tr>
        <tr>
            <td style="text-align: left;">Yapılabilirlik Analizi, SWOT Analizi</td>
            <td style="text-align: left;">1 Saat</td><td style="text-align: left;">09:30-10:20</td>
        </tr>
        <tr>
            <td style="text-align: left;">Çay/Kahve Molası</td>
            <td style="text-align: left;">10 Dk</td>
            <td style="text-align: left;">10:20-10:30</td>
        </tr>
        <tr>
            <td style="text-align: left;">İş Süreçlerini Modelleme</td><td style="text-align: left;">1 Saat</td>
            <td style="text-align: left;">10:30-11:20</td>
        </tr>
        
        <tr>
            <td style="text-align: left;">Çay/Kahve Molası</td>
            <td style="text-align: left;">10 Dk</td>
            <td style="text-align: left;">11:20-11:30</td>
        </tr>
        <tr>
            <td style="text-align: left;">Finansal Süreçler ve Yönetimi</td><td style="text-align: left;">1 Saat</td>
            <td style="text-align: left;">11:30-12:20</td>
        </tr>
        
        
        
        <tr style="background-color: #ffff00;">
            <td style="text-align: left;">Öğle Arası</td>
            <td style="text-align: left;">1 Saat</td>
            <td style="text-align: left;">12:30-13:30</td>
        </tr>
        <tr>
            <td style="text-align: left;">Pazarlama Süreçleri ve Yönetimi 1/2<br></td>
            <td style="text-align: left;">1 Saat</td>
            <td style="text-align: left;">13:30-14:20</td>
        </tr>
        <tr>
            <td style="text-align: left;">Çay/Kahve Molası</td>
            <td style="text-align: left;">10 Dk</td>
            <td style="text-align: left;">14:20-14:30</td>
        </tr>
        <tr>
            <td style="text-align: left;">Pazarlama Süreçleri ve Yönetimi&nbsp;2/2<br>
            </td>
            <td style="text-align: left;">1 Saat</td>
            <td style="text-align: left;">14:30-15:20</td>
        </tr>
        <tr>
            <td style="text-align: left;">Çay/Kahve Molası</td>
            <td style="text-align: left;">10 Dk</td>
            <td style="text-align: left;">15:20-15:30</td>
        </tr>
        <tr>
            <td style="text-align: left;">Proje Yazımı (Mentorlar Desteğinde) 1/5</td>
            <td style="text-align: left;">1 Saat</td>
            <td style="text-align: left;">15:30-16:20</td>
        </tr>
        <tr>
            <td style="text-align: left;">Çay/Kahve Molası</td>
            <td style="text-align: left;">10 Dk</td>
            <td style="text-align: left;">16:20-16:30</td>
        </tr>
        <tr>
            <td style="text-align: left;">Proje Yazımı (Mentorlar Desteğinde) 2/5<br>
            </td>
            <td style="text-align: left;">1 Saat</td>
            <td style="text-align: left;">16:30-17:20</td>
        </tr>
        <tr>
            <td style="text-align: left;">Günün Özeti ve Değerlendirme Anketi</td>
            <td style="text-align: left;">10 Dk</td>
            <td style="text-align: left;">17:20-17:30</td>
        </tr>
        <tr style="background-color: #8db4e2;">
            <td style="text-align: center;" colspan="3"><b>PAZAR</b></td>
        </tr>
        <tr>
            <td style="text-align: left;">Proje Yazımı (Mentorlar Desteğinde) 3/5</td><td style="text-align: left;">1 Saat</td>
            <td style="text-align: left;">08:30-09:20</td>
        </tr>
        <tr>
            <td style="text-align: left;">Çay/Kahve Molası</td>
            <td style="text-align: left;">10 Dk</td>
            <td style="text-align: left;">09:20-09:30</td>
        </tr>
        
        
        <tr>
            <td style="text-align: left;">Proje Yazımı (Mentorlar Desteğinde) 4/5</td><td style="text-align: left;">1 Saat</td>
            <td style="text-align: left;">09:30-10:20</td>
        </tr>
        <tr>
            <td style="text-align: left;">Çay/Kahve Molası</td>
            <td style="text-align: left;">10 Dk</td>
            <td style="text-align: left;">10:20-10:30</td>
        </tr>
        <tr>
            <td style="text-align: left;">Proje Yazımı (Mentorlar Desteğinde) 5/5</td><td style="text-align: left;">1 Saat</td>
            <td style="text-align: left;">10:30-11:20</td>
        </tr><tr><td style="text-align: left;">Çay/Kahve Molası</td><td style="text-align: left;">10 Dk</td><td style="text-align: left;">11:20-11:30</td></tr><tr><td style="text-align: left;">Eğitim Programının Değerlendirilmesi</td><td style="text-align: left;">1 Saat</td><td style="text-align: left;">11:30-12:20</td></tr>
        <tr style="background-color: #ffff00;">
            <td style="text-align: left;">Öğle Arası</td>
            <td style="text-align: left;">1 Saat</td>
            <td style="text-align: left;">12:00-13:00</td>
        </tr>
        <tr>
            <td style="text-align: left;">Proje Sunumları (3 Grup)</td>
            <td style="text-align: left;">1 Saat</td>
            <td style="text-align: left;">13:00-14:00</td>
        </tr>
        <tr>
            <td style="text-align: left;">Çay/Kahve Molası</td>
            <td style="text-align: left;">15 Dk</td>
            <td style="text-align: left;">14:00-14:15</td>
        </tr>
        <tr>
            <td style="text-align: left;">Proje Sunumları (3 Grup)<br></td>
            <td style="text-align: left;">1 Saat</td>
            <td style="text-align: left;">14:15-15:15</td>
        </tr>
        <tr>
            <td style="text-align: left;">Çay/Kahve Molası</td>
            <td style="text-align: left;">15 Dk</td>
            <td style="text-align: left;">15:15-15:30</td>
        </tr>
        <tr>
            <td style="text-align: left;">Proje Sunumları (3 Grup)<br></td>
            <td style="text-align: left;">1 Saat</td>
            <td style="text-align: left;">15:30-16:30</td>
        </tr>
        <tr>
            <td style="text-align: left;">Çay/Kahve Molası</td>
            <td style="text-align: left;">15 Dk</td>
            <td style="text-align: left;">16:30-16:45</td>
        </tr>
        <tr>
            <td style="text-align: left;">Kapanış Toplantısı, Sertifika Töreni</td>
            <td style="text-align: left;">45 Dk</td><td style="text-align: left;">16:45-17:30</td>
        </tr>
        
        <tr>
            <td style="text-align: right;"><b>Toplam</b></td>
            <td style="text-align: center;" colspan="2"><b>16 Saat</b></td>
        </tr>
    </tbody>
</table>
<p style="text-align: center;"><span style="font-family: Poppins;">
    <br>
</span></p>
