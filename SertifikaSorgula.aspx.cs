﻿using EXERT;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SertifikaSorgula : System.Web.UI.Page
{
    
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    protected void btnDevamEt_Click(object sender, EventArgs e)
    {
        string adSoyad = ttAdSoyad.Text.Trim();
        DirectoryInfo d = new DirectoryInfo(Server.MapPath("/sertifikalar/"));//Assuming Test is your Folder
        FileInfo[] Files = d.GetFiles("*.pdf"); //Getting Text files
        for (int i = 0; i < Files.Length; i++)
        {
            if (Files[i].Name.Contains(adSoyad.UrlMaker()))
            {
                Response.Redirect("/sertifikalar/" + Files[i].Name);
            }            
        }
        ltrBasvuruBilgi.Text = "Lütfen girdiğiniz bilgileri kontrol edin girilen ad soyada ait bir sertifika bulunamadı!";
    }
}