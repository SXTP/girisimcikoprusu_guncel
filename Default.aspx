﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cpContent" runat="Server">
    <div class="container pb-40 pt-40" style="padding-top: 15px;">
        <div class="row">
            <div class="col-md-8">
                Değerli Proje Ortağımız,<br />
                <br />
                Öncelikle sizlerin ve sevdiklerinizin sağlıklı olmalarını umuyoruz.
                <br />
                <br />
                <strong>İnovasyona Dayalı Genç Girişimci Liderler Geliştirme Projesi'nde </strong>göstermiş olduğunuz başarı sonucunda, <strong>“İleri Düzey Girişimcilik Eğitimi”</strong>
                ve <strong>“İleri Proje Yazma Eğitimi'ne”</strong> katılma ve ayrıca <strong>“Projenizi Sunma”</strong>  hakkını kazanmıştınız. 
                <br />
                <br />
                Sizi tekrar tebrik ediyoruz.
                <br />
                <br />
                Projenin etkilerinin tabana yayılması ve katma değer yaratması açısından hem asıl hem de yedek listesinde yer alan katılımcılarımız <strong>İleri Düzey Girişimcilik Eğitimi'ne</strong> davet edilmiştir.
                <br />
                <br />
                Alınan karar gereği sadece 1nci değil en iyi 2 ve 3ncü projeler de ödüllendirilecektir.
 <br />
                Haziran ayı içerisinde kazanmış olduğunuz eğitimlerin tamamlanması ve mentörünüz danışmanlığında projelerinizin yazım sürecinin bitirilmesi planlanmıştır.
                <br />
                <br />

                Sürece ilişkin takvimimiz şu şekildedir;
                <br />
                <br />
                <table class="table table-bordered">
                    <tr>
                        <td>12 Haziran Cumartesi
                            <br />
                            Saat: 21:30-23:30
                        </td>
                        <td>Adım Adım Proje Yazımı
                            <br />
                            (Online, Zoom üzerinden)
                        </td>
                        <td>Proje fikrinin girişime dönüşebilmesi için gerekli işlemler, uygulamalı olarak, adım adım paylaşılacak.
                        </td>
                    </tr>
                    <tr>
                        <td>13 Haziran Pazar 
                            <br />
                            Saat: 13:00-16:30
                        </td>
                        <td>İleri Düzey Girişimcilik Eğitimi 
(Online, Zoom üzerinden)
                        </td>
                        <td>–	Yeni fikirler bulma,
                            <br />
                            –	SWOT Analizi,
                            <br />
                            –	İş süreçlerinin modellenmesi,
                            <br />
                            –	Finans yönetimi,
                            <br />
                            –	İleri düzey pazarlama,
                            <br />
                            –	Soru cevap<br />

                        </td>
                    </tr>
                    <tr>
                        <td>16-30 Haziran
                        </td>
                        <td>Mentörlük Hizmeti
(Online, Zoom veya Google Meet üzerinden)</td>
                        <td>Bu tarih aralığında danışmanınızdan randevu alarak projenizle ilgili olarak mentörlük (rehberlik/danışmanlık) hizmeti alabileceksiniz.</td>
                    </tr>
                    <tr>
                        <td>3-4 Temmuz 
Cumartesi-Pazar
(Sakarya’da yüz yüze)</td>
                        <td>–	Proje sunumları<br />
                            –	Değerlendirme<br />
                            –	Ödül Töreni
                            <br />
                        </td>
                        <td>Ulaşım, konaklama, yeme-içme giderleri proje kapsamında karşılanacaktır.</td>
                    </tr>
                </table>


                <strong>Notlar:</strong>

                <br />
                <br />

                1.	<strong>11 Haziran 2021 Cuma, 12-13 Haziran 2021 (Cumartesi-Pazar) </strong> günkü eğitimlere ilişkin linkler mailinize ve cep telefonunuza SMS olarak gönderilecektir.<br />
                2.	<strong>16 Hazirandan</strong> itibaren yazdığınız veya kafanızda şekillendirdiğiniz projenize ilişkin değerlendirmeler yapmak, görüş almak için mentörünüzden mail yoluyla randevu almalı ve görüşmelerinizi yapmalısınız.<br />
                3.	<strong>16-30 Haziran </strong> arasındaki periyotta danışmanınızdan alacağınız mentörlükle, proje süreçlerinizi adım adım gözden geçirme imkanınız olacaktır.<br />
                4.	En geç <strong>30 Haziran Çarşamba 23.59</strong> 'a kadar proje öneri dosyanızı <strong>www.girisimcikoprusu.com</strong> adresine yüklemelisiniz. Projeniz (konularına göre) uzman değerlendirme komisyonuna ataması yapılacak, internet sayfasında duyurulan tarih ve saatte sunumunuzu (canlı/yüz yüze) yapmanız beklenecektir.<br />
                5.	Zoom veya Google Meet oturumlarında Kullanıcı Adı kısmına lütfen Ad ve Soyadınızı eksiksiz yazınız, rumuz kullanmayınız. 


                <br />
                <br />
                <%--<a href="/ApplicationForm.aspx" class="btn btn-primary" style="background: red;">BAŞVURU</a>--%>
            </div>
            <div class="col-md-4" style="margin: 1rem 0px;">
                <img src="/images/anasayfa.png" />
            </div>
        </div>
    </div>
    <div class="modal" id="ModalDuyuru" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">DUYURU</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    İLERİ DÜZEY GİRİŞİMCİLİK EĞİTİMİNE KATILACAKLAR ASIL/YEDEK LİSTESİ AÇIKLANDI. 
                    SONUÇLARI GÖRMEK İÇİN <a href="/Uploads/liste.pdf" target="_blank">Tıklayınız</a>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Kapat</button>
                </div>
            </div>
        </div>
    </div>
    <%--    <div class="row">
        <div class="col-md-8" style="margin: 1rem 0px;">
            <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
                    <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">
                    <asp:Repeater ID="rptBanner" runat="server">
                        <ItemTemplate>
                            <div class="carousel-item <%# SonSlider(Convert.ToInt32(Eval("ID")))%>">
                                <a href="<%# Eval("BLink") %>">
                                    <img src="<%# Eval("BBanner") %>" height="400px" class="d-block w-100"></a>
                                <div class="carousel-caption d-none d-md-block">
                                    <span style="background-color: #aeaeae; opacity: 0.5;">
                                        <h3 style="color: #FFF;"><%# Eval("BTitle") %></h3>
                                    </span>
                                    <p><%# Eval("BSubtitle") %></p>
                                    <p><%# Eval("BContent") %></p>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                    <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-4" style="margin: 1rem 0px;">
            <h3 class="text-uppercase line-bottom mt-0 line-height-1"><i class="fa fa-calendar mr-10"></i>Son <span class="text-theme-color-2">Duyurular</span></h3>
            <asp:Repeater ID="rptAnnouncement" runat="server">
                <ItemTemplate>
                    <article class="post media-post clearfix pb-0 mb-10">
                        <a href="/AnnouncementDetail.aspx?Id=<%#Eval("ID") %>" class="post-thumb mr-20">
                            <img width="150" height="150" alt="" src="<%#Eval("ABanner") %>"></a>
                        <div class="post-right">
                            <h4 class="mt-0 mb-5"><a href="/AnnouncementDetail.aspx?Id=<%#Eval("ID") %>"><%#Eval("ATitle") %></a></h4>
                            <ul class="list-inline font-12 mb-5">
                                <li class="pr-0"><i class="fa fa-calendar mr-5"></i><%#(Eval("CreateDate") as DateTime?).Value.ToLongDateString()%> <%#(Eval("CreateDate") as DateTime?).Value.ToLongTimeString() %> |</li>
                            </ul>
                            <p class="mb-0 font-13">
                                <%# Eval("AContent") %>
                            </p>
                            <a class="text-theme-colored font-13" href="/AnnouncementDetail.aspx?Id=<%#Eval("ID") %>">Devamı →</a>
                        </div>
                    </article>
                </ItemTemplate>
            </asp:Repeater>
        </div>
    </div>--%>


    <section class="bg-lighter" style="background-color: #eeeeee; width: 100%;">
        <div class="container pb-40" style="padding-top: 15px;">
            <div class="section-title mb-0">
                <div class="row">
                    <div class="col-md-12">
                        <asp:Literal ID="ltrContent" runat="server"></asp:Literal>

                        <%--<p style="text-align: center; background: white;">
    <object data="/images/mainafis.svg" type="image/svg+xml"></object></p>--%>
                    </div>
                </div>
            </div>

        </div>
    </section>
    <section id="event" style="width: 100%;">
        <div class="container pb-50">
            <div class="section-content">
                <div class="row">
                    <div class="col-md-12" style="text-align: center;">
                        <!-- içerik -->
                    </div>
                </div>
            </div>
        </div>
    </section>



</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        @media screen and (min-width: 1200px) {
            .content {
                height: 107px;
            }

            .bilgiBox {
                width: 240px;
            }

            .fiyatBox {
                width: 120px;
            }

            .bannerCont {
                margin-top: 15px;
            }
        }

        @media screen and (max-width: 1199px) and (min-width: 990px) {
            .content {
                height: 107px;
            }

            .bilgiBox {
                width: 190px;
            }

            .fiyatBox {
                width: 100px;
            }

            .bannerBtn {
                margin-top: 150px;
            }

            .bannerCont {
                margin-top: 100px;
                width: 90vw !important;
            }
        }

        @media screen and (max-width: 989px) and (min-width: 768px) {
            .content {
                height: 107px;
            }

            .bilgiBox {
                width: 220px;
            }

            .fiyatBox {
                width: 125px;
            }

            .bannerBtn {
                margin-top: 150px;
            }

            .bannerCont {
                margin-top: 100px;
                font-size: 20px;
                width: 90vw !important;
            }
        }

        @media screen and (max-width: 767px) and (min-width: 450px) {
            .content {
                width: 400px;
            }

            .bilgiBox {
                width: 250px;
            }

            .fiyatBox {
                width: 145px;
            }

            .bannerBtn {
                margin-top: 150px;
            }

            .bannerCont {
                margin-top: 100px;
                font-size: 20px;
                width: 90vw !important;
            }
        }

        @media screen and (max-width: 449px) {
            .content {
                width: 300px;
            }

            .bilgiBox {
                width: 200px;
            }

            .fiyatBox {
                width: 100px;
            }

            .bannerBtn {
                margin-top: 150px;
            }

            .bannerCont {
                display: none !important;
            }

            .rev_slider_wrapper {
                height: 300px !important;
            }
        }
    </style>
    <script>
        $(document).ready(function () {
            //<$("#ModalDuyuru").modal('show');
        })
    </script>
</asp:Content>
