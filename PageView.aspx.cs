﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PageView : WebSite
{
    
    protected void Page_Load(object sender, EventArgs e)
    {
        //string Type = Request["Type"];
        //if (Type=="Egitim")
        //{
        xContentId =Convert.ToInt32(Page.RouteData.Values["ID"]);
        var cnt = db.WebPageContents.FirstOrDefault(x => x.ID == xContentId && x.DbStatus == false);
        if (cnt != null)
        {
            dvInfoTitle.Visible = true;
            SetHeader(cnt.PageTitle, EXERT.SiteSettings.SiteName, cnt.PageTitle, cnt.PageTitle);
            ltrContent.Text = cnt.PageContent;
            
        }
        else
            Response.Redirect("/AnaSayfa");
        //}


    }
}