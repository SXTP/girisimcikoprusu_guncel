﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="SertifikaSorgula.aspx.cs" Inherits="SertifikaSorgula" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpContent" runat="Server">
    <section id="home" class="divider bg-lighter">
        <div class="display-table">
            <div id="dvForm" runat="server" class="display-table-cell">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-md-push-3">
                            <span style="color: red;">
                                <asp:Literal ID="ltrBasvuruBilgi" runat="server"></asp:Literal>
                            </span>
                            <div class="bg-lightest border-1px p-25">
                                <h4 class="text-theme-colored text-uppercase m-0">SERTİFİKA SORGULAMA FORMU</h4>
                                <div class="line-bottom mb-30"></div>
                                <div id="job_apply_form">
                                    <div class="form-group name">
                                        <label for="TCNumaraniz">Ad Soyad</label>
                                        <asp:TextBox ID="ttAdSoyad" class="form-control" value="" placeholder="Ad Soyad Giriniz.." runat="server"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container" style="padding-top: 0px; margin-top: -35px;">
                    <div class="row">
                        <div class="col-md-12" style="text-align: center;">
                            <asp:Button ID="btnDevamEt" OnClick="btnDevamEt_Click" runat="server" class="btn btn-primary" Text="SORGULA" ValidationGroup="grpKayit"></asp:Button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>

