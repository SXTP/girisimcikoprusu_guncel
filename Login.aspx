﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Giriş -
        <asp:Literal ID="ltrPageTitle" runat="server"></asp:Literal></title>
    <meta content="width=device-width, initial-scale=1.0, shrink-to-fit=no" name="viewport" />
    <link rel="icon" href="https://via.placeholder.com/32x32" type="image/x-icon" />

    <!-- Fonts and icons -->
    <script src="../assets/js/plugin/webfont/webfont.min.js"></script>
    <script>
        WebFont.load({
            google: { "families": ["Lato:300,400,700,900"] },
            custom: { "families": ["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands", "simple-line-icons"], urls: ['../assets/css/fonts.min.css'] },
            active: function () {
                sessionStorage.fonts = true;
            }
        });
    </script>
    <!-- CSS Files -->
    <link rel="stylesheet" href="../Administrator/assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="../Administrator/assets/css/millenium.min.css" />
</head>
<body class="login">
    <form id="form1" runat="server">
        <div class="wrapper wrapper-login wrapper-login-full p-0">
            <div class="login-aside w-50 d-flex flex-column align-items-center justify-content-center text-center bg-secondary-gradient">
                <h1 class="title fw-bold text-white mb-3">
                    <asp:Literal ID="ltrPageName" runat="server"></asp:Literal></h1>
                <p class="subtitle text-white op-7">
                    <asp:Literal ID="ltrAppName" runat="server"></asp:Literal>
                </p>
                <p class="subtitle text-white op-7">
                    <img src="/Administrator/assets/img/118x40.png" />
                </p>
            </div>
            <div class="login-aside w-50 d-flex align-items-center justify-content-center bg-white">
                <div class="container container-login container-transparent animated fadeIn">

                    <h3 class="text-center">Lütfen Giriş Yapınız</h3>
                    <div class="login-form">
                        <asp:Label ID="lblInfo" runat="server" Text=""></asp:Label>
                        <div class="form-group">
                            <label for="username" class="placeholder"><b>Kullanıcı Adı</b></label>
                            <asp:TextBox ID="txtUserName" class="form-control" runat="server"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label for="password" class="placeholder"><b>Şifreniz</b></label>
                            <a href="#" class="link float-right">Şifremi Unuttum ?</a>
                            <div class="position-relative">
                                <asp:TextBox ID="txtPassword" TextMode="Password" class="form-control" runat="server"></asp:TextBox>
                                <div class="show-password">
                                    <i class="icon-eye"></i>
                                </div>
                            </div>
                        </div>
                        <div class="form-group form-action-d-flex mb-3">
                            <div class="custom-control custom-checkbox">
                                <span class="custom-control-input">
                                    <asp:CheckBox ID="chkRemember" runat="server" />
                                </span>
                                <label class="custom-control-label m-0" for="rememberme">Beni Hatırla</label>
                            </div>
                            <asp:Button ID="btnLogin" runat="server" class="btn btn-secondary col-md-5 float-right mt-3 mt-sm-0 fw-bold" Text="Giriş Yap" OnClick="btnLogin_Click" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="../assets/js/core/jquery.3.2.1.min.js"></script>
        <script src="../assets/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
        <script src="../assets/js/core/popper.min.js"></script>
        <script src="../assets/js/core/bootstrap.min.js"></script>
        <script src="../assets/js/millenium.min.js"></script>


        <script type="text/javascript">
            //<![CDATA[

            theForm.oldSubmit = theForm.submit;
            theForm.submit = WebForm_SaveScrollPositionSubmit;

            theForm.oldOnSubmit = theForm.onsubmit;
            theForm.onsubmit = WebForm_SaveScrollPositionOnSubmit;
//]]>
        </script>
    </form>
</body>
</html>
