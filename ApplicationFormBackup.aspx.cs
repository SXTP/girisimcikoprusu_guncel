﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EXERT;


public partial class ApplicationFormBackup : WebSite
{
    protected void Page_Load(object sender, EventArgs e)
    {

        dvInfoTitle.Visible = false;
        dvSuc.Visible = false;
        chkKrediKarti.Checked = false;
        chkKrediKarti.Visible = false;
        chkEftHavale.Checked = true;
        dvkrediKarti.Visible = false;
        xContentId = Convert.ToInt32(Page.RouteData.Values["ID"]);

        if (!IsPostBack)
        {
            var cnt = db.Contents.FirstOrDefault(x => x.ID == xContentId && x.DbStatus == false);
            if (cnt != null)
            {
                txtEgitimAdi.Text = cnt.ContentTitle;
                txtEgitimUcreti.Text = cnt.ContentPrice.ToString() + " TL";
                this.Title = cnt.ContentTitle + " - Eğitim Başvuru - " + EXERT.SiteSettings.SiteName;
                txtEgitimAdi.Text = cnt.ContentTitle;
                txtEgitimUcreti.Text = cnt.ContentPrice.ToString() + " TL";

                if (cnt.ContentType > 0)
                    divEgitimIl.Visible = true;
                else
                    divEgitimIl.Visible = false;

                ListItem taksit1 = new ListItem("TEK ÇEKİM - " + string.Format("{0:0.00}", cnt.ContentPrice) + " TL", "1");
                ListItem taksit3 = new ListItem("3 TAKSİT - " + string.Format("{0:0.00}", Taksitlendir(3, cnt.ContentPrice)) + " TL", "3");
                ListItem taksit9 = new ListItem("9 TAKSİT - " + string.Format("{0:0.00}", Taksitlendir(9, cnt.ContentPrice)) + " TL", "9");
                drpKKTaksit.Items.Add(taksit1);
                drpKKTaksit.Items.Add(taksit3);
                drpKKTaksit.Items.Add(taksit9);

                drpDogumYil.DataTextField = "Texter";
                drpDogumYil.DataValueField = "Texter";
                drpDogumYil.DataSource = Numbers.NumberList(DateTime.Now.Year - 15, DateTime.Now.Year - 70, false);
                drpDogumYil.DataBind();
            }
            //else
            //    Response.Redirect("/");
        }
    }

    protected void chkKrediKarti_CheckedChanged(object sender, EventArgs e)
    {

    }

    protected void chkEftHavale_CheckedChanged(object sender, EventArgs e)
    {

    }

    protected void btnDevamEt_Click(object sender, EventArgs e)
    {
        //User Control
        xContentId = Convert.ToInt32(Page.RouteData.Values["ID"]);

        var usr = db.Users.FirstOrDefault(x => x.UserName == txtTC.Text);
        var appC = db.Applications.FirstOrDefault(x => x.Users.UserName == txtTC.Text && x.FkContentId == xContentId);
        if (appC!=null)
        {
            ltrBasvuruBilgi.Text = "Tekrar Başvuru Yapamazsınız.Başvurunuz Mevcuttur.";
            return;
        }
        if (usr == null)
        {
            usr = new Users();
            usr.UserName = txtTC.Text;
            usr.Name = txtAd.Text;
            usr.Surname = txtSoyad.Text;
            usr.Password = HashTool.GetMD5(txtTC.Text.Substring(0, 5));
            usr.ProfilImg = "";
            usr.Status = 0;
            db.Users.Add(usr);

            UserDetails usd = new UserDetails();
            usd.FkUserId = usr.ID;
            usd.Address = txtAdres.Text;
            usd.AddressCity = drpil.SelectedValue;
            usd.AddressPostCode = txtPostaKodu.Text;
            usd.AddressStreet = txtIlce.Text;
            usd.BirthYear = Convert.ToInt32(drpDogumYil.SelectedValue);
            usd.CreateDate = DateTime.Now;
            usd.Email = txtMail.Text;
            usd.PhoneNumber = txtCepTel.Text;
            usd.PhoneNumber2 = "";
            db.UserDetails.Add(usd);

            Applications app = new Applications();
            app.FkUserId = usr.ID;
            app.FkContentId = xContentId;
            app.CreateDate = DateTime.Now;
            app.DbStatus = false;
            app.IPAdress = "";
            app.PaymentType = 1;
            app.Status = 0;
            if (db.Contents.FirstOrDefault(c => c.ID == xContentId).ContentType > 0)
                app.ContentField1 = drpEgitimIl.SelectedValue;
            else
                app.ContentField1 = null;
            db.Applications.Add(app);

            db.SaveChanges();
            dvForm.Visible = false;
            dvSuc.Visible = true;
            ltrAppStatu.Text = "Başvurunuz Alınmıştır.";

            db.Notifications.Add(new Notifications
            {
                BContent = txtAd.Text + " " + txtSoyad.Text + " başvuru yaptı.",
                UserID = 1,
                Readed = false,
                Time = DateTime.Now
            });

            db.SaveChanges();
        }
        else
        {
            var usd = db.UserDetails.FirstOrDefault(x => x.FkUserId == usr.ID);
            usd.FkUserId = usr.ID;
            usd.Address = txtAdres.Text;
            usd.AddressCity = drpil.SelectedValue;
            usd.AddressPostCode = txtPostaKodu.Text;
            usd.AddressStreet = txtIlce.Text;
            usd.BirthYear = Convert.ToInt32(drpDogumYil.SelectedValue);
            usd.CreateDate = DateTime.Now;
            usd.Email = txtMail.Text;
            usd.PhoneNumber = txtCepTel.Text;
            usd.PhoneNumber2 = "";

            Applications app = new Applications();
            app.FkUserId = usr.ID;
            app.FkContentId = xContentId;
            app.CreateDate = DateTime.Now;
            app.DbStatus = false;
            app.IPAdress = "";
            app.PaymentType = 1;
            app.Status = 0;
            if (db.Contents.FirstOrDefault(c => c.ID == xContentId).ContentType > 0)
                app.ContentField1 = drpEgitimIl.SelectedValue;
            else
                app.ContentField1 = null;
            db.Applications.Add(app);

            db.SaveChanges();

            dvForm.Visible = false;
            dvSuc.Visible = true;
            ltrAppStatu.Text = "Başvurunuz Alınmıştır.";

            db.Notifications.Add(new Notifications
            {
                BContent = txtAd.Text + " " + txtSoyad.Text + " başvuru yaptı.",
                UserID = 1,
                Readed = false,
                Time = DateTime.Now
            });

            db.SaveChanges();
        }
    }

    protected void cv2_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = (drpil.SelectedIndex > 0);
    }
    protected decimal Taksitlendir(int taksit, decimal ucret)
    {

        decimal tutar = ucret;

        if (taksit == 1)
        {
            tutar = ucret;
        }
        else if (taksit == 3)
        {
            tutar = (ucret * ((Decimal)1.06));
        }
        else if (taksit == 9)
        {
            tutar = (ucret * ((Decimal)1.15));
        }

        return tutar;
    }
    public static string KarakterDuzelt(string metin)
    {
        return metin.Replace('ş', 's').Replace('Ş', 'S').Replace('ç', 'c').Replace("Ç", "C").Replace('ğ', 'g').Replace('Ğ', 'G').
            Replace('ü', 'u').Replace('Ü', 'U').Replace('ı', 'i').Replace('İ', 'I').Replace('ö', 'o').Replace('Ö', 'O').
            Replace(' ', '-').Replace("?", "").Replace(",", "").Replace("/", "").Replace(".", "").Replace("\"", "").Replace("#", "");  //Gibi Gibi
    }
}