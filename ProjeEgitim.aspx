﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ProjeEgitim.aspx.cs" Inherits="ProjeEgitim" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpContent" runat="Server">
    <section id="egitim">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="egitimDetay">
                        <h2 class="egitimBaslik">Proje Dijital Eğitim Seti</h2>
                        <p class="aciklama">
                            Proje Dijital Eğitim Seti içerikleri aşağıda görüntülenmektedir. Eğitim içeriği alanından linke tıklayarak eğitim videosuna ulaşabilirsiniz. Eğitim dokümanlarını indirmek için indirme butonuna tıklayabilirsiniz.
                        </p>
                        <br />
                        <br />
                        <table class="table table-bordered">
                            <tr>
                                <td><strong>Eğitim Adı</strong>
                                </td>
                                <td><strong>Eğitim İçeriği</strong>
                                </td>
                                <td><strong>Eğitim Dokümanı</strong>
                                </td>
                            </tr>
                            <tr>
                                <td>1.Faz Temel Girişimcilik Eğitimi
                                </td>
                                <td></td>
                                <td><a href="/Uploads/egitimdosya/TemelGirisimcilikEgitimi.pdf" target="_blank">Doküman İndir <i class="fa fa-download"></i></a></td>
                            </tr>
                            <tr>
                                <td>2. Faz İleri Düzey Girişimcilik Eğitimi
                                </td>
                                <td><a href="https://www.youtube.com/watch?v=nrLwMD_xSvg" target="_blank">https://www.youtube.com/watch?v=nrLwMD_xSvg</a>
                                </td>
                                <td><a href="/Uploads/egitimdosya/İleriDüzeyGirisimcilikEgitimi.pdf" target="_blank">Doküman İndir <i class="fa fa-download"></i></a>
                            </tr>
                            <tr>
                                <td>2. Faz Adım Adım Proje Yazma Eğitimi
                                </td>
                                <td><a href="https://www.youtube.com/watch?v=20xPiQDm_lQ" target="_blank">https://www.youtube.com/watch?v=20xPiQDm_lQ</a>
                                </td>
                                <td><a href="/Uploads/egitimdosya/AdimAdimProjeYazmaEgitimi.pdf" target="_blank">Doküman İndir <i class="fa fa-download"></i></a>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>

