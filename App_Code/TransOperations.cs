﻿using ePayment;
using EXERT;
using System;
using System.Web;

/// <summary>
/// Summary description for TransOperations
/// </summary>
public class TransOperations
{

    public TransOperations()
    {

    }
    public static string Bankhost = "https://sanalpos.halkbank.com.tr/fim/api";
    public static string Bankname = "uludagadmin";
    public static string Bankpassword = "KUTU9090";
    public static string Bankclientid = "500119278";
    public static string Bankstorekey = "90gjDgjg";
    public static string HataYazisi(string ResponseCode, string sErrMsg)
    {
        string ResponseText = string.Empty;

        int[] iHatalar = new int[] { 3, 4, 5, 6, 7, 13, 15, 17, 19, 21, 25, 28, 29, 30, 31, 32, 37, 38, 39, 51, 52, 53, 63, 68, 75, 76, 77, 78, 80, 81, 82, 83, 85, 86, 88, 89, 91, 92, 94, 95, 96, 98 };
        if (ResponseCode == "99" && sErrMsg == "The card failed compliancy checks")
            ResponseText = "Kredi kart numarası geçerli değil";
        else if (ResponseCode == "99" && sErrMsg == "The card has expired")
            ResponseText = "Kartın son kullanım tarihi geçmiş.";
        else if (ResponseCode == "99" && sErrMsg == "Insufficient permissions to perform requested operation")
            ResponseText = "Kullanıcı hatası (belirli bir işlem yaparken o işleme yetkisi olmayan bir kullanıcı kullanılmış. Mağaza kodunu, kullanıcı adını ve şifresini gözden geçiriniz.)";
        else if (ResponseCode == "99" && sErrMsg == "Value for element 'Total' is not valid.")
            ResponseText = "Currency kodu hatalı.";
        else if (ResponseCode == "99" && (sErrMsg == "Unable to determine card type. ('length' is '16')" || sErrMsg == "The card failed compliancy checks" || sErrMsg == "Kredi karti numarasi gecerli formatta degil."))
            ResponseText = "Kart Tanımlanamadı. Lütfen kart numaranızı kontrol ediniz.";
        else if (ResponseCode == "93" && sErrMsg == "Transaction cannot be completed (violation of law)")
            ResponseText = "İşlem tamamlanamadı. İşleyiş kurallarından biri çiğnendi.";
        else if (ResponseCode == "54" && sErrMsg == "Expired Card")
            ResponseText = "Kart son kullanım tarihi hatalı.";
        else if (ResponseCode == "51" && sErrMsg == "Not sufficient funds")
            ResponseText = "Kredi kartınızın bakiyesi yetersiz. Başka bir kredi kartı ile tekrar deneyiniz.";
        else if (ResponseCode == "82" && sErrMsg == "Incorrect CVV")
            ResponseText = "Geçersiz CVV kodu girildi.";
        else if (ResponseCode == "10" && sErrMsg == "Reserved")
            ResponseText = "Yetkisiz işlem.";
        else if (ResponseCode == "1" || ResponseCode == "2")
            ResponseText = "Kredi kartınız için bankanız provizyon talep etmektedir. İşlem sonuçlanmamıştır.";
        else if (ResponseCode == "8")
            ResponseText = "Kart üzerindeki bilgileri kontrol ederek tekrar deneyiniz.";
        else if (ResponseCode == "9")
            ResponseText = "Kredi kartınız yenilenmiş. Lütfen yeni kredi kartı bilgileriniz ile tekrar deneyiniz.";
        else if (ResponseCode == "12")
            ResponseText = "Kartınızın arka yüzünde bulunan CVV kodu yanlış veya Bu karta taksit yapılamaz";
        else if (ResponseCode == "14")
            ResponseText = "Girmiş olduğunuz kart numarası hatalı.";
        else if (ResponseCode == "16")
            ResponseText = "Kredi kartınızın bakiyesi yetersiz. Başka bir kredi kartı ile tekrar deneyiniz.";
        else if (ResponseCode == "18")
            ResponseText = "Kartınız kullanıma kapanmış durumdadır.";
        else if (ResponseCode == "33")
            ResponseText = "Kartınızın süresi dolmuş durumdadır.";
        else if (ResponseCode == "34" || ResponseCode == "43")
            ResponseText = "Kartınız çalıntı olarak saptanmış durumda olduğu için işleminizi gerçekleştiremedik.";
        else if (ResponseCode == "36")
            ResponseText = "Kartınız sınırlandırılmış olduğu için işleminizi gerçekleştiremedik.";
        else if (ResponseCode == "41")
            ResponseText = "Kartınız kayıp olarak saptanmış durumda olduğu için işleminizi gerçekleştiremedik.";
        else if (ResponseCode == "51")
            ResponseText = "Kredi kartınızın bakiyesi yetersiz. Başka bir kredi kartı ile tekrar deneyiniz.";
        else if (ResponseCode == "54")
            ResponseText = "Kredi kartınızın son kullanma tarihi hatalı yada eksik. Bilgileri kontrol edip tekrar deneyiniz.";
        else if (ResponseCode == "56")
            ResponseText = "Girmiş olduğunuz bilgilerle eşleşen kredi kartı bulunmamaktadır. Başka bir kredi kartı ile tekrar deneyiniz.";
        else if (ResponseCode == "57")
            ResponseText = "Bu işlem için kredi kartınıza izin verilmedi. Başka bir kredi kartı ile tekrar deneyiniz.";
        else if (ResponseCode == "58")
            ResponseText = "Yetkisiz bir işlem yapıldı. Örn: Kredi kartınızın ait olduğu banka dışında bir bankadan taksitlendirme yapıyor olabilirsiniz. Başka bir kredi kartı ile işlem yapmayı deneyiniz.";
        else if (ResponseCode == "61")
            ResponseText = "Kartınızın para çekme limiti üst sınırdadır. Başka bir kredi kartı ile tekrar deneyiniz.";
        else if (ResponseCode == "62")
            ResponseText = "Kartınızın kısıtlandırılmıştır. Kartınız sadece kendi ülkenizde geçerlidir. Başka bir kredi kartı ile tekrar deneyiniz.";
        else if (ResponseCode == "65")
            ResponseText = "Kredi kartınızın günlük işlem limiti dolmuştur. Başka bir kredi kartı ile deneyiniz.";
        else if (ResponseCode == "90")
            ResponseText = "Gün sonu işlemi yapılıyor. Kısa bir süre sonra tekrar deneyiniz.";
        else if (ResponseCode == "91")
            ResponseText = "Bankanıza ulaşılamıyor. Başka bir kredi kartı ile tekrar deneyiniz.";
        else if (ResponseCode == "93")
            ResponseText = "Hukuki nedenlerden dolayı işleminiz reddedildi.";
        else if (Array.IndexOf(iHatalar, ResponseCode) >= 0)
            ResponseText = "İşleminiz onaylanmadı. Lütfen kısa bir süre sonra tekrar deneyiniz.";
        else
            ResponseText = "Bir hata oluştu (Hata Kodu:" + ResponseCode + ") Tekrar deneyiniz. Sorun devam ederse lütfen bizimle temasa geçiniz.";

        return ResponseText;
    }

    public static bool DoSaleTrans(string kkNo, int iSonKullanmaAy, int iSonKullanmaYil, string kkCvc, string orderId, decimal amount, string currency, string instnumber,
                                    string sKullaniciIsim, string sAdres, string sSehir, string sFirmaAdi, string sTelNo,
                                    out string approvedCode, out string Authcode, out string HostlogKey, out string ResponseCode, out string ResponseText, out string ResponseXML)
    {
        bool rez = false;
        approvedCode = string.Empty;
        Authcode = string.Empty;
        HostlogKey = string.Empty;
        ResponseCode = string.Empty;
        ResponseText = string.Empty;
        ResponseXML = string.Empty;

        cc5payment mycc5pay = GetPayment();
        mycc5pay.orderresult = 0;
        mycc5pay.oid = orderId;
        mycc5pay.cardnumber = kkNo;
        mycc5pay.expmonth = string.Format("{0:00}", iSonKullanmaAy);
        mycc5pay.expyear = string.Format("{0:00}", iSonKullanmaYil);
        mycc5pay.cv2 = kkCvc;
        mycc5pay.subtotal = string.Format("{0:0.00}", amount);
        mycc5pay.currency = currency;
        mycc5pay.chargetype = "Auth";

        mycc5pay.bname = sKullaniciIsim;
        mycc5pay.baddr1 = sAdres;
        mycc5pay.bcity = sSehir;
        mycc5pay.bcompany = sFirmaAdi;
        mycc5pay.phone = sTelNo;

        approvedCode = mycc5pay.processorder();
        Authcode = mycc5pay.transid;
        HostlogKey = mycc5pay.refno;
        ResponseCode = mycc5pay.procreturncode;

        string sErrMsg = mycc5pay.errmsg;

        //Oid1.Text = mycc5pay.oid;
        //out = mycc5pay.groupid;
        //ttappr1.Text = mycc5pay.appr;
        ResponseXML = sErrMsg;
        if (ResponseCode == "00")
            rez = true;
        else
        {
            ResponseText = HataYazisi(ResponseCode, sErrMsg);
            rez = false;
        }

        return rez;
    }



    public static cc5payment GetPayment()
    {
        cc5payment mycc5pay = new ePayment.cc5payment();
        mycc5pay.host = Bankhost;
        mycc5pay.name = Bankname;
        mycc5pay.password = Bankpassword;
        mycc5pay.clientid = Bankclientid;

        return mycc5pay;
    }

    private static cc5payment TestPayment()
    {
        cc5payment mycc5pay = new ePayment.cc5payment();
        mycc5pay.host = "https://testsanalpos.est.com.tr/servlet/est3Dgate";
        mycc5pay.name = "HALKBANKAPI";
        mycc5pay.password = "HALKBANK05";
        mycc5pay.clientid = "500500000";

        return mycc5pay;
    }
    /*
    public static void AidatlarAddtoCache(List<OdenmemisAidat> aidatlar, string firmaId)
    {
        List<OdenmemisAidat> CacheAidatlar = (List<OdenmemisAidat>)HttpContext.Current.Cache["aidatlar-" + firmaId];
        if (CacheAidatlar != null)
        {
            HttpContext.Current.Cache.Remove("aidatlar-" + firmaId);
        }
        HttpContext.Current.Cache.Insert("aidatlar-" + firmaId, aidatlar, null, DateTime.Now.AddMinutes(20), TimeSpan.Zero, System.Web.Caching.CacheItemPriority.NotRemovable, null);
    }*/


    public static void FormHTMLAddtoCache(string sFormHTML, string userId)
    {
        string CacheHTML = (string)HttpContext.Current.Cache["odeme-" + userId];
        if (CacheHTML != null)
        {
            HttpContext.Current.Cache.Remove("odeme-" + userId);
        }
        HttpContext.Current.Cache.Insert("odeme-" + userId, sFormHTML, null, DateTime.Now.AddMinutes(1), TimeSpan.Zero, System.Web.Caching.CacheItemPriority.NotRemovable, null);
    }

    public static string TransactionIdCreate()
    {
        DateTime dt = DateTime.Now;

        return string.Format("ULUSEM{0}{1}{2}{3}{4}{5}{6}",
            dt.Year,
            PaddedZero(dt.Month, 2),
            PaddedZero(dt.Day, 2),
            PaddedZero(dt.Hour, 2),
            PaddedZero(dt.Minute, 2),
            PaddedZero(dt.Second, 2),
            PaddedZero(Numbers.Random(99999), 5)
            );

    }

    public static string PaddedZero(int i, int iDgt)
    {
        if (iDgt == 5)
            return i.ToString("00000");

        if (iDgt == 4)
            return i.ToString("0000");

        if (iDgt == 3)
            return i.ToString("000");

        if (iDgt == 2)
            return i.ToString("00");

        return string.Empty;

    }
}