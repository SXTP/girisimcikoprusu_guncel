﻿using EXERT;
using System.Linq;

/// <summary>
/// Summary description for Admin
/// </summary>
public class Admin : AdminBasePage
{
    public DataEntities db = new DataEntities();

    public Users LoginController(string UserName, string Password)
    {
        string pass = HashTool.GetMD5(Password);
        return db.Users.FirstOrDefault(x => x.UserName == UserName && x.Password == pass);
    }
}