﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class _Default : WebSite
{
    protected void Page_Load(object sender, EventArgs e)
    {
        dvInfoTitle.Visible = false;
        this.Title = EXERT.SiteSettings.SiteName;
        var cnt = db.Contents.FirstOrDefault(x => x.ID == 1086);
        //rptContents.DataSource = cnt;
        //rptContents.DataBind();

        var ann = db.Announcements.Where(a => a.DbStatus == false).OrderByDescending(a => a.CreateDate).Take(5).ToList();
        foreach (var announcement in ann)
        {
            string str = Regex.Replace(announcement.AContent, "<.*?>", String.Empty);
            if (str.Length > 150)
                str = str.Substring(0, 150) + "...";
            announcement.AContent = str;
        }
        var news = db.News.Where(n => n.DbStatus == false).OrderByDescending(a => a.CreateDate).Take(5).ToList();
        foreach (var _news in news)
        {
            string str = Regex.Replace(_news.NContent, "<.*?>", String.Empty);
            if (str.Length > 150)
                str = str.Substring(0, 150) + "...";
            _news.NContent = str;
        }
        //rptAnnouncement.DataSource = ann;
        ////rptNews.DataSource = news;
        //rptAnnouncement.DataBind();
        ////rptNews.DataBind();

        //var banners = db.Banners.Where(b => b.DbStatus == false).ToList();
        //    foreach (var banner in banners)
        //    {
        //        banner.BContent = Regex.Replace(banner.BContent, "<.*?>", String.Empty);
        //    }
        //    rptBanner.DataSource = banners;
        //    rptBanner.DataBind();

        //ltrContent.Text = cnt.ContentDescription;
    }
    public static string SonSlider(int id)
    {
        using(DataEntities db = new DataEntities())
        {
            var banners = db.Banners.Where(b => b.DbStatus == false).ToList().LastOrDefault();
            if (banners.ID == id)
            {
                return "active";
            }
            else
            {
                return "";
            }
        }

    }
}