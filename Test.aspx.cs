﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EXERT;


public partial class Test : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        using (DataEntities db = new DataEntities())
        {
            foreach (var item in db.UserDetails.Where(p => p.AddressCityNo == null))
            {
                if (item.AddressCity == "Bursa")
                    item.AddressCityNo = 16;
                else if (item.AddressCity == "Balıkesir")
                    item.AddressCityNo = 10;
                else if (item.AddressCity == "Kocaeli")
                    item.AddressCityNo = 41;
                else if (item.AddressCity == "Sakarya")
                    item.AddressCityNo = 54;
                else if (item.AddressCity == "Çanakkale")
                    item.AddressCityNo = 17;
                else if (item.AddressCity == "İstanbul")
                    item.AddressCityNo = 34;
                else if (item.AddressCity == "Ankara")
                    item.AddressCityNo = 6;
            }
            db.SaveChanges();
        }
    }
}