﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ProjeBasvuru.aspx.cs" Inherits="ProjeBasvuru" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpContent" runat="Server">
    <section id="home" class="divider bg-lighter">
      
        <div class="display-table">
            <div id="dvForm" runat="server" class="display-table-cell">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-md-push-3">
                            <span style="color: red;">
                                <asp:Literal ID="ltrBasvuruBilgi" runat="server"></asp:Literal>
                            </span>
                            <div class="bg-lightest border-1px p-25">
                                <h4 class="text-theme-colored text-uppercase m-0">Proje Başvuru Formu</h4>
                                <div class="line-bottom mb-30"></div>
                                <div class="alert alert-danger" id="dvHata" runat="server" visible="false">Hata</div>
                                <div id="job_apply_form">
                                    <div class="form-group name">
                                        <label>TC Kimlik Numaranız</label>
                                        <asp:TextBox ID="txtTC" class="form-control" value="" data-mask="99999999999" placeholder="TC Kimlik Numaranız" runat="server"></asp:TextBox>
                                    </div>
                                    <div class="form-group name">
                                        <label>Adınız</label>
                                        <asp:TextBox ID="txtAd" class="form-control" value="" placeholder="Adınız" runat="server"></asp:TextBox>
                                    </div>
                                    <div class="form-group name">
                                        <label>Soyadınız</label>
                                        <asp:TextBox ID="txtSoyad" class="form-control" value="" placeholder="Soyadınız" runat="server"></asp:TextBox>
                                    </div>
                                    <div class="form-group name">
                                        <label>Proje Dosyası</label>
                                        <asp:FileUpload ID="fUp" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container" style="padding-top: 0px; margin-top: -35px;">
                    <div class="row">
                        <div class="col-md-12" style="text-align: center;">
                            <asp:Button ID="btnDevamEt" OnClick="btnDevamEt_Click" runat="server" class="btn btn-primary" Text="BAŞVUR" ValidationGroup="grpKayit"></asp:Button>
                        </div>
                    </div>
                </div>
            </div>
            <div id="dvSuc" runat="server" visible="false" class="display-table-cell">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-md-push-3">
                            <div class="bg-lightest border-1px p-25">
                                <h4 class="text-theme-colored text-uppercase m-0">BAŞVURU DURUMU</h4>
                                <div class="line-bottom mb-30"></div>
                                <p>
                                    <asp:Literal ID="ltrAppStatu" runat="server"></asp:Literal>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </section>
</asp:Content>

