﻿using EXERT;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ContentGetPrice : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Guid YxContentGuid = QueryStringOperations.GetGuidValue("guid");
        string CacheHTML = (string)HttpContext.Current.Cache["odeme-" + YxContentGuid];
        if (CacheHTML != null)
        {
            ltrHidden.Text = CacheHTML;
        }
    }
}