﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Sikayet : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string adSoyad = Request.QueryString["adSoyad"];
        string mail = Request.QueryString["mail"];
        string telefon = Request.QueryString["telefon"];
        string baslik = Request.QueryString["baslik"];
        string icerik = Request.QueryString["icerik"];

        using (DataEntities db = new DataEntities())
        {
            SikayetOneri s = new SikayetOneri
            {
                AdSoyad = adSoyad,
                Baslik = baslik,
                Email = mail,
                Icerik = icerik,
                Tarih = DateTime.Now,
                Telefon = telefon
            };
            db.SikayetOneri.Add(s);
        }
    }
}