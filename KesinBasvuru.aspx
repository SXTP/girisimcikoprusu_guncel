﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="KesinBasvuru.aspx.cs" Inherits="KesinBasvuru" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .page_loading {
            position: fixed;
            z-index: 1000;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            background: rgba( 255, 255, 255, .8 ) url('/images/Preloader_3.gif') 50% 50% no-repeat;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpContent" runat="Server">
    <section id="home" class="divider bg-lighter">
        <div class="display-table">
            <div id="dvForm" runat="server" class="display-table-cell">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-md-push-3">
                            <div class="bg-lightest border-1px p-25">
                                <asp:Literal ID="ltrBasvuruBilgi" Visible="false" runat="server"></asp:Literal>
                                <h4 class="text-theme-colored text-uppercase m-0">KİŞİSEL BİLGİLER</h4>
                                <div class="line-bottom mb-30"></div>
                                <div id="job_apply_form" name="job_apply_form" action="includes/job.php" method="post" enctype="multipart/form-data" novalidate="novalidate">
                                    <div class="form-group basvuruError">
                                        <asp:ValidationSummary ValidationGroup="grpKayit" ID="vs1" runat="server" ShowMessageBox="false" ShowSummary="true" />
                                    </div>

                                    <div class="form-group name">
                                        <label for="TCNumaraniz">TC Numaranız</label>
                                        <asp:TextBox ID="txtTC" class="form-control" value="" data-mask="99999999999" ValidationGroup="grpKayit" placeholder="TC Numaranızı Giriniz.." runat="server"></asp:TextBox>
                                    </div>
                                    <div class="form-group name">
                                        <label for="adiSoyadi">Adınız<span class="required">*</span></label>
                                        <asp:TextBox ID="txtAd" class="form-control" value="" ValidationGroup="grpKayit" placeholder="Adınızı Giriniz..." runat="server"></asp:TextBox>
                                    </div>
                                    <div class="form-group name">
                                        <label for="adiSoyadi">Soyadınız<span class="required">*</span></label>
                                        <asp:TextBox ID="txtSoyad" class="form-control" value="" ValidationGroup="grpKayit" placeholder="Soyadınızı Giriniz..." runat="server"></asp:TextBox>
                                    </div>
                                    <div class="form-group email">
                                        <label for="eMail">E-Mail<span class="required">(Sık Kullandığınız E-Mail Adresinizi Dikkatlice Giriniz. Sistem Üzerindeki Tüm İşlemler E-Mail Üzerinden Yapılmaktadır.)</span></label>
                                        <asp:TextBox ID="txtMail" TextMode="Email" ValidationGroup="grpKayit" class="form-control" value="" placeholder="E-Mail Adresinizi Giriniz..." runat="server"></asp:TextBox>
                                    </div>
                                    <div class="form-group message">
                                        <label for="il">Programa Katılacağınız İl</label>
                                        <asp:DropDownList ID="drpil" class="form-control" runat="server" ValidationGroup="grpKayit">
                                            <asp:ListItem Value="0">İl Se&#231;iniz</asp:ListItem>
                                            <asp:ListItem Value="Bilecik">Bilecik</asp:ListItem>
                                            <asp:ListItem Value="Bolu">Bolu</asp:ListItem>
                                            <asp:ListItem Value="Sakarya">Sakarya</asp:ListItem>
                                            <asp:ListItem Value="Yalova">Yalova</asp:ListItem>
                                            <asp:ListItem Value="Düzce">Düzce</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="container" style="padding-top: 0px; margin-top: -35px;">
                    <div class="row">
                        <div class="col-md-12" style="text-align: center;">
                            <asp:Button ID="btnDevamEt" OnClick="btnDevamEt_Click" runat="server" class="btn btn-primary" Text="BAŞVURUYU TAMAMLA" ValidationGroup="grpKayit"></asp:Button>

                            <asp:RequiredFieldValidator ValidationGroup="grpKayit" ID="rfv1" runat="server" ControlToValidate="txtTC" ErrorMessage="TC Kimlik Numaranızı giriniz" Display="None"></asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ValidationGroup="grpKayit" ID="rfv2" runat="server" ControlToValidate="txtAd" ErrorMessage="Adınızı giriniz" Display="None"></asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ValidationGroup="grpKayit" ID="rfv3" runat="server" ControlToValidate="txtSoyad" ErrorMessage="Soyadınızı giriniz" Display="None"></asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ValidationGroup="grpKayit" ID="rfv4" runat="server" ControlToValidate="txtMail" ErrorMessage="E-mail adresinizi giriniz" Display="None"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="rev1" ValidationGroup="grpKayit" runat="server" ErrorMessage="E-mail adresinizi hatalı girdiniz" ControlToValidate="txtMail" Text="*" Display="None" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                            <%--<asp:RequiredFieldValidator ValidationGroup="grpKayit" ID="rfv5" runat="server" ControlToValidate="txtCepTel" ErrorMessage="Telefon numaranızı giriniz" Display="None"></asp:RequiredFieldValidator>--%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" id="chkKontrol" runat="server" />
    </section>

    <script src="/js/inputmask.js"></script>
</asp:Content>
