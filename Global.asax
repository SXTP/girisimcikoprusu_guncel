﻿<%@ Application Language="C#" %>

<script runat="server">

    void Application_Start(object sender, EventArgs e) 
    {
        // Code that runs on application startup
        RegisterRoutes(System.Web.Routing.RouteTable.Routes);

    }
    private void RegisterRoutes(System.Web.Routing.RouteCollection routes)
    {
        routes.Clear();
        routes.Ignore("assets/");
        System.Web.Routing.RouteTable.Routes.Ignore("{*chartimg}", new { chartimg = @".*/ChartImg\.axd(/.*)?" });
        System.Web.Routing.RouteTable.Routes.Ignore("{resource}.axd/{*pathInfo}");
        System.Web.Routing.RouteTable.Routes.Ignore("{*favicon}", new { favicon = @"(.*/)?favicon.ico(/.*)?" });
        routes.MapPageRoute("homepage", "anasayfa", "~/Default.aspx");
        routes.MapPageRoute("content", "Egitim/{Id}/{Title}", "~/ContentView.aspx");
        routes.MapPageRoute("content2", "Sayfa/{Id}/{Title}", "~/PageView.aspx");

        routes.MapPageRoute("AppForm", "Basvuru/{Id}/{Title}", "~/ApplicationForm.aspx");
        routes.MapPageRoute("AppFormTest", "BasvuruTest/{Id}/{Title}", "~/ApplicationFormTest.aspx");


    }
    
    void Application_End(object sender, EventArgs e) 
    {
        //  Code that runs on application shutdown

    }
        
    void Application_Error(object sender, EventArgs e) 
    { 
        // Code that runs when an unhandled error occurs

    }

    void Session_Start(object sender, EventArgs e) 
    {
        // Code that runs when a new session is started

    }

    void Session_End(object sender, EventArgs e) 
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.

    }
       
</script>
