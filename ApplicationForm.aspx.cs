﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EXERT;


public partial class ApplicationForm : WebSite
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            drpDogumYil.DataTextField = "Texter";
            drpDogumYil.DataValueField = "Texter";
            drpDogumYil.DataSource = Numbers.NumberList(DateTime.Now.Year - 18, DateTime.Now.Year - 70, false);
            drpDogumYil.DataBind();
        }

        foreach (var item in db.Universities.ToList())
        {
            ddlUni.Items.Add(new ListItem(item.name, item.Id.ToString()));
        }

        //xContentId = Convert.ToInt32(Page.RouteData.Values["ID"]);

        if (!IsPostBack)
        {
            dvInfoTitle.Visible = false;
            dvSuc.Visible = false;
            //var cnt = db.Contents.FirstOrDefault(x => x.ID == xContentId && x.DbStatus == false);

            //if (cnt != null)
            //{
            //    txtEgitimAdi.Text = cnt.ContentTitle;
            //    txtEgitimUcreti.Text = cnt.ContentPrice.ToString() + " TL";
            //    this.Title = cnt.ContentTitle + " - Eğitim Başvuru - " + EXERT.SiteSettings.SiteName;
            //    txtEgitimAdi.Text = cnt.ContentTitle;
            //    txtEgitimUcreti.Text = cnt.ContentPrice.ToString() + " TL";


            //}
            //else
            //    Response.Redirect("/");
        }
    }

    protected void btnDevamEt_Click(object sender, EventArgs e)
    {
        //User Control
        xContentId = 1087;

        var BasvuruTamamladiMi = db.KesinBasvurular.FirstOrDefault(x => x.TCNo == txtTC.Text.Trim() && x.ID > 385);
        if (BasvuruTamamladiMi != null)
        {
            ltrBasvuruBilgi.Text = "<p style='color:red;'>Tekrar başvuru yapamazsınız.</p>";
            ltrBasvuruBilgi.Visible = true;
            return;
        }
        else
        {

        }


        var usr = db.Users.FirstOrDefault(x => x.UserDetails.FirstOrDefault().Email == txtMail.Text);




        var appC = db.Applications.FirstOrDefault(x => x.Users.UserDetails.FirstOrDefault().Email == txtMail.Text && x.FkContentId == xContentId && x.Status == 4 && x.FkContentId == 1087);
        if (appC != null)
        {
            if (appC.Status == 4)
            {
                ltrBasvuruBilgi.Text = "Tekrar Başvuru Yapamazsınız.Başvurunuz Mevcuttur.";
                return;
            }
        }
        if (usr == null)
        {
            usr = new Users();
            usr.UserName = txtSoyad.Text;
            usr.Name = txtAd.Text.ToUpper(new System.Globalization.CultureInfo("tr-TR"));
            usr.Surname = txtSoyad.Text.ToUpper(new System.Globalization.CultureInfo("tr-TR"));
            usr.Password = "12345";
            usr.ProfilImg = "";
            usr.Status = 0;
            usr.UserType = 1;
            usr.Gender = drpCinsiyet.SelectedValue;
            db.Users.Add(usr);

            UserDetails usd = new UserDetails();
            usd.FkUserId = usr.ID;
            usd.Address = "";
            usd.AddressCity = /*drpil.SelectedItem.Text*/"";
            usd.AddressCityNo = /*Convert.ToInt32(drpil.SelectedValue)*/0;
            usd.AddressPostCode = "";
            usd.AddressStreet = "";
            usd.BirthYear = Convert.ToInt32(drpDogumYil.SelectedValue);
            usd.CreateDate = DateTime.Now;
            usd.Email = txtMail.Text;
            usd.PhoneNumber = txtCepTel.Text;
            usd.PhoneNumber2 = "";
            usd.FKUniversity = Convert.ToInt32(ddlUni.SelectedValue);
            usd.GirisimciEgitimiAldi = string.Empty; // drpEgitimKontrol.SelectedValue;
            //usd.DahaOnceGirisim = drpGirisimFaaliyet.SelectedValue == "1" ? true : false;
            usd.Nationality = drpVatandaslik.SelectedValue == "1" ? true : false;
            usd.UniverstyDepartment = txtBolum.Text;
            db.UserDetails.Add(usd);

            Applications app = new Applications();
            app.FkUserId = usr.ID;
            app.FkContentId = 1087;
            app.CreateDate = DateTime.Now;
            app.DbStatus = false;
            app.IPAdress = "";
            app.PaymentType = 0;
            app.Status = 0;
            //app.ProjectSummary = txtProjeFikri.Text;
            //app.ProjectPlan = txtProjeSurec.Text;
            app.ContentField1 = null;
            app.FkGroupId = null;
            db.Applications.Add(app);

            db.SaveChanges();

            dvForm.Visible = false;
            dvSuc.Visible = true;

            KesinBasvurular basvuru = new KesinBasvurular();
            basvuru.FkUserId = usr.ID;
            basvuru.EgitimeKatilacagiIl = /*drpil.SelectedValue*/"";
            basvuru.TCNo = txtTC.Text.Trim();
            db.KesinBasvurular.Add(basvuru);
            db.SaveChanges();
            ltrAppStatu.Text = "<p style='color:red;'>Başvurunuz Tamamlanmıştır Sizinle En Yakında Zamanda İletişime Geçilecektir.</p>";
            ltrAppStatu.Visible = true;


            foreach (var user in db.Users.Where(u => u.UserType == 0))
            {
                db.Notifications.Add(new Notifications
                {
                    AppId = app.ID,
                    BContent = txtAd.Text.ToUpper(new System.Globalization.CultureInfo("tr-TR")) + " " + txtSoyad.Text.ToUpper(new System.Globalization.CultureInfo("tr-TR")) + " başvuru yaptı.",
                    UserID = user.ID,
                    Readed = false,
                    Time = DateTime.Now
                });
            }

            db.SaveChanges();
        }
        else
        {
            var usd = db.UserDetails.FirstOrDefault(x => x.FkUserId == usr.ID);
            usd.FkUserId = usr.ID;
            usd.Address = "";
            usd.AddressCity = ""; //drpil.SelectedItem.Text;
            usd.AddressCityNo = 0; //Convert.ToInt32(drpil.SelectedValue);
            usd.AddressPostCode = "";
            usd.AddressStreet = "";
            usd.BirthYear = Convert.ToInt32(drpDogumYil.SelectedValue);
            usd.CreateDate = DateTime.Now;
            usd.Email = txtMail.Text;
            usd.PhoneNumber = txtCepTel.Text;
            usd.PhoneNumber2 = "";

            Applications app = db.Applications.FirstOrDefault(a => a.FkUserId == usr.ID && a.FkContentId == xContentId && a.DbStatus == false);
            bool bildirimKontrol = false;
            if (app == null)
            {
                app = new Applications();
                app.FkUserId = usr.ID;
                app.FkContentId = 1087;
                app.CreateDate = DateTime.Now;
                app.DbStatus = false;
                app.IPAdress = "";
                app.PaymentType = 0;
                app.Status = 0;
                //app.ProjectSummary = txtProjeFikri.Text;
                //app.ProjectPlan = txtProjeSurec.Text;
                app.ContentField1 = null;
                app.FkGroupId = null;
                db.Applications.Add(app);

                //KesinBasvurular basvuru = new KesinBasvurular();
                //basvuru.FkUserId = usr.ID;
                //basvuru.EgitimeKatilacagiIl = drpil.SelectedValue;
                //basvuru.TCNo = txtTC.Text.Trim();
                //db.KesinBasvurular.Add(basvuru);
                //db.SaveChanges();
                //ltrBasvuruBilgi.Text = "<p style='color:red;'>Başvurunuz Tamamlanmıştır Sizinle En Yakında Zamanda İletişime Geçilecektir.</p>";
                //ltrBasvuruBilgi.Visible = true;

                ltrAppStatu.Text = "Başvurunuz Alınmıştır.";
                bildirimKontrol = true;
            }
            else
            {
                KesinBasvurular basvuru = db.KesinBasvurular.Where(p => p.FkUserId == usr.ID).FirstOrDefault();
                if (basvuru != null)
                {
                    basvuru.FkUserId = usr.ID;
                    basvuru.EgitimeKatilacagiIl = ""; //drpil.SelectedValue;
                    basvuru.TCNo = txtTC.Text.Trim();
                    db.SaveChanges();
                    ltrAppStatu.Text = "Başvurunuz Güncellenmiştir.";
                    //ltrBasvuruBilgi.Text = "<p style='color:red;'>Başvurunuz Tamamlanmıştır Sizinle En Yakında Zamanda İletişime Geçilecektir.</p>";
                    //ltrBasvuruBilgi.Visible = true;
                }
                
            }

            app.FkContentId = xContentId;
            app.CreateDate = DateTime.Now;
            app.DbStatus = false;
            app.IPAdress = "";
            app.PaymentType = 0;
            app.Status = 0;

            app.ContentField1 = null;
            app.FkGroupId = null;

            db.SaveChanges();

            dvForm.Visible = false;
            dvSuc.Visible = true;

            foreach (var user in db.Users.Where(u => u.UserType == 0))
            {
                db.Notifications.Add(new Notifications
                {
                    AppId = app.ID,
                    BContent = txtAd.Text.ToUpper(new System.Globalization.CultureInfo("tr-TR")) + " " + txtSoyad.Text.ToUpper(new System.Globalization.CultureInfo("tr-TR")) + (bildirimKontrol == false ? " başvurusunu güncelledi." : " başvuru yaptı."),
                    UserID = user.ID,
                    Readed = false,
                    Time = DateTime.Now
                });
            }

            db.SaveChanges();
        }
    }

    //protected void cv2_ServerValidate(object source, ServerValidateEventArgs args)
    //{
    //    args.IsValid = (drpil.SelectedIndex > 0);
    //}
    protected decimal Taksitlendir(int taksit, decimal ucret)
    {

        decimal tutar = ucret;

        if (taksit == 1)
        {
            tutar = ucret;
        }
        else if (taksit == 3)
        {
            tutar = (ucret * ((Decimal)1.06));
        }
        else if (taksit == 9)
        {
            tutar = (ucret * ((Decimal)1.15));
        }

        return tutar;
    }
    public static string KarakterDuzelt(string metin)
    {
        return metin.Replace('ş', 's').Replace('Ş', 'S').Replace('ç', 'c').Replace("Ç", "C").Replace('ğ', 'g').Replace('Ğ', 'G').
            Replace('ü', 'u').Replace('Ü', 'U').Replace('ı', 'i').Replace('İ', 'I').Replace('ö', 'o').Replace('Ö', 'O').
            Replace(' ', '-').Replace("?", "").Replace(",", "").Replace("/", "").Replace(".", "").Replace("\"", "").Replace("#", "");  //Gibi Gibi
    }
}