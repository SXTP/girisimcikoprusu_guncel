﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Administrator_SikayetveOneriler : Admin
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SetHeader("Şikayet ve Öneriler", "Tüm Liste", "", "");
        rptSikayetList.DataSource = db.SikayetOneri.ToList();
        rptSikayetList.DataBind();
    }
}