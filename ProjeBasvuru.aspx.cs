﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EXERT;
using System.Globalization;

public partial class ProjeBasvuru : WebSite
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //dvInfoTitle.Visible = false;
            dvSuc.Visible = false;
            //var cnt = db.Contents.FirstOrDefault(x => x.ID == xContentId && x.DbStatus == false);

        }
    }

    protected void btnDevamEt_Click(object sender, EventArgs e)
    {
        using (DataEntities db = new DataEntities())
        {
            string FileExtension = ".docx";
            string sErrors = string.Empty;
            if (string.IsNullOrEmpty(txtTC.Text.Trim()))
                sErrors = "Lütfen TC Kimlik Numaranızı giriniz<br/>";
            if (string.IsNullOrEmpty(txtAd.Text.Trim()))
                sErrors = "Lütfen Adınızı giriniz<br/>";
            if (string.IsNullOrEmpty(txtSoyad.Text.Trim()))
                sErrors = "Lütfen Soyadınızı giriniz<br/>";
            if (!fUp.HasFile)
                sErrors = "Lütfen Proje dosyanızı seçiniz<br/>";
            else
            {
                FileExtension = Path.GetExtension(fUp.PostedFile.FileName).ToLower();
                if (FileExtension != ".pdf" && FileExtension != ".docx" && FileExtension != ".doc")
                    sErrors = "Proje dosyası pdf veya doc formatında olmalıdır.<br/>";
            }

            
    
            if (!string.IsNullOrEmpty(sErrors))
            {
                dvHata.Visible = true;
                dvHata.InnerHtml = sErrors;
                return;
            }


            var appC = db.ProjectApplications.FirstOrDefault(x => x.TCNo == txtTC.Text.Trim());
            if (appC != null)
            {
                dvHata.Visible = true;
                dvHata.InnerText = "Daha önce başvuru yaptınız.";
                return;
            }
            else
            {
                ProjectApplications yn = new ProjectApplications();
                yn.TCNo = txtTC.Text;
                yn.Ad = txtAd.Text.TurkishUpper();
                yn.Soyad = txtSoyad.Text.TurkishUpper();

                //folder path to save uploaded file
                string folderPath = Server.MapPath("~/uploads/ProjeDosya/");

                Guid g = Guid.NewGuid();
                string sFileName = g.ToString();
                string sFilePath = string.Format("{0}{1}", folderPath, sFileName + FileExtension);
                fUp.SaveAs(sFilePath);
                yn.DosyaUrl = sFileName + FileExtension;
                yn.IsDeleted = false;
                yn.CreatedOn = DateTime.Now;
                db.ProjectApplications.Add(yn);
                db.SaveChanges();

                dvForm.Visible = false;
                dvSuc.Visible = true;
                int iBasvuruNo = yn.ID;
                ltrAppStatu.Text = "Başvurunuz başarıyla kaydedilmiştir.<br><br>Başvuru No:" + iBasvuruNo;
            }
        }

    }
}